package org.jeecg.business.webapp.service.impl;

import java.util.Map;
import org.jeecg.common.util.SpringContextUtils;
import org.jeecg.modules.online.cgform.enhance.CgformEnhanceJavaInter;
import org.jeecg.modules.online.cgform.mapper.OnlCgformFieldMapper;
import org.jeecg.modules.online.config.exception.BusinessException;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;

@Component("circuitDeleteEnhanceInter")
public class CircuitDeleteEnhanceInter implements CgformEnhanceJavaInter {

	@Override
	public int execute(String s, Map map) throws BusinessException {
		return 0;
	}

	@Override
	public int execute(String s, JSONObject jsonobject) throws BusinessException {
		System.out.println("--------java增强JSON："+ jsonobject.toJSONString());
		String taskCode = jsonobject.getString("task_code");
		OnlCgformFieldMapper onlCgformFieldMapper = SpringContextUtils.getBean(OnlCgformFieldMapper.class);
		
		String deleteSql = "delete from xy_park_patrol_taskinfo where task_code = '" + taskCode + "'";
		
		onlCgformFieldMapper.deleteAutoList(deleteSql);
		
		return 1;
	}

}
