package org.jeecg.business.webapp.service.impl;

import org.jeecg.common.util.SpringContextUtils;
import org.jeecg.modules.online.cgform.mapper.OnlCgformFieldMapper;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class UpdateHourseStatusJob implements Job  {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		OnlCgformFieldMapper onlCgformFieldMapper = SpringContextUtils.getBean(OnlCgformFieldMapper.class);
		String sql1 = "update xy_park_leasecontract set contract_status = '3' where lease_endtime < NOW() and contract_status != '3'";
		onlCgformFieldMapper.editFormData(sql1);
		String sql2 = "update xy_park_propertycontract set contract_status = '3' where service_endtime < NOW() and contract_status != '3'";
		onlCgformFieldMapper.editFormData(sql2);	
		String sql3 = "update xy_park_landlease set contract_status = '3' where end_date < NOW() and contract_status != '3'";
		onlCgformFieldMapper.editFormData(sql3);
	}

}
