package org.jeecg.business.webapp.service.impl;

import java.math.BigDecimal;
import java.util.Map;
import java.util.UUID;

import org.jeecg.common.util.SpringContextUtils;
import org.jeecg.modules.online.cgform.enhance.CgformEnhanceJavaInter;
import org.jeecg.modules.online.cgform.mapper.OnlCgformFieldMapper;
import org.jeecg.modules.online.config.exception.BusinessException;
import org.jeecg.modules.system.service.SnRuleService;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;

@Component("meterReadAddEnhanceInter")
public class MeterReadAddEnhanceInter implements CgformEnhanceJavaInter {

	@Override
	public int execute(String s, Map map) throws BusinessException {
		return 0;
	}

	@Override
	public int execute(String s, JSONObject jsonobject) throws BusinessException {
		String recordType = jsonobject.getString("record_type");
		if("2".equals(recordType)){
			return 1;
		}
		
		String deviceCode = jsonobject.getString("device_code");
		OnlCgformFieldMapper onlCgformFieldMapper = SpringContextUtils.getBean(OnlCgformFieldMapper.class);
		String selectSql = "Select * from xy_park_meterreadingrecord where device_code = '"+deviceCode+"' order by create_time desc limit 1";
		System.out.println("----selectSql===" + selectSql.toString());
		
		Map<String, Object> lastRecord = onlCgformFieldMapper.queryFormData(selectSql);
		
		String deviceSql = "Select * from xy_park_device where device_code = '"+deviceCode+"'";
		System.out.println("----deviceSql===" + deviceSql.toString());
		
		Map<String, Object> deviceRecord = onlCgformFieldMapper.queryFormData(deviceSql);
		BigDecimal unitPrice = new BigDecimal(deviceRecord.get("price").toString());
		
		String updateSql = "update xy_park_meterreadingrecord set billing_status = '2' where billing_status = '1' and device_code = '" + deviceCode + "'";
		System.out.println("----updateSql===" + updateSql.toString());
		
		onlCgformFieldMapper.editFormData(updateSql);
		BigDecimal diff = jsonobject.getBigDecimal("meterreading_record").subtract(new BigDecimal(lastRecord.get("meterreading_record").toString()));
		BigDecimal price = diff.multiply(unitPrice);
		
		String type = (String) deviceRecord.get("device_sort");
		String costType;
		String costName;
		if("1".equals(type)){
			costType = "XY-fx-2021-08-005";
			costName = "电费";
		}else{
			costType = "XY-fx-2021-08-004";
			costName = "水费";
		}
		String uuid = UUID.randomUUID().toString();
		SnRuleService snRuleService = SpringContextUtils.getBean(SnRuleService.class);
		String sn = snRuleService.execute("XTSD-${YYYY}${MM}${DD}-#####", null) + "";
		
		StringBuffer insertSql = new StringBuffer("insert into xy_park_meterreadingbill (id, create_time, bill_code, device_code, device_sort, meter_diff, bill_amount, cost_type, cost_name, record_code) values ('"+uuid+"', now()");
		insertSql.append(",'"+sn+"'");
		insertSql.append(",'"+deviceCode+"'");
		insertSql.append(",'"+type+"'");
		insertSql.append(","+diff.toString());
		insertSql.append(","+price.toString());
		insertSql.append(",'"+costType+"'");
		insertSql.append(",'"+costName+"'");
		insertSql.append(",'"+jsonobject.getString("record_code")+"'");
		insertSql.append(")");
		System.out.println("----insertSql===" + insertSql.toString());
		onlCgformFieldMapper.saveFormData(insertSql.toString());
		return 1;
	}

}
