package org.jeecg.business.webapp.service.impl;

import java.util.Map;

import org.jeecg.common.util.SpringContextUtils;
import org.jeecg.modules.online.cgform.enhance.CgformEnhanceJavaInter;
import org.jeecg.modules.online.cgform.mapper.OnlCgformFieldMapper;
import org.jeecg.modules.online.config.exception.BusinessException;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;

@Component("importXyParkAccountFlowEnhanceInter")
public class ImportXyParkAccountFlowEnhanceInter implements CgformEnhanceJavaInter {

	@Override
	public int execute(String s, Map<String, Object> map) throws BusinessException {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public int execute(String tableName, JSONObject json) throws BusinessException {
		OnlCgformFieldMapper onlCgformFieldMapper = SpringContextUtils.getBean(OnlCgformFieldMapper.class);
		System.out.println("import json===>"+json.toJSONString());
		if(!"1".equals(json.getString("trading_status"))){
			return 0;
		}
		String selectSql = "Select * from xy_park_accounting_flow where account_code = '"+json.getString("account_code")+"'";
		Map<String, Object> record = onlCgformFieldMapper.queryFormData(selectSql);
		System.out.println("import record===>"+record);
		if(record == null){
			return 1;
		}else{
			return 0;
		}
	}
}
