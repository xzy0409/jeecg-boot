package org.jeecg.weixin.cp.bean.workbench;

import lombok.Data;

import java.io.Serializable;

/**
 * 关键数据型模板类型
 * @author songshiyu
 */
@Data
public class WorkBenchKeyData implements Serializable {
	private static final long serialVersionUID = -247823931134070754L;
	/*
	 * 关键数据名称
	 */
	private String key;
	/*
	 * 关键数据
	 */
	private String data;
	/*
	 * 点击跳转url，若不填且应用设置了主页url，则跳转到主页url，否则跳到应用会话窗口
	 */
	private String jumpUrl;
	/*
	 * 若应用为小程序类型，该字段填小程序pagepath，若未设置，跳到小程序主页
	 */
	private String pagePath;
}
