package org.jeecg.weixin.cp.bean.oa;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

/**
 * 企业微信日报数据.
 *
 * @author Element
 */
@Data
public class WxCpCheckinMonthData implements Serializable {
	
	private static final long serialVersionUID = 2307305003623690774L;

	@SerializedName("base_info")
	private BaseInfo baseInfo;
	
	@SerializedName("summary_info")
	private SummaryInfo summaryInfo;
	
	@SerializedName("exception_infos")
	private List<ExceptionInfo> exceptionInfos;
	
	@SerializedName("sp_items")
	private List<SpItem> spItems;
	
	@SerializedName("overwork_info")
	private OverworkInfo overworkInfo;
	
	@Data
	public static class BaseInfo implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -1720519958099372544L;

		@SerializedName("record_type")
		private Long recordType;

		@SerializedName("name")
		private String name;

		@SerializedName("name_ex")
		private String nameEx;
		
		@SerializedName("departs_name")
		private String departsName;

		@SerializedName("acctid")
		private String acctid;
	}
	
	@Data
	public static class SummaryInfo implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -5971040472792452081L;

		//应打卡天数
		@SerializedName("work_days")
		private Integer workDays;

		//正常天数
		@SerializedName("regular_days")
		private Integer regularDays;

		//异常天数
		@SerializedName("except_days")
		private Integer exceptDays;

		//实际工作时长
		@SerializedName("regular_work_sec")
		private Integer regularWorkSec;
		
		//标准工作时长
		@SerializedName("standard_work_sec")
		private Integer standardWorkSec;

	}
	
	@Data
	public static class ExceptionInfo implements Serializable {

		private static final long serialVersionUID = 2283134614139415966L;

		@SerializedName("exception")
		private Long exception;
		
		@SerializedName("count")
		private Integer count;
		
		@SerializedName("duration")
		private Integer duration;
	}
	
	
	@Data
	public static class SpItem implements Serializable {

		private static final long serialVersionUID = -5178189161883925070L;

		@SerializedName("type")
		private Long type;
		
		@SerializedName("vacation_id")
		private Long vacationId;
		
		@SerializedName("count")
		private Long count;
		
		@SerializedName("duration")
		private Long duration;
		
		@SerializedName("time_type")
		private Long timeType;
		
		@SerializedName("name")
		private String name;
	}
	
	@Data
	public static class OverworkInfo implements Serializable {

		private static final long serialVersionUID = 4978002350598056185L;
		
		@SerializedName("workday_over_sec")
		private Integer workdayOverSec;
		
		@SerializedName("holidaysOverSec")
		private Integer holidaysOverSec;
		
		@SerializedName("restdaysOverSec")
		private Integer restdaysOverSec;
	}

}
