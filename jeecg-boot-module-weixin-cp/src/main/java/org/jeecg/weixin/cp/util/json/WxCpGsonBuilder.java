package org.jeecg.weixin.cp.util.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.LongSerializationPolicy;

import org.jeecg.weixin.common.bean.menu.WxMenu;
import org.jeecg.weixin.common.error.WxError;
import org.jeecg.weixin.common.util.json.WxErrorAdapter;
import org.jeecg.weixin.cp.bean.WxCpChat;
import org.jeecg.weixin.cp.bean.WxCpDepart;
import org.jeecg.weixin.cp.bean.WxCpTag;
import org.jeecg.weixin.cp.bean.WxCpUser;

/**
 * @author Daniel Qian
 */
public class WxCpGsonBuilder {

	private static final GsonBuilder INSTANCE = new GsonBuilder();

	static {
		INSTANCE.disableHtmlEscaping();
		INSTANCE.registerTypeAdapter(WxCpChat.class, new WxCpChatGsonAdapter());
		INSTANCE.registerTypeAdapter(WxCpDepart.class, new WxCpDepartGsonAdapter());
		INSTANCE.registerTypeAdapter(WxCpUser.class, new WxCpUserGsonAdapter());
		INSTANCE.registerTypeAdapter(WxError.class, new WxErrorAdapter());
		INSTANCE.registerTypeAdapter(WxMenu.class, new WxCpMenuGsonAdapter());
		INSTANCE.registerTypeAdapter(WxCpTag.class, new WxCpTagGsonAdapter());
		INSTANCE.setLongSerializationPolicy(LongSerializationPolicy.STRING);
	}

	public static Gson create() {
		return INSTANCE.create();
	}

}
