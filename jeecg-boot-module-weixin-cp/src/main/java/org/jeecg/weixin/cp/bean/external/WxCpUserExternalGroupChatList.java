package org.jeecg.weixin.cp.bean.external;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import org.jeecg.weixin.cp.bean.WxCpBaseResp;
import org.jeecg.weixin.cp.util.json.WxCpGsonBuilder;

import java.util.List;

/**
 * @author yqx
 */
@Getter
@Setter
public class WxCpUserExternalGroupChatList extends WxCpBaseResp {

	private static final long serialVersionUID = 1L;
	@SerializedName("group_chat_list")
	private List<ChatStatus> groupChatList;

	@Getter
	@Setter
	public static class ChatStatus {

		/**
		 * 客户群ID
		 */
		@SerializedName("chat_id")
		private String chatId;

		/**
		 * 客户群状态 0 - 正常 1 - 跟进人离职 2 - 离职继承中 3 - 离职继承完成
		 */
		@SerializedName("status")
		private int status;

	}

	public static WxCpUserExternalGroupChatList fromJson(String json) {
		return WxCpGsonBuilder.create().fromJson(json, WxCpUserExternalGroupChatList.class);
	}
}
