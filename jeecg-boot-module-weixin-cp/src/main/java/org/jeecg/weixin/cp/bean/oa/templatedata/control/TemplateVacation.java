package org.jeecg.weixin.cp.bean.oa.templatedata.control;

import lombok.Data;
import org.jeecg.weixin.cp.bean.oa.templatedata.TemplateVacationItem;

import java.io.Serializable;
import java.util.List;

/**
 * @author gyv12345@163.com
 */
@Data
public class TemplateVacation implements Serializable {

	private static final long serialVersionUID = -1029690689666139465L;
	private List<TemplateVacationItem> item;

}
