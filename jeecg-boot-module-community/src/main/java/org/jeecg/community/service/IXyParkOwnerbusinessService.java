package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkOwnerbusiness;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * 业主工商信息
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface IXyParkOwnerbusinessService extends IService<XyParkOwnerbusiness> {

	public List<XyParkOwnerbusiness> selectByMainId(String mainId);
}
