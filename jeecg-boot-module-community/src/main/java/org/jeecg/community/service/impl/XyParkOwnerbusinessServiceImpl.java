package org.jeecg.community.service.impl;

import org.jeecg.community.entity.XyParkOwnerbusiness;
import org.jeecg.community.mapper.XyParkOwnerbusinessMapper;
import org.jeecg.community.service.IXyParkOwnerbusinessService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class XyParkOwnerbusinessServiceImpl extends ServiceImpl<XyParkOwnerbusinessMapper, XyParkOwnerbusiness> implements IXyParkOwnerbusinessService {
	
	@Autowired
	private XyParkOwnerbusinessMapper xyParkOwnerbusinessMapper;
	
	@Override
	public List<XyParkOwnerbusiness> selectByMainId(String mainId) {
		return xyParkOwnerbusinessMapper.selectByMainId(mainId);
	}
}
