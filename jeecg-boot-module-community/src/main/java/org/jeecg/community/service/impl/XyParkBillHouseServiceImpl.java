package org.jeecg.community.service.impl;

import org.jeecg.community.entity.XyParkBillHouse;
import org.jeecg.community.mapper.XyParkBillHouseMapper;
import org.jeecg.community.service.IXyParkBillHouseService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class XyParkBillHouseServiceImpl extends ServiceImpl<XyParkBillHouseMapper,XyParkBillHouse> implements IXyParkBillHouseService {

}
