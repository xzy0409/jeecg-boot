package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkOwnercontact;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * 业主联系人信息
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface IXyParkOwnercontactService extends IService<XyParkOwnercontact> {

	public List<XyParkOwnercontact> selectByMainId(String mainId);
}
