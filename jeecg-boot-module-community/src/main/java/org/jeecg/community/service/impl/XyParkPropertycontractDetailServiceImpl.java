package org.jeecg.community.service.impl;

import org.jeecg.community.entity.XyParkPropertycontractDetail;
import org.jeecg.community.mapper.XyParkPropertycontractDetailMapper;
import org.jeecg.community.service.IXyParkPropertycontractDetailService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 物业合同房源信息
 * @Author: jeecg-boot
 * @Date:   2022-04-13
 * @Version: V1.0
 */
@Service
public class XyParkPropertycontractDetailServiceImpl extends ServiceImpl<XyParkPropertycontractDetailMapper, XyParkPropertycontractDetail> implements IXyParkPropertycontractDetailService {
	
	@Autowired
	private XyParkPropertycontractDetailMapper xyParkPropertycontractDetailMapper;
	
	@Override
	public List<XyParkPropertycontractDetail> selectByMainId(String mainId) {
		return xyParkPropertycontractDetailMapper.selectByMainId(mainId);
	}
}
