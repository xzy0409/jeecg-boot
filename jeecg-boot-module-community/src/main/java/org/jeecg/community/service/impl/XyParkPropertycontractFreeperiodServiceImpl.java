package org.jeecg.community.service.impl;

import org.jeecg.community.entity.XyParkPropertycontractFreeperiod;
import org.jeecg.community.mapper.XyParkPropertycontractFreeperiodMapper;
import org.jeecg.community.service.IXyParkPropertycontractFreeperiodService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 免租期
 * @Author: jeecg-boot
 * @Date:   2022-04-13
 * @Version: V1.0
 */
@Service
public class XyParkPropertycontractFreeperiodServiceImpl extends ServiceImpl<XyParkPropertycontractFreeperiodMapper, XyParkPropertycontractFreeperiod> implements IXyParkPropertycontractFreeperiodService {
	
	@Autowired
	private XyParkPropertycontractFreeperiodMapper xyParkPropertycontractFreeperiodMapper;
	
	@Override
	public List<XyParkPropertycontractFreeperiod> selectByMainId(String mainId) {
		return xyParkPropertycontractFreeperiodMapper.selectByMainId(mainId);
	}
}
