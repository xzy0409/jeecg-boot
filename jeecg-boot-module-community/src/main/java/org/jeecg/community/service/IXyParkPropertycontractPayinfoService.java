package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkPropertycontractPayinfo;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 物业合同款项信息
 * @Author: jeecg-boot
 * @Date:   2022-04-13
 * @Version: V1.0
 */
public interface IXyParkPropertycontractPayinfoService extends IService<XyParkPropertycontractPayinfo> {

	public List<XyParkPropertycontractPayinfo> selectByMainId(String mainId);
}
