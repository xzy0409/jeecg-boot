package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkContractFee;

import com.baomidou.mybatisplus.extension.service.IService;

public interface IXyParkContractFeeService extends IService<XyParkContractFee> {

}
