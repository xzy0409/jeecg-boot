package org.jeecg.community.service.impl;

import org.jeecg.community.entity.XyParkLeasecontract;
import org.jeecg.community.entity.XyParkLeasecontractDetail;
import org.jeecg.community.entity.XyParkLeasecontractPayinfo;
import org.jeecg.community.entity.XyParkLeasecontractFreeperiod;
import org.jeecg.community.mapper.XyParkLeasecontractDetailMapper;
import org.jeecg.community.mapper.XyParkLeasecontractPayinfoMapper;
import org.jeecg.community.mapper.XyParkLeasecontractFreeperiodMapper;
import org.jeecg.community.mapper.XyParkLeasecontractMapper;
import org.jeecg.community.service.IXyParkLeasecontractService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 新亚园区租赁合同
 * @Author: jeecg-boot
 * @Date:   2022-04-13
 * @Version: V1.0
 */
@Service
public class XyParkLeasecontractServiceImpl extends ServiceImpl<XyParkLeasecontractMapper, XyParkLeasecontract> implements IXyParkLeasecontractService {

	@Autowired
	private XyParkLeasecontractMapper xyParkLeasecontractMapper;
	@Autowired
	private XyParkLeasecontractDetailMapper xyParkLeasecontractDetailMapper;
	@Autowired
	private XyParkLeasecontractPayinfoMapper xyParkLeasecontractPayinfoMapper;
	@Autowired
	private XyParkLeasecontractFreeperiodMapper xyParkLeasecontractFreeperiodMapper;
	
	@Override
	@Transactional
	public void saveMain(XyParkLeasecontract xyParkLeasecontract, List<XyParkLeasecontractDetail> xyParkLeasecontractDetailList,List<XyParkLeasecontractPayinfo> xyParkLeasecontractPayinfoList,List<XyParkLeasecontractFreeperiod> xyParkLeasecontractFreeperiodList) {
		xyParkLeasecontractMapper.insert(xyParkLeasecontract);
		if(xyParkLeasecontractDetailList!=null && xyParkLeasecontractDetailList.size()>0) {
			for(XyParkLeasecontractDetail entity:xyParkLeasecontractDetailList) {
				//外键设置
				entity.setContractCode(xyParkLeasecontract.getContractCode());

				xyParkLeasecontractDetailMapper.insert(entity);
			}
		}
		if(xyParkLeasecontractPayinfoList!=null && xyParkLeasecontractPayinfoList.size()>0) {
			for(XyParkLeasecontractPayinfo entity:xyParkLeasecontractPayinfoList) {
				//外键设置
				entity.setContractCode(xyParkLeasecontract.getContractCode());
				//外键设置
				xyParkLeasecontractPayinfoMapper.insert(entity);
			}
		}
		if(xyParkLeasecontractFreeperiodList!=null && xyParkLeasecontractFreeperiodList.size()>0) {
			for(XyParkLeasecontractFreeperiod entity:xyParkLeasecontractFreeperiodList) {
				//外键设置
				entity.setPid(xyParkLeasecontract.getContractCode());
				xyParkLeasecontractFreeperiodMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void updateMain(XyParkLeasecontract xyParkLeasecontract,List<XyParkLeasecontractDetail> xyParkLeasecontractDetailList,List<XyParkLeasecontractPayinfo> xyParkLeasecontractPayinfoList,List<XyParkLeasecontractFreeperiod> xyParkLeasecontractFreeperiodList) {
		xyParkLeasecontractMapper.updateById(xyParkLeasecontract);
		
		//1.先删除子表数据
		xyParkLeasecontractDetailMapper.deleteByMainId(xyParkLeasecontract.getContractCode());
		xyParkLeasecontractDetailMapper.deleteByMainId(xyParkLeasecontract.getContractCode());
		xyParkLeasecontractDetailMapper.deleteByMainId(xyParkLeasecontract.getContractCode());
		xyParkLeasecontractPayinfoMapper.deleteByMainId(xyParkLeasecontract.getContractCode());
		xyParkLeasecontractPayinfoMapper.deleteByMainId(xyParkLeasecontract.getContractCode());
		xyParkLeasecontractPayinfoMapper.deleteByMainId(xyParkLeasecontract.getContractCode());
		xyParkLeasecontractFreeperiodMapper.deleteByMainId(xyParkLeasecontract.getContractCode());
		xyParkLeasecontractFreeperiodMapper.deleteByMainId(xyParkLeasecontract.getContractCode());
		xyParkLeasecontractFreeperiodMapper.deleteByMainId(xyParkLeasecontract.getContractCode());
		
		//2.子表数据重新插入
		if(xyParkLeasecontractDetailList!=null && xyParkLeasecontractDetailList.size()>0) {
			for(XyParkLeasecontractDetail entity:xyParkLeasecontractDetailList) {
				//外键设置
				entity.setContractCode(xyParkLeasecontract.getContractCode());
				xyParkLeasecontractDetailMapper.insert(entity);
			}
		}
		if(xyParkLeasecontractPayinfoList!=null && xyParkLeasecontractPayinfoList.size()>0) {
			for(XyParkLeasecontractPayinfo entity:xyParkLeasecontractPayinfoList) {
				//外键设置
				entity.setContractCode(xyParkLeasecontract.getContractCode());
				xyParkLeasecontractPayinfoMapper.insert(entity);
			}
		}
		if(xyParkLeasecontractFreeperiodList!=null && xyParkLeasecontractFreeperiodList.size()>0) {
			for(XyParkLeasecontractFreeperiod entity:xyParkLeasecontractFreeperiodList) {
				//外键设置
				entity.setPid(xyParkLeasecontract.getContractCode());
				xyParkLeasecontractFreeperiodMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void delMain(String id) {
		xyParkLeasecontractDetailMapper.deleteByMainId(id);
		xyParkLeasecontractPayinfoMapper.deleteByMainId(id);
		xyParkLeasecontractFreeperiodMapper.deleteByMainId(id);
		xyParkLeasecontractMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			xyParkLeasecontractDetailMapper.deleteByMainId(id.toString());
			xyParkLeasecontractPayinfoMapper.deleteByMainId(id.toString());
			xyParkLeasecontractFreeperiodMapper.deleteByMainId(id.toString());
			xyParkLeasecontractMapper.deleteById(id);
		}
	}

	@Override
	@Transactional
	public void invalid(String id) {
		xyParkLeasecontractMapper.invalid(id);
	}
	
}
