package org.jeecg.community.service.impl;

import org.jeecg.community.entity.XyParkAccount;
import org.jeecg.community.entity.XyParkCash;
import org.jeecg.community.entity.XyParkCostType;
import org.jeecg.community.entity.XyParkOwner;
import org.jeecg.community.mapper.XyParkAccountMapper;
import org.jeecg.community.mapper.XyParkCashBillShipMapper;
import org.jeecg.community.mapper.XyParkCashMapper;
import org.jeecg.community.mapper.XyParkCostTypeMapper;
import org.jeecg.community.mapper.XyParkOwnerMapper;
import org.jeecg.community.service.IXyParkCashService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

@Service
public class XyParkCashServiceImpl extends ServiceImpl<XyParkCashMapper,XyParkCash> implements IXyParkCashService {
	
	@Autowired
	private XyParkOwnerMapper xyParkOwnerMapper;
	
	@Autowired
	private XyParkAccountMapper xyParkAccountMapper;
	
	@Autowired
	private XyParkCashBillShipMapper xyParkCashBillShipMapper;
	
	@Autowired
	private XyParkCostTypeMapper xyParkCostTypeMapper;

	@Override
	public void handleDetails(IPage<XyParkCash> pageList) {
		for(XyParkCash cash : pageList.getRecords()){
			cash.setXyParkOwner(xyParkOwnerMapper.selectOne(new QueryWrapper<XyParkOwner>().eq("owner_code", cash.getOwnerCode())));
			cash.setXyParkAccount(xyParkAccountMapper.selectOne(new QueryWrapper<XyParkAccount>().eq("park_number", cash.getAccountCode())));
			cash.setCheckAmount(xyParkCashBillShipMapper.sumCheckAmountByCashCode(cash.getCashCode()));
			cash.setXyParkCostType(xyParkCostTypeMapper.selectOne(new QueryWrapper<XyParkCostType>().eq("cost_type", cash.getCostType())));
		}
	}

	@Override
	public void handleDetailsWithOwner(List<XyParkCash> pageList) {
		for(XyParkCash cash : pageList){
			XyParkOwner owner_code = xyParkOwnerMapper.selectOne(new QueryWrapper<XyParkOwner>().eq("owner_code", cash.getOwnerCode()));
			cash.setXyParkOwner(owner_code);
			if(owner_code!=null){
				cash.setOwnerName(owner_code.getOwnerName());
			}
			XyParkCostType cost_type = xyParkCostTypeMapper.selectOne(new QueryWrapper<XyParkCostType>().eq("cost_type", cash.getCostType()));
			if (cost_type!=null){
				cash.setCostName(cost_type.getCostName());
			}
			/*cash.setXyParkAccount(xyParkAccountMapper.selectOne(new QueryWrapper<XyParkAccount>().eq("park_number", cash.getAccountCode())));
			cash.setCheckAmount(xyParkCashBillShipMapper.sumCheckAmountByCashCode(cash.getCashCode()));
			cash.setXyParkCostType(xyParkCostTypeMapper.selectOne(new QueryWrapper<XyParkCostType>().eq("cost_type", cash.getCostType())));*/
		}
	}
}
