package org.jeecg.community.service.impl;

import org.jeecg.community.entity.XyParkCustomercontact;
import org.jeecg.community.mapper.XyParkCustomercontactMapper;
import org.jeecg.community.service.IXyParkCustomercontactService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class XyParkCustomercontactServiceImpl extends ServiceImpl<XyParkCustomercontactMapper, XyParkCustomercontact> implements IXyParkCustomercontactService {
	
	@Autowired
	private XyParkCustomercontactMapper xyParkCustomercontactMapper;
	
	@Override
	public List<XyParkCustomercontact> selectByMainId(String mainId) {
		return xyParkCustomercontactMapper.selectByMainId(mainId);
	}
}
