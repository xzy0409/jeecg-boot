package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkLeasecontractDetail;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 租赁合同房源信息
 * @Author: jeecg-boot
 * @Date:   2022-04-13
 * @Version: V1.0
 */
public interface IXyParkLeasecontractDetailService extends IService<XyParkLeasecontractDetail> {

	public List<XyParkLeasecontractDetail> selectByMainId(String mainId);
}
