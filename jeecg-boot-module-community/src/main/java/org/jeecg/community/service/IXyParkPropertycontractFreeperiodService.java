package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkPropertycontractFreeperiod;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 免租期
 * @Author: jeecg-boot
 * @Date:   2022-04-13
 * @Version: V1.0
 */
public interface IXyParkPropertycontractFreeperiodService extends IService<XyParkPropertycontractFreeperiod> {

	public List<XyParkPropertycontractFreeperiod> selectByMainId(String mainId);
}
