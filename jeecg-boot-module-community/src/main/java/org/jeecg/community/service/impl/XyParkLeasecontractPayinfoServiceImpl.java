package org.jeecg.community.service.impl;

import org.jeecg.community.entity.XyParkLeasecontractPayinfo;
import org.jeecg.community.mapper.XyParkLeasecontractPayinfoMapper;
import org.jeecg.community.service.IXyParkLeasecontractPayinfoService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 租赁合同款项信息
 * @Author: jeecg-boot
 * @Date:   2022-04-13
 * @Version: V1.0
 */
@Service
public class XyParkLeasecontractPayinfoServiceImpl extends ServiceImpl<XyParkLeasecontractPayinfoMapper, XyParkLeasecontractPayinfo> implements IXyParkLeasecontractPayinfoService {
	
	@Autowired
	private XyParkLeasecontractPayinfoMapper xyParkLeasecontractPayinfoMapper;
	
	@Override
	public List<XyParkLeasecontractPayinfo> selectByMainId(String mainId) {
		return xyParkLeasecontractPayinfoMapper.selectByMainId(mainId);
	}
}
