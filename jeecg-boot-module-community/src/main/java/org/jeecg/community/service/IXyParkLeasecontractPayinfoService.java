package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkLeasecontractPayinfo;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 租赁合同款项信息
 * @Author: jeecg-boot
 * @Date:   2022-04-13
 * @Version: V1.0
 */
public interface IXyParkLeasecontractPayinfoService extends IService<XyParkLeasecontractPayinfo> {

	public List<XyParkLeasecontractPayinfo> selectByMainId(String mainId);
}
