package org.jeecg.community.service.impl;

import org.jeecg.community.entity.XyParkLeasecontractFreeperiod;
import org.jeecg.community.mapper.XyParkLeasecontractFreeperiodMapper;
import org.jeecg.community.service.IXyParkLeasecontractFreeperiodService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 免租期
 * @Author: jeecg-boot
 * @Date:   2022-04-13
 * @Version: V1.0
 */
@Service
public class XyParkLeasecontractFreeperiodServiceImpl extends ServiceImpl<XyParkLeasecontractFreeperiodMapper, XyParkLeasecontractFreeperiod> implements IXyParkLeasecontractFreeperiodService {
	
	@Autowired
	private XyParkLeasecontractFreeperiodMapper xyParkLeasecontractFreeperiodMapper;
	
	@Override
	public List<XyParkLeasecontractFreeperiod> selectByMainId(String mainId) {
		return xyParkLeasecontractFreeperiodMapper.selectByMainId(mainId);
	}
}
