package org.jeecg.community.service;

import java.util.List;
import org.jeecg.community.entity.XyParkBill;
import org.jeecg.community.entity.XyParkCash;
import org.jeecg.community.entity.XyParkCashBillShip;
import org.jeecg.community.vo.XyParkBillVo;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

public interface IXyParkBillService extends IService<XyParkBill> {

	void handleDetails(IPage<XyParkBill> pageList);

	void approvalByIds(List<String> idList, String status);

	List<XyParkBillVo> getCashBillShipByAmount(XyParkCash cash);

	void updateStatus(List<XyParkCashBillShip> ships);

	void updateStatus(XyParkCashBillShip ship);

}
