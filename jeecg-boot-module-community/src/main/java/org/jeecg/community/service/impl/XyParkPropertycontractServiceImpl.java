package org.jeecg.community.service.impl;

import org.jeecg.community.entity.XyParkPropertycontract;
import org.jeecg.community.entity.XyParkPropertycontractDetail;
import org.jeecg.community.entity.XyParkPropertycontractPayinfo;
import org.jeecg.community.entity.XyParkPropertycontractFreeperiod;
import org.jeecg.community.mapper.XyParkPropertycontractDetailMapper;
import org.jeecg.community.mapper.XyParkPropertycontractPayinfoMapper;
import org.jeecg.community.mapper.XyParkPropertycontractFreeperiodMapper;
import org.jeecg.community.mapper.XyParkPropertycontractMapper;
import org.jeecg.community.service.IXyParkPropertycontractService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 新亚园区物业合同
 * @Author: jeecg-boot
 * @Date:   2022-04-13
 * @Version: V1.0
 */
@Service
public class XyParkPropertycontractServiceImpl extends ServiceImpl<XyParkPropertycontractMapper, XyParkPropertycontract> implements IXyParkPropertycontractService {

	@Autowired
	private XyParkPropertycontractMapper xyParkPropertycontractMapper;
	@Autowired
	private XyParkPropertycontractDetailMapper xyParkPropertycontractDetailMapper;
	@Autowired
	private XyParkPropertycontractPayinfoMapper xyParkPropertycontractPayinfoMapper;
	@Autowired
	private XyParkPropertycontractFreeperiodMapper xyParkPropertycontractFreeperiodMapper;
	
	@Override
	@Transactional
	public void saveMain(XyParkPropertycontract xyParkPropertycontract, List<XyParkPropertycontractDetail> xyParkPropertycontractDetailList,List<XyParkPropertycontractPayinfo> xyParkPropertycontractPayinfoList,List<XyParkPropertycontractFreeperiod> xyParkPropertycontractFreeperiodList) {
		xyParkPropertycontractMapper.insert(xyParkPropertycontract);
		if(xyParkPropertycontractDetailList!=null && xyParkPropertycontractDetailList.size()>0) {
			for(XyParkPropertycontractDetail entity:xyParkPropertycontractDetailList) {
				//外键设置
				entity.setContractCode(xyParkPropertycontract.getContractCode());
				xyParkPropertycontractDetailMapper.insert(entity);
			}
		}
		if(xyParkPropertycontractPayinfoList!=null && xyParkPropertycontractPayinfoList.size()>0) {
			for(XyParkPropertycontractPayinfo entity:xyParkPropertycontractPayinfoList) {
				//外键设置
				entity.setContractCode(xyParkPropertycontract.getContractCode());
				xyParkPropertycontractPayinfoMapper.insert(entity);
			}
		}
		if(xyParkPropertycontractFreeperiodList!=null && xyParkPropertycontractFreeperiodList.size()>0) {
			for(XyParkPropertycontractFreeperiod entity:xyParkPropertycontractFreeperiodList) {
				//外键设置
				entity.setPid(xyParkPropertycontract.getContractCode());
				xyParkPropertycontractFreeperiodMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void updateMain(XyParkPropertycontract xyParkPropertycontract,List<XyParkPropertycontractDetail> xyParkPropertycontractDetailList,List<XyParkPropertycontractPayinfo> xyParkPropertycontractPayinfoList,List<XyParkPropertycontractFreeperiod> xyParkPropertycontractFreeperiodList) {
		xyParkPropertycontractMapper.updateById(xyParkPropertycontract);
		
		//1.先删除子表数据
		xyParkPropertycontractDetailMapper.deleteByMainId(xyParkPropertycontract.getContractCode());
		xyParkPropertycontractDetailMapper.deleteByMainId(xyParkPropertycontract.getContractCode());
		xyParkPropertycontractDetailMapper.deleteByMainId(xyParkPropertycontract.getContractCode());
		xyParkPropertycontractPayinfoMapper.deleteByMainId(xyParkPropertycontract.getContractCode());
		xyParkPropertycontractPayinfoMapper.deleteByMainId(xyParkPropertycontract.getContractCode());
		xyParkPropertycontractPayinfoMapper.deleteByMainId(xyParkPropertycontract.getContractCode());
		xyParkPropertycontractFreeperiodMapper.deleteByMainId(xyParkPropertycontract.getContractCode());
		xyParkPropertycontractFreeperiodMapper.deleteByMainId(xyParkPropertycontract.getContractCode());
		xyParkPropertycontractFreeperiodMapper.deleteByMainId(xyParkPropertycontract.getContractCode());
		
		//2.子表数据重新插入
		if(xyParkPropertycontractDetailList!=null && xyParkPropertycontractDetailList.size()>0) {
			for(XyParkPropertycontractDetail entity:xyParkPropertycontractDetailList) {
				//外键设置
				entity.setContractCode(xyParkPropertycontract.getContractCode());
				xyParkPropertycontractDetailMapper.insert(entity);
			}
		}
		if(xyParkPropertycontractPayinfoList!=null && xyParkPropertycontractPayinfoList.size()>0) {
			for(XyParkPropertycontractPayinfo entity:xyParkPropertycontractPayinfoList) {
				//外键设置
				entity.setContractCode(xyParkPropertycontract.getContractCode());
				xyParkPropertycontractPayinfoMapper.insert(entity);
			}
		}
		if(xyParkPropertycontractFreeperiodList!=null && xyParkPropertycontractFreeperiodList.size()>0) {
			for(XyParkPropertycontractFreeperiod entity:xyParkPropertycontractFreeperiodList) {
				entity.setPid(xyParkPropertycontract.getContractCode());
				xyParkPropertycontractFreeperiodMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void delMain(String id) {
		xyParkPropertycontractDetailMapper.deleteByMainId(id);
		xyParkPropertycontractPayinfoMapper.deleteByMainId(id);
		xyParkPropertycontractFreeperiodMapper.deleteByMainId(id);
		xyParkPropertycontractMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			xyParkPropertycontractDetailMapper.deleteByMainId(id.toString());
			xyParkPropertycontractPayinfoMapper.deleteByMainId(id.toString());
			xyParkPropertycontractFreeperiodMapper.deleteByMainId(id.toString());
			xyParkPropertycontractMapper.deleteById(id);
		}
	}

	@Override
	@Transactional
	public void invalid(String id) {
		xyParkPropertycontractMapper.invalid(id);
	}
	
}
