package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkLeasecontractDetail;
import org.jeecg.community.entity.XyParkLeasecontractPayinfo;
import org.jeecg.community.entity.XyParkLeasecontractFreeperiod;
import org.jeecg.community.entity.XyParkLeasecontract;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 新亚园区租赁合同
 * @Author: jeecg-boot
 * @Date:   2022-04-13
 * @Version: V1.0
 */
public interface IXyParkLeasecontractService extends IService<XyParkLeasecontract> {

	/**
	 * 添加一对多
	 * 
	 */
	public void saveMain(XyParkLeasecontract xyParkLeasecontract, List<XyParkLeasecontractDetail> xyParkLeasecontractDetailList, List<XyParkLeasecontractPayinfo> xyParkLeasecontractPayinfoList, List<XyParkLeasecontractFreeperiod> xyParkLeasecontractFreeperiodList) ;
	
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(XyParkLeasecontract xyParkLeasecontract, List<XyParkLeasecontractDetail> xyParkLeasecontractDetailList, List<XyParkLeasecontractPayinfo> xyParkLeasecontractPayinfoList, List<XyParkLeasecontractFreeperiod> xyParkLeasecontractFreeperiodList);
	
	/**
	 * 删除一对多
	 */
	public void delMain(String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain(Collection<? extends Serializable> idList);
	
	/**
	 * 作废合同
	 * @param id
	 */
	public void invalid(String id);
	
}
