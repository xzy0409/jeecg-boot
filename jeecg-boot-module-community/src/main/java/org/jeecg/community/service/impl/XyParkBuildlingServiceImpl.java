package org.jeecg.community.service.impl;

import org.jeecg.community.entity.XyParkBuildling;
import org.jeecg.community.entity.XyParkFloor;
import org.jeecg.community.mapper.XyParkFloorMapper;
import org.jeecg.community.mapper.XyParkBuildlingMapper;
import org.jeecg.community.service.IXyParkBuildlingService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

@Service
public class XyParkBuildlingServiceImpl extends ServiceImpl<XyParkBuildlingMapper, XyParkBuildling> implements IXyParkBuildlingService {

	@Autowired
	private XyParkBuildlingMapper xyParkBuildlingMapper;
	@Autowired
	private XyParkFloorMapper xyParkFloorMapper;
	
	@Override
	@Transactional
	public void saveMain(XyParkBuildling xyParkBuildling, List<XyParkFloor> xyParkFloorList) {
		xyParkBuildlingMapper.insert(xyParkBuildling);
		if(xyParkFloorList!=null && xyParkFloorList.size()>0) {
			for(XyParkFloor entity:xyParkFloorList) {
				//外键设置
				entity.setBuildingCode(xyParkBuildling.getBuildingCode());
				xyParkFloorMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void updateMain(XyParkBuildling xyParkBuildling,List<XyParkFloor> xyParkFloorList) {
		xyParkBuildlingMapper.updateById(xyParkBuildling);
		
		//1.先删除子表数据
		xyParkFloorMapper.deleteByMainId(xyParkBuildling.getBuildingCode());
		
		//2.子表数据重新插入
		if(xyParkFloorList!=null && xyParkFloorList.size()>0) {
			for(XyParkFloor entity:xyParkFloorList) {
				//外键设置
				entity.setBuildingCode(xyParkBuildling.getBuildingCode());
				xyParkFloorMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void delMain(String id) {
		xyParkFloorMapper.deleteByMainId(id);
		xyParkBuildlingMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			xyParkFloorMapper.deleteByMainId(id.toString());
			xyParkBuildlingMapper.deleteById(id);
		}
	}
	
}
