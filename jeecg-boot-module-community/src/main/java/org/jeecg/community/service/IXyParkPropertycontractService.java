package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkPropertycontractDetail;
import org.jeecg.community.entity.XyParkPropertycontractPayinfo;
import org.jeecg.community.entity.XyParkPropertycontractFreeperiod;
import org.jeecg.community.entity.XyParkPropertycontract;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 新亚园区物业合同
 * @Author: jeecg-boot
 * @Date:   2022-04-13
 * @Version: V1.0
 */
public interface IXyParkPropertycontractService extends IService<XyParkPropertycontract> {

	/**
	 * 添加一对多
	 * 
	 */
	public void saveMain(XyParkPropertycontract xyParkPropertycontract, List<XyParkPropertycontractDetail> xyParkPropertycontractDetailList, List<XyParkPropertycontractPayinfo> xyParkPropertycontractPayinfoList, List<XyParkPropertycontractFreeperiod> xyParkPropertycontractFreeperiodList) ;
	
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(XyParkPropertycontract xyParkPropertycontract, List<XyParkPropertycontractDetail> xyParkPropertycontractDetailList, List<XyParkPropertycontractPayinfo> xyParkPropertycontractPayinfoList, List<XyParkPropertycontractFreeperiod> xyParkPropertycontractFreeperiodList);
	
	/**
	 * 删除一对多
	 */
	public void delMain(String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain(Collection<? extends Serializable> idList);

	/**
	 * 作废合同
	 * @param id
	 */
	public void invalid(String id);
	
}
