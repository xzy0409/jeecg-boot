package org.jeecg.community.service.impl;

import org.jeecg.community.entity.XyParkPropertycontractPayinfo;
import org.jeecg.community.mapper.XyParkPropertycontractPayinfoMapper;
import org.jeecg.community.service.IXyParkPropertycontractPayinfoService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 物业合同款项信息
 * @Author: jeecg-boot
 * @Date:   2022-04-13
 * @Version: V1.0
 */
@Service
public class XyParkPropertycontractPayinfoServiceImpl extends ServiceImpl<XyParkPropertycontractPayinfoMapper, XyParkPropertycontractPayinfo> implements IXyParkPropertycontractPayinfoService {
	
	@Autowired
	private XyParkPropertycontractPayinfoMapper xyParkPropertycontractPayinfoMapper;
	
	@Override
	public List<XyParkPropertycontractPayinfo> selectByMainId(String mainId) {
		return xyParkPropertycontractPayinfoMapper.selectByMainId(mainId);
	}
}
