package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkCash;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface IXyParkCashService extends IService<XyParkCash> {

	void handleDetails(IPage<XyParkCash> pageList);

    void handleDetailsWithOwner(List<XyParkCash> pageList);
}
