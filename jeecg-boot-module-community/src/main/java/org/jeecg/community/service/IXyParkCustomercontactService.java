package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkCustomercontact;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * 客户联系人
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface IXyParkCustomercontactService extends IService<XyParkCustomercontact> {

	public List<XyParkCustomercontact> selectByMainId(String mainId);
}
