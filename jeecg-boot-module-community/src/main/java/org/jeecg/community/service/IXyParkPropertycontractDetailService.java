package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkPropertycontractDetail;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 物业合同房源信息
 * @Author: jeecg-boot
 * @Date:   2022-04-13
 * @Version: V1.0
 */
public interface IXyParkPropertycontractDetailService extends IService<XyParkPropertycontractDetail> {

	public List<XyParkPropertycontractDetail> selectByMainId(String mainId);
}
