package org.jeecg.community.service.impl;

import org.jeecg.community.entity.XyParkLeasecontractDetail;
import org.jeecg.community.mapper.XyParkLeasecontractDetailMapper;
import org.jeecg.community.service.IXyParkLeasecontractDetailService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 租赁合同房源信息
 * @Author: jeecg-boot
 * @Date:   2022-04-13
 * @Version: V1.0
 */
@Service
public class XyParkLeasecontractDetailServiceImpl extends ServiceImpl<XyParkLeasecontractDetailMapper, XyParkLeasecontractDetail> implements IXyParkLeasecontractDetailService {
	
	@Autowired
	private XyParkLeasecontractDetailMapper xyParkLeasecontractDetailMapper;
	
	@Override
	public List<XyParkLeasecontractDetail> selectByMainId(String mainId) {
		return xyParkLeasecontractDetailMapper.selectByMainId(mainId);
	}
}
