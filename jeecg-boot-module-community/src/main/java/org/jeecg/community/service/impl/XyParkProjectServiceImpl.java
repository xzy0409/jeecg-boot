package org.jeecg.community.service.impl;

import org.jeecg.community.entity.XyParkProject;
import org.jeecg.community.mapper.XyParkProjectMapper;
import org.jeecg.community.service.IXyParkProjectService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class XyParkProjectServiceImpl extends ServiceImpl<XyParkProjectMapper, XyParkProject> implements IXyParkProjectService {

}
