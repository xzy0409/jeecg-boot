package org.jeecg.community.vo;

import java.util.List;
import org.jeecg.community.entity.XyParkPropertycontractDetail;
import org.jeecg.community.entity.XyParkPropertycontractPayinfo;
import org.jeecg.community.entity.XyParkPropertycontractFreeperiod;
import lombok.Data;
import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.autopoi.poi.excel.annotation.ExcelCollection;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 新亚园区物业合同
 * @Author: jeecg-boot
 * @Date:   2022-04-13
 * @Version: V1.0
 */
@Data
@ApiModel(value="xy_park_propertycontractPage对象", description="新亚园区物业合同")
public class XyParkPropertycontractPage {
	/**主键*/
	@ApiModelProperty(value = "主键")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**合同编号*/
	@Excel(name = "合同编号", width = 15)
	@ApiModelProperty(value = "合同编号")
	private java.lang.String contractCode;
	/**甲方编码*/
	@Excel(name = "甲方编码", width = 15)
	@ApiModelProperty(value = "甲方编码")
	private java.lang.String item;
	/**甲方（物业公司）*/
	@Excel(name = "甲方（物业公司）", width = 15)
	@ApiModelProperty(value = "甲方（物业公司）")
	private java.lang.String company;
	/**开户银行*/
	@Excel(name = "开户银行", width = 15)
	@ApiModelProperty(value = "开户银行")
	private java.lang.String depositPark;
	/**开户账号*/
	@Excel(name = "开户账号", width = 15)
	@ApiModelProperty(value = "开户账号")
	private java.lang.String parkNumber;
	/**业主编号*/
	@Excel(name = "业主编号", width = 15)
	@ApiModelProperty(value = "业主编号")
	private java.lang.String ownerCode;
	/**业主名称*/
	@Excel(name = "业主名称", width = 15)
	@ApiModelProperty(value = "业主名称")
	private java.lang.String ownerName;
	/**社会信用代码*/
	@Excel(name = "社会信用代码", width = 15)
	@ApiModelProperty(value = "社会信用代码")
	private java.lang.String socialCreditcode;
	/**身份证号*/
	@Excel(name = "身份证号", width = 15)
	@ApiModelProperty(value = "身份证号")
	private java.lang.String idNumber;
	/**联系电话*/
	@Excel(name = "联系电话", width = 15)
	@ApiModelProperty(value = "联系电话")
	private java.lang.String contactMethod;
	/**项目编号*/
	@Excel(name = "项目编号", width = 15)
	@ApiModelProperty(value = "项目编号")
	private java.lang.String projectCode;
	/**项目名称*/
	@Excel(name = "项目名称", width = 15)
	@ApiModelProperty(value = "项目名称")
	private java.lang.String projectName;
	/**服务面积*/
	@Excel(name = "服务面积", width = 15)
	@ApiModelProperty(value = "服务面积")
	private java.math.BigDecimal serviceArea;
	/**综合服务费总额（小写）*/
	@Excel(name = "综合服务费总额（小写）", width = 15)
	@ApiModelProperty(value = "综合服务费总额（小写）")
	private java.math.BigDecimal totalAmount;
	/**综合服务费总额（大写）*/
	@Excel(name = "综合服务费总额（大写）", width = 15)
	@ApiModelProperty(value = "综合服务费总额（大写）")
	private java.lang.String amountDetail;
	/**综合服务费标准*/
	@Excel(name = "综合服务费标准", width = 15)
	@ApiModelProperty(value = "综合服务费标准")
	private java.math.BigDecimal amountStandard;
	/**服务开始时间*/
	@Excel(name = "服务开始时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "服务开始时间")
	private java.util.Date serviceStarttime;
	/**服务结束时间*/
	@Excel(name = "服务结束时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "服务结束时间")
	private java.util.Date serviceEndtime;
	/**缴费方式*/
	@Excel(name = "缴费方式", width = 15, dicCode = "xy_park_contract_cycle")
	@Dict(dicCode = "xy_park_contract_cycle")
	@ApiModelProperty(value = "缴费方式")
	private java.lang.String paymentMethod;
	/**装修保证金*/
	@Excel(name = "装修保证金", width = 15)
	@ApiModelProperty(value = "装修保证金")
	private java.math.BigDecimal earnestMoney;
	/**单据状态*/
	@Excel(name = "单据状态", width = 15)
	@ApiModelProperty(value = "单据状态")
	private java.lang.String bpmStatus;
	/**合同类型*/
	@Excel(name = "合同类型", width = 15, dicCode = "xy_park_contract_type")
	@Dict(dicCode = "xy_park_contract_type")
	@ApiModelProperty(value = "合同类型")
	private java.lang.String contractType;
	/**是否三七*/
	@Excel(name = "是否三七", width = 15)
	@ApiModelProperty(value = "是否三七")
	private java.lang.String isThreeSeven;
	/**合同附件*/
	@Excel(name = "合同附件", width = 15)
	@ApiModelProperty(value = "合同附件")
	private java.lang.String contractFile;
	/**合同状态*/
	@Excel(name = "合同状态", width = 15)
    @ApiModelProperty(value = "合同状态")
	private String contractStatus;

	@ExcelCollection(name="物业合同房源信息")
	@ApiModelProperty(value = "物业合同房源信息")
	private List<XyParkPropertycontractDetail> xyParkPropertycontractDetailList;
	@ExcelCollection(name="物业合同款项信息")
	@ApiModelProperty(value = "物业合同款项信息")
	private List<XyParkPropertycontractPayinfo> xyParkPropertycontractPayinfoList;
	@ExcelCollection(name="免租期")
	@ApiModelProperty(value = "免租期")
	private List<XyParkPropertycontractFreeperiod> xyParkPropertycontractFreeperiodList;

}
