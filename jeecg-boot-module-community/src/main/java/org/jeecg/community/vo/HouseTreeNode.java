package org.jeecg.community.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Author : 吴盼盼
 * @Datetime : 2022/1/14 20:12
 * @Desc :
 * @Modor :  Modifytime:
 * @modDesc :
 */
@Data
public class HouseTreeNode {

    @ApiModelProperty(value = "主键")
    private String id;
    @ApiModelProperty(value = "key")
    private String key;
    @ApiModelProperty(value = "value")
    private String value;

    @ApiModelProperty(value = "title显示的文本")
    private String title;

    @ApiModelProperty(value = "state节点状态，'open' 或 'closed'，默认：'open'。如果为'closed'的时候，将不自动展开该节点。")
    private String state="open";

    @ApiModelProperty(value = "节点图标id")
    private String iconCls;

    @ApiModelProperty(value = "是否被选中")
    private boolean checked;

    @ApiModelProperty(value = "自定义属性")
    private String attributes;

    @ApiModelProperty(value = "层级：0：项目、1：楼宇、2：楼层")
    private Integer level;

    @ApiModelProperty(value = "子节点")
    private List<HouseTreeNode> children;
}
