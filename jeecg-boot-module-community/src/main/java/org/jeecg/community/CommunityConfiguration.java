package org.jeecg.community;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 园区物业模块
 * @author MxpIO
 *
 */
@Configuration
@ComponentScan
@AutoConfigurationPackage
@MapperScan({"org.jeecg.community.mapper"})
public class CommunityConfiguration {
	
}