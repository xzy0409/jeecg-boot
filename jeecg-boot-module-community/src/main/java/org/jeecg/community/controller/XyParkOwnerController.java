package org.jeecg.community.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecg.autopoi.poi.excel.ExcelImportUtil;
import org.jeecg.autopoi.poi.excel.def.NormalExcelConstants;
import org.jeecg.autopoi.poi.excel.entity.ExportParams;
import org.jeecg.autopoi.poi.excel.entity.ImportParams;
import org.jeecg.autopoi.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.vo.LoginUser;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.community.entity.XyParkOwnercontact;
import org.jeecg.community.entity.XyParkOwnerbusiness;
import org.jeecg.community.entity.XyParkOwnerbillinginfo;
import org.jeecg.community.entity.XyParkOwner;
import org.jeecg.community.vo.XyParkOwnerPage;
import org.jeecg.community.service.IXyParkOwnerService;
import org.jeecg.community.service.IXyParkOwnercontactService;
import org.jeecg.community.service.IXyParkOwnerbusinessService;
import org.jeecg.community.service.IXyParkOwnerbillinginfoService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * 业主信息表
 * @author: jeecg-boot
 * @version: V1.0
 */
@Api(tags="业主信息表")
@RestController
@RequestMapping("/community/xyParkOwner")
@Slf4j
public class XyParkOwnerController {
	@Autowired
	private IXyParkOwnerService xyParkOwnerService;
	@Autowired
	private IXyParkOwnercontactService xyParkOwnercontactService;
	@Autowired
	private IXyParkOwnerbusinessService xyParkOwnerbusinessService;
	@Autowired
	private IXyParkOwnerbillinginfoService xyParkOwnerbillinginfoService;
	
	/**
	 * 分页列表查询
	 *
	 * @param xyParkOwner
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "业主信息表-分页列表查询")
	@ApiOperation(value="业主信息表-分页列表查询", notes="业主信息表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(XyParkOwner xyParkOwner,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<XyParkOwner> queryWrapper = QueryGenerator.initQueryWrapper(xyParkOwner, req.getParameterMap());
		Page<XyParkOwner> page = new Page<XyParkOwner>(pageNo, pageSize);
		IPage<XyParkOwner> pageList = xyParkOwnerService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param xyParkOwnerPage
	 * @return
	 */
	@AutoLog(value = "业主信息表-添加")
	@ApiOperation(value="业主信息表-添加", notes="业主信息表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody XyParkOwnerPage xyParkOwnerPage) {
		XyParkOwner xyParkOwner = new XyParkOwner();
		BeanUtils.copyProperties(xyParkOwnerPage, xyParkOwner);
		xyParkOwnerService.saveMain(xyParkOwner, xyParkOwnerPage.getXyParkOwnercontactList(),xyParkOwnerPage.getXyParkOwnerbusinessList(),xyParkOwnerPage.getXyParkOwnerbillinginfoList());
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param xyParkOwnerPage
	 * @return
	 */
	@AutoLog(value = "业主信息表-编辑")
	@ApiOperation(value="业主信息表-编辑", notes="业主信息表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody XyParkOwnerPage xyParkOwnerPage) {
		XyParkOwner xyParkOwner = new XyParkOwner();
		BeanUtils.copyProperties(xyParkOwnerPage, xyParkOwner);
		XyParkOwner xyParkOwnerEntity = xyParkOwnerService.getById(xyParkOwner.getId());
		if(xyParkOwnerEntity==null) {
			return Result.error("未找到对应数据");
		}
		xyParkOwnerService.updateMain(xyParkOwner, xyParkOwnerPage.getXyParkOwnercontactList(),xyParkOwnerPage.getXyParkOwnerbusinessList(),xyParkOwnerPage.getXyParkOwnerbillinginfoList());
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "业主信息表-通过id删除")
	@ApiOperation(value="业主信息表-通过id删除", notes="业主信息表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		xyParkOwnerService.delMain(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "业主信息表-批量删除")
	@ApiOperation(value="业主信息表-批量删除", notes="业主信息表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.xyParkOwnerService.delBatchMain(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "业主信息表-通过id查询")
	@ApiOperation(value="业主信息表-通过id查询", notes="业主信息表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		XyParkOwner xyParkOwner = xyParkOwnerService.getById(id);
		if(xyParkOwner==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(xyParkOwner);

	}
	
	/**
	 * 通过id查询
	 *
	 * @param ownerCode
	 * @return
	 */
	@AutoLog(value = "业主联系人信息通过主表ID查询")
	@ApiOperation(value="业主联系人信息主表ID查询", notes="业主联系人信息-通主表ID查询")
	@GetMapping(value = "/queryXyParkOwnercontactByMainId")
	public Result<?> queryXyParkOwnercontactListByMainId(@RequestParam(name="ownerCode",required=true) String ownerCode) {
		List<XyParkOwnercontact> xyParkOwnercontactList = xyParkOwnercontactService.selectByMainId(ownerCode);
		return Result.OK(xyParkOwnercontactList);
	}
	/**
	 * 通过id查询
	 *
	 * @param ownerCode
	 * @return
	 */
	@AutoLog(value = "业主工商信息通过主表ID查询")
	@ApiOperation(value="业主工商信息主表ID查询", notes="业主工商信息-通主表ID查询")
	@GetMapping(value = "/queryXyParkOwnerbusinessByMainId")
	public Result<?> queryXyParkOwnerbusinessListByMainId(@RequestParam(name="ownerCode",required=true) String ownerCode) {
		List<XyParkOwnerbusiness> xyParkOwnerbusinessList = xyParkOwnerbusinessService.selectByMainId(ownerCode);
		return Result.OK(xyParkOwnerbusinessList);
	}
	/**
	 * 通过id查询
	 *
	 * @param ownerCode
	 * @return
	 */
	@AutoLog(value = "业主开票信息表通过主表ID查询")
	@ApiOperation(value="业主开票信息表主表ID查询", notes="业主开票信息表-通主表ID查询")
	@GetMapping(value = "/queryXyParkOwnerbillinginfoByMainId")
	public Result<?> queryXyParkOwnerbillinginfoListByMainId(@RequestParam(name="ownerCode",required=true) String ownerCode) {
		List<XyParkOwnerbillinginfo> xyParkOwnerbillinginfoList = xyParkOwnerbillinginfoService.selectByMainId(ownerCode);
		return Result.OK(xyParkOwnerbillinginfoList);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param xyParkOwner
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, XyParkOwner xyParkOwner) {
      // Step.1 组装查询条件查询数据
      QueryWrapper<XyParkOwner> queryWrapper = QueryGenerator.initQueryWrapper(xyParkOwner, request.getParameterMap());
      LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

      //Step.2 获取导出数据
      List<XyParkOwner> queryList = xyParkOwnerService.list(queryWrapper);
      // 过滤选中数据
      String selections = request.getParameter("selections");
      List<XyParkOwner> xyParkOwnerList = new ArrayList<XyParkOwner>();
      if(oConvertUtils.isEmpty(selections)) {
          xyParkOwnerList = queryList;
      }else {
          List<String> selectionList = Arrays.asList(selections.split(","));
          xyParkOwnerList = queryList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
      }

      // Step.3 组装pageList
      List<XyParkOwnerPage> pageList = new ArrayList<XyParkOwnerPage>();
      for (XyParkOwner main : xyParkOwnerList) {
          XyParkOwnerPage vo = new XyParkOwnerPage();
          BeanUtils.copyProperties(main, vo);
          List<XyParkOwnercontact> xyParkOwnercontactList = xyParkOwnercontactService.selectByMainId(main.getId());
          vo.setXyParkOwnercontactList(xyParkOwnercontactList);
          List<XyParkOwnerbusiness> xyParkOwnerbusinessList = xyParkOwnerbusinessService.selectByMainId(main.getId());
          vo.setXyParkOwnerbusinessList(xyParkOwnerbusinessList);
          List<XyParkOwnerbillinginfo> xyParkOwnerbillinginfoList = xyParkOwnerbillinginfoService.selectByMainId(main.getId());
          vo.setXyParkOwnerbillinginfoList(xyParkOwnerbillinginfoList);
          pageList.add(vo);
      }

      // Step.4 AutoPoi 导出Excel
      ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
      mv.addObject(NormalExcelConstants.FILE_NAME, "业主信息表列表");
      mv.addObject(NormalExcelConstants.CLASS, XyParkOwnerPage.class);
      mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("业主信息表数据", "导出人:"+sysUser.getRealname(), "业主信息表"));
      mv.addObject(NormalExcelConstants.DATA_LIST, pageList);
      return mv;
    }

    /**
    * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
      Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
      for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
          MultipartFile file = entity.getValue();// 获取上传文件对象
          ImportParams params = new ImportParams();
          params.setTitleRows(2);
          params.setHeadRows(1);
          params.setNeedSave(true);
          try {
              List<XyParkOwnerPage> list = ExcelImportUtil.importExcel(file.getInputStream(), XyParkOwnerPage.class, params);
              for (XyParkOwnerPage page : list) {
                  XyParkOwner po = new XyParkOwner();
                  BeanUtils.copyProperties(page, po);
                  xyParkOwnerService.saveMain(po, page.getXyParkOwnercontactList(),page.getXyParkOwnerbusinessList(),page.getXyParkOwnerbillinginfoList());
              }
              return Result.OK("文件导入成功！数据行数:" + list.size());
          } catch (Exception e) {
              log.error(e.getMessage(),e);
              return Result.error("文件导入失败:"+e.getMessage());
          } finally {
              try {
                  file.getInputStream().close();
              } catch (IOException e) {
                  e.printStackTrace();
              }
          }
      }
      return Result.OK("文件导入失败！");
    }

}
