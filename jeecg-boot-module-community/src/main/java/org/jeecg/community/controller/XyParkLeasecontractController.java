package org.jeecg.community.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecg.autopoi.poi.excel.ExcelImportUtil;
import org.jeecg.autopoi.poi.excel.def.NormalExcelConstants;
import org.jeecg.autopoi.poi.excel.entity.ExportParams;
import org.jeecg.autopoi.poi.excel.entity.ImportParams;

import org.jeecg.autopoi.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.vo.LoginUser;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.community.entity.XyParkLeasecontractDetail;
import org.jeecg.community.entity.XyParkLeasecontractPayinfo;
import org.jeecg.community.entity.XyParkLeasecontractFreeperiod;
import org.jeecg.community.entity.XyParkLeasecontract;
import org.jeecg.community.vo.XyParkLeasecontractPage;
import org.jeecg.community.service.IXyParkLeasecontractService;
import org.jeecg.community.service.IXyParkLeasecontractDetailService;
import org.jeecg.community.service.IXyParkLeasecontractPayinfoService;
import org.jeecg.community.service.IXyParkLeasecontractFreeperiodService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 新亚园区租赁合同
 * @Author: jeecg-boot
 * @Date:   2022-04-13
 * @Version: V1.0
 */
@Api(tags="新亚园区租赁合同")
@RestController
@RequestMapping("/community/xyParkLeasecontract")
@Slf4j
public class XyParkLeasecontractController {
	@Autowired
	private IXyParkLeasecontractService xyParkLeasecontractService;
	@Autowired
	private IXyParkLeasecontractDetailService xyParkLeasecontractDetailService;
	@Autowired
	private IXyParkLeasecontractPayinfoService xyParkLeasecontractPayinfoService;
	@Autowired
	private IXyParkLeasecontractFreeperiodService xyParkLeasecontractFreeperiodService;
	
	/**
	 * 分页列表查询
	 *
	 * @param xyParkLeasecontract
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "新亚园区租赁合同-分页列表查询")
	@ApiOperation(value="新亚园区租赁合同-分页列表查询", notes="新亚园区租赁合同-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(XyParkLeasecontract xyParkLeasecontract,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<XyParkLeasecontract> queryWrapper = QueryGenerator.initQueryWrapper(xyParkLeasecontract, req.getParameterMap());
		Page<XyParkLeasecontract> page = new Page<XyParkLeasecontract>(pageNo, pageSize);
		IPage<XyParkLeasecontract> pageList = xyParkLeasecontractService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param xyParkLeasecontractPage
	 * @return
	 */
	@AutoLog(value = "新亚园区租赁合同-添加")
	@ApiOperation(value="新亚园区租赁合同-添加", notes="新亚园区租赁合同-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody XyParkLeasecontractPage xyParkLeasecontractPage) {
		XyParkLeasecontract xyParkLeasecontract = new XyParkLeasecontract();
		BeanUtils.copyProperties(xyParkLeasecontractPage, xyParkLeasecontract);
        xyParkLeasecontract.setBpmStatus("0");
		xyParkLeasecontractService.saveMain(xyParkLeasecontract, xyParkLeasecontractPage.getXyParkLeasecontractDetailList(),xyParkLeasecontractPage.getXyParkLeasecontractPayinfoList(),xyParkLeasecontractPage.getXyParkLeasecontractFreeperiodList());
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param xyParkLeasecontractPage
	 * @return
	 */
	@AutoLog(value = "新亚园区租赁合同-编辑")
	@ApiOperation(value="新亚园区租赁合同-编辑", notes="新亚园区租赁合同-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody XyParkLeasecontractPage xyParkLeasecontractPage) {
		XyParkLeasecontract xyParkLeasecontract = new XyParkLeasecontract();
		BeanUtils.copyProperties(xyParkLeasecontractPage, xyParkLeasecontract);
		XyParkLeasecontract xyParkLeasecontractEntity = xyParkLeasecontractService.getById(xyParkLeasecontract.getId());
		if(xyParkLeasecontractEntity==null) {
			return Result.error("未找到对应数据");
		}
		xyParkLeasecontractService.updateMain(xyParkLeasecontract, xyParkLeasecontractPage.getXyParkLeasecontractDetailList(),xyParkLeasecontractPage.getXyParkLeasecontractPayinfoList(),xyParkLeasecontractPage.getXyParkLeasecontractFreeperiodList());
		return Result.OK("编辑成功!");
	}
	
	/**
	 * 作废
	 * @param id
	 * @return
	 */
	@AutoLog(value = "新亚园区物业合同-作废")
	@ApiOperation(value="新亚园区物业合同-作废", notes="新亚园区物业合同-作废")
	@PutMapping(value = "/invalid")
	public Result<?> invalid(@RequestParam(name="id",required=true) String id) {
		xyParkLeasecontractService.invalid(id);
		return Result.OK("作废成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "新亚园区租赁合同-通过id删除")
	@ApiOperation(value="新亚园区租赁合同-通过id删除", notes="新亚园区租赁合同-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		xyParkLeasecontractService.delMain(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "新亚园区租赁合同-批量删除")
	@ApiOperation(value="新亚园区租赁合同-批量删除", notes="新亚园区租赁合同-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.xyParkLeasecontractService.delBatchMain(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "新亚园区租赁合同-通过id查询")
	@ApiOperation(value="新亚园区租赁合同-通过id查询", notes="新亚园区租赁合同-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		XyParkLeasecontract xyParkLeasecontract = xyParkLeasecontractService.getById(id);
		if(xyParkLeasecontract==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(xyParkLeasecontract);

	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "租赁合同房源信息通过主表ID查询")
	@ApiOperation(value="租赁合同房源信息主表ID查询", notes="租赁合同房源信息-通主表ID查询")
	@GetMapping(value = "/queryXyParkLeasecontractDetailByMainId")
	public Result<?> queryXyParkLeasecontractDetailListByMainId(@RequestParam(name="id",required=true) String id) {
		List<XyParkLeasecontractDetail> xyParkLeasecontractDetailList = xyParkLeasecontractDetailService.selectByMainId(id);
		return Result.OK(xyParkLeasecontractDetailList);
	}
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "租赁合同款项信息通过主表ID查询")
	@ApiOperation(value="租赁合同款项信息主表ID查询", notes="租赁合同款项信息-通主表ID查询")
	@GetMapping(value = "/queryXyParkLeasecontractPayinfoByMainId")
	public Result<?> queryXyParkLeasecontractPayinfoListByMainId(@RequestParam(name="id",required=true) String id) {
		List<XyParkLeasecontractPayinfo> xyParkLeasecontractPayinfoList = xyParkLeasecontractPayinfoService.selectByMainId(id);
		return Result.OK(xyParkLeasecontractPayinfoList);
	}
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "免租期通过主表ID查询")
	@ApiOperation(value="免租期主表ID查询", notes="免租期-通主表ID查询")
	@GetMapping(value = "/queryXyParkLeasecontractFreeperiodByMainId")
	public Result<?> queryXyParkLeasecontractFreeperiodListByMainId(@RequestParam(name="id",required=true) String id) {
		List<XyParkLeasecontractFreeperiod> xyParkLeasecontractFreeperiodList = xyParkLeasecontractFreeperiodService.selectByMainId(id);
		return Result.OK(xyParkLeasecontractFreeperiodList);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param xyParkLeasecontract
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, XyParkLeasecontract xyParkLeasecontract) {
      // Step.1 组装查询条件查询数据
      QueryWrapper<XyParkLeasecontract> queryWrapper = QueryGenerator.initQueryWrapper(xyParkLeasecontract, request.getParameterMap());
      LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

      //Step.2 获取导出数据
      List<XyParkLeasecontract> queryList = xyParkLeasecontractService.list(queryWrapper);
      // 过滤选中数据
      String selections = request.getParameter("selections");
      List<XyParkLeasecontract> xyParkLeasecontractList = new ArrayList<XyParkLeasecontract>();
      if(oConvertUtils.isEmpty(selections)) {
          xyParkLeasecontractList = queryList;
      }else {
          List<String> selectionList = Arrays.asList(selections.split(","));
          xyParkLeasecontractList = queryList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
      }

      // Step.3 组装pageList
      List<XyParkLeasecontractPage> pageList = new ArrayList<XyParkLeasecontractPage>();
      for (XyParkLeasecontract main : xyParkLeasecontractList) {
          XyParkLeasecontractPage vo = new XyParkLeasecontractPage();
          BeanUtils.copyProperties(main, vo);
          List<XyParkLeasecontractDetail> xyParkLeasecontractDetailList = xyParkLeasecontractDetailService.selectByMainId(main.getId());
          vo.setXyParkLeasecontractDetailList(xyParkLeasecontractDetailList);
          List<XyParkLeasecontractPayinfo> xyParkLeasecontractPayinfoList = xyParkLeasecontractPayinfoService.selectByMainId(main.getId());
          vo.setXyParkLeasecontractPayinfoList(xyParkLeasecontractPayinfoList);
          List<XyParkLeasecontractFreeperiod> xyParkLeasecontractFreeperiodList = xyParkLeasecontractFreeperiodService.selectByMainId(main.getId());
          vo.setXyParkLeasecontractFreeperiodList(xyParkLeasecontractFreeperiodList);
          pageList.add(vo);
      }

      // Step.4 AutoPoi 导出Excel
      ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
      mv.addObject(NormalExcelConstants.FILE_NAME, "新亚园区租赁合同列表");
      mv.addObject(NormalExcelConstants.CLASS, XyParkLeasecontractPage.class);
      mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("新亚园区租赁合同数据", "导出人:"+sysUser.getRealname(), "新亚园区租赁合同"));
      mv.addObject(NormalExcelConstants.DATA_LIST, pageList);
      return mv;
    }

    /**
    * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
      Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
      for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
          MultipartFile file = entity.getValue();// 获取上传文件对象
          ImportParams params = new ImportParams();
          params.setTitleRows(2);
          params.setHeadRows(1);
          params.setNeedSave(true);
          try {
              List<XyParkLeasecontractPage> list = ExcelImportUtil.importExcel(file.getInputStream(), XyParkLeasecontractPage.class, params);
              for (XyParkLeasecontractPage page : list) {
                  XyParkLeasecontract po = new XyParkLeasecontract();
                  BeanUtils.copyProperties(page, po);
                  xyParkLeasecontractService.saveMain(po, page.getXyParkLeasecontractDetailList(),page.getXyParkLeasecontractPayinfoList(),page.getXyParkLeasecontractFreeperiodList());
              }
              return Result.OK("文件导入成功！数据行数:" + list.size());
          } catch (Exception e) {
              log.error(e.getMessage(),e);
              return Result.error("文件导入失败:"+e.getMessage());
          } finally {
              try {
                  file.getInputStream().close();
              } catch (IOException e) {
                  e.printStackTrace();
              }
          }
      }
      return Result.OK("文件导入失败！");
    }

}
