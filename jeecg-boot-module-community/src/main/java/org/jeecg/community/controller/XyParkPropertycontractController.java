package org.jeecg.community.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecg.autopoi.poi.excel.ExcelImportUtil;
import org.jeecg.autopoi.poi.excel.def.NormalExcelConstants;
import org.jeecg.autopoi.poi.excel.entity.ExportParams;
import org.jeecg.autopoi.poi.excel.entity.ImportParams;

import org.jeecg.autopoi.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.vo.LoginUser;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.community.entity.XyParkPropertycontractDetail;
import org.jeecg.community.entity.XyParkPropertycontractPayinfo;
import org.jeecg.community.entity.XyParkPropertycontractFreeperiod;
import org.jeecg.community.entity.XyParkPropertycontract;
import org.jeecg.community.vo.XyParkPropertycontractPage;
import org.jeecg.community.service.IXyParkPropertycontractService;
import org.jeecg.community.service.IXyParkPropertycontractDetailService;
import org.jeecg.community.service.IXyParkPropertycontractPayinfoService;
import org.jeecg.community.service.IXyParkPropertycontractFreeperiodService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 新亚园区物业合同
 * @Author: jeecg-boot
 * @Date:   2022-04-13
 * @Version: V1.0
 */
@Api(tags="新亚园区物业合同")
@RestController
@RequestMapping("/community/xyParkPropertycontract")
@Slf4j
public class XyParkPropertycontractController {
	@Autowired
	private IXyParkPropertycontractService xyParkPropertycontractService;
	@Autowired
	private IXyParkPropertycontractDetailService xyParkPropertycontractDetailService;
	@Autowired
	private IXyParkPropertycontractPayinfoService xyParkPropertycontractPayinfoService;
	@Autowired
	private IXyParkPropertycontractFreeperiodService xyParkPropertycontractFreeperiodService;
	
	/**
	 * 分页列表查询
	 *
	 * @param xyParkPropertycontract
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "新亚园区物业合同-分页列表查询")
	@ApiOperation(value="新亚园区物业合同-分页列表查询", notes="新亚园区物业合同-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(XyParkPropertycontract xyParkPropertycontract,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<XyParkPropertycontract> queryWrapper = QueryGenerator.initQueryWrapper(xyParkPropertycontract, req.getParameterMap());
		Page<XyParkPropertycontract> page = new Page<XyParkPropertycontract>(pageNo, pageSize);
		IPage<XyParkPropertycontract> pageList = xyParkPropertycontractService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param xyParkPropertycontractPage
	 * @return
	 */
	@AutoLog(value = "新亚园区物业合同-添加")
	@ApiOperation(value="新亚园区物业合同-添加", notes="新亚园区物业合同-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody XyParkPropertycontractPage xyParkPropertycontractPage) {
		XyParkPropertycontract xyParkPropertycontract = new XyParkPropertycontract();
		BeanUtils.copyProperties(xyParkPropertycontractPage, xyParkPropertycontract);
        xyParkPropertycontract.setBpmStatus("0");
		xyParkPropertycontractService.saveMain(xyParkPropertycontract, xyParkPropertycontractPage.getXyParkPropertycontractDetailList(),xyParkPropertycontractPage.getXyParkPropertycontractPayinfoList(),xyParkPropertycontractPage.getXyParkPropertycontractFreeperiodList());
		return Result.OK("添加成功！");
	}
	
	/**
	 * 作废
	 * @param id
	 * @return
	 */
	@AutoLog(value = "新亚园区物业合同-作废")
	@ApiOperation(value="新亚园区物业合同-作废", notes="新亚园区物业合同-作废")
	@PutMapping(value = "/invalid")
	public Result<?> invalid(@RequestParam(name="id",required=true) String id) {
		xyParkPropertycontractService.invalid(id);
		return Result.OK("作废成功!");
	}
	
	/**
	 *  编辑
	 *
	 * @param xyParkPropertycontractPage
	 * @return
	 */
	@AutoLog(value = "新亚园区物业合同-编辑")
	@ApiOperation(value="新亚园区物业合同-编辑", notes="新亚园区物业合同-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody XyParkPropertycontractPage xyParkPropertycontractPage) {
		XyParkPropertycontract xyParkPropertycontract = new XyParkPropertycontract();
		BeanUtils.copyProperties(xyParkPropertycontractPage, xyParkPropertycontract);
		XyParkPropertycontract xyParkPropertycontractEntity = xyParkPropertycontractService.getById(xyParkPropertycontract.getId());
		if(xyParkPropertycontractEntity==null) {
			return Result.error("未找到对应数据");
		}
		xyParkPropertycontractService.updateMain(xyParkPropertycontract, xyParkPropertycontractPage.getXyParkPropertycontractDetailList(),xyParkPropertycontractPage.getXyParkPropertycontractPayinfoList(),xyParkPropertycontractPage.getXyParkPropertycontractFreeperiodList());
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "新亚园区物业合同-通过id删除")
	@ApiOperation(value="新亚园区物业合同-通过id删除", notes="新亚园区物业合同-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		xyParkPropertycontractService.delMain(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "新亚园区物业合同-批量删除")
	@ApiOperation(value="新亚园区物业合同-批量删除", notes="新亚园区物业合同-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.xyParkPropertycontractService.delBatchMain(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "新亚园区物业合同-通过id查询")
	@ApiOperation(value="新亚园区物业合同-通过id查询", notes="新亚园区物业合同-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		XyParkPropertycontract xyParkPropertycontract = xyParkPropertycontractService.getById(id);
		if(xyParkPropertycontract==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(xyParkPropertycontract);

	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "物业合同房源信息通过主表ID查询")
	@ApiOperation(value="物业合同房源信息主表ID查询", notes="物业合同房源信息-通主表ID查询")
	@GetMapping(value = "/queryXyParkPropertycontractDetailByMainId")
	public Result<?> queryXyParkPropertycontractDetailListByMainId(@RequestParam(name="id",required=true) String id) {
		List<XyParkPropertycontractDetail> xyParkPropertycontractDetailList = xyParkPropertycontractDetailService.selectByMainId(id);
		return Result.OK(xyParkPropertycontractDetailList);
	}
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "物业合同款项信息通过主表ID查询")
	@ApiOperation(value="物业合同款项信息主表ID查询", notes="物业合同款项信息-通主表ID查询")
	@GetMapping(value = "/queryXyParkPropertycontractPayinfoByMainId")
	public Result<?> queryXyParkPropertycontractPayinfoListByMainId(@RequestParam(name="id",required=true) String id) {
		List<XyParkPropertycontractPayinfo> xyParkPropertycontractPayinfoList = xyParkPropertycontractPayinfoService.selectByMainId(id);
		return Result.OK(xyParkPropertycontractPayinfoList);
	}
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "免租期通过主表ID查询")
	@ApiOperation(value="免租期主表ID查询", notes="免租期-通主表ID查询")
	@GetMapping(value = "/queryXyParkPropertycontractFreeperiodByMainId")
	public Result<?> queryXyParkPropertycontractFreeperiodListByMainId(@RequestParam(name="id",required=true) String id) {
		List<XyParkPropertycontractFreeperiod> xyParkPropertycontractFreeperiodList = xyParkPropertycontractFreeperiodService.selectByMainId(id);
		return Result.OK(xyParkPropertycontractFreeperiodList);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param xyParkPropertycontract
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, XyParkPropertycontract xyParkPropertycontract) {
      // Step.1 组装查询条件查询数据
      QueryWrapper<XyParkPropertycontract> queryWrapper = QueryGenerator.initQueryWrapper(xyParkPropertycontract, request.getParameterMap());
      LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

      //Step.2 获取导出数据
      List<XyParkPropertycontract> queryList = xyParkPropertycontractService.list(queryWrapper);
      // 过滤选中数据
      String selections = request.getParameter("selections");
      List<XyParkPropertycontract> xyParkPropertycontractList = new ArrayList<XyParkPropertycontract>();
      if(oConvertUtils.isEmpty(selections)) {
          xyParkPropertycontractList = queryList;
      }else {
          List<String> selectionList = Arrays.asList(selections.split(","));
          xyParkPropertycontractList = queryList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
      }

      // Step.3 组装pageList
      List<XyParkPropertycontractPage> pageList = new ArrayList<XyParkPropertycontractPage>();
      for (XyParkPropertycontract main : xyParkPropertycontractList) {
          XyParkPropertycontractPage vo = new XyParkPropertycontractPage();
          BeanUtils.copyProperties(main, vo);
          List<XyParkPropertycontractDetail> xyParkPropertycontractDetailList = xyParkPropertycontractDetailService.selectByMainId(main.getId());
          vo.setXyParkPropertycontractDetailList(xyParkPropertycontractDetailList);
          List<XyParkPropertycontractPayinfo> xyParkPropertycontractPayinfoList = xyParkPropertycontractPayinfoService.selectByMainId(main.getId());
          vo.setXyParkPropertycontractPayinfoList(xyParkPropertycontractPayinfoList);
          List<XyParkPropertycontractFreeperiod> xyParkPropertycontractFreeperiodList = xyParkPropertycontractFreeperiodService.selectByMainId(main.getId());
          vo.setXyParkPropertycontractFreeperiodList(xyParkPropertycontractFreeperiodList);
          pageList.add(vo);
      }

      // Step.4 AutoPoi 导出Excel
      ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
      mv.addObject(NormalExcelConstants.FILE_NAME, "新亚园区物业合同列表");
      mv.addObject(NormalExcelConstants.CLASS, XyParkPropertycontractPage.class);
      mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("新亚园区物业合同数据", "导出人:"+sysUser.getRealname(), "新亚园区物业合同"));
      mv.addObject(NormalExcelConstants.DATA_LIST, pageList);
      return mv;
    }

    /**
    * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
      Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
      for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
          MultipartFile file = entity.getValue();// 获取上传文件对象
          ImportParams params = new ImportParams();
          params.setTitleRows(2);
          params.setHeadRows(1);
          params.setNeedSave(true);
          try {
              List<XyParkPropertycontractPage> list = ExcelImportUtil.importExcel(file.getInputStream(), XyParkPropertycontractPage.class, params);
              for (XyParkPropertycontractPage page : list) {
                  XyParkPropertycontract po = new XyParkPropertycontract();
                  BeanUtils.copyProperties(page, po);
                  xyParkPropertycontractService.saveMain(po, page.getXyParkPropertycontractDetailList(),page.getXyParkPropertycontractPayinfoList(),page.getXyParkPropertycontractFreeperiodList());
              }
              return Result.OK("文件导入成功！数据行数:" + list.size());
          } catch (Exception e) {
              log.error(e.getMessage(),e);
              return Result.error("文件导入失败:"+e.getMessage());
          } finally {
              try {
                  file.getInputStream().close();
              } catch (IOException e) {
                  e.printStackTrace();
              }
          }
      }
      return Result.OK("文件导入失败！");
    }

}
