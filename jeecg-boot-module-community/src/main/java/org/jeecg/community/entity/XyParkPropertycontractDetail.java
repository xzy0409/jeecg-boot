package org.jeecg.community.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 物业合同房源信息
 * @Author: jeecg-boot
 * @Date:   2022-04-13
 * @Version: V1.0
 */
@ApiModel(value="xy_park_propertycontract对象", description="新亚园区物业合同")
@Data
@TableName("xy_park_propertycontract_detail")
public class XyParkPropertycontractDetail implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
	@ApiModelProperty(value = "主键")
	private String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "创建日期")
	private Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "更新日期")
	private Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private String sysOrgCode;
	/**合同编号*/
	@ApiModelProperty(value = "合同编号")
	private String contractCode;
	/**房源编号*/
	@ApiModelProperty(value = "房源编号")
	private String houseCode;
	/**楼宇编号*/
	@ApiModelProperty(value = "楼宇编号")
	private String buildingCode;
	/**楼宇名称*/
	@ApiModelProperty(value = "楼宇名称")
	private String buildingName;
	/**项目编号*/
	@ApiModelProperty(value = "项目编号")
	private String projectCode;
	/**项目名称*/
	@ApiModelProperty(value = "项目名称")
	private String projectName;
	/**楼层*/
	@ApiModelProperty(value = "楼层")
	private String floor;
	/**建筑面积（㎡）*/
	@ApiModelProperty(value = "建筑面积（㎡）")
	private java.math.BigDecimal buildingArea;
	/**产权面积（㎡）*/
	@ApiModelProperty(value = "产权面积（㎡）")
	private java.math.BigDecimal propertyArea;
	/**装修情况*/
	@ApiModelProperty(value = "装修情况")
	private String decoration;
	/**房源类型*/
	@ApiModelProperty(value = "房源类型")
	private String houseType;
}
