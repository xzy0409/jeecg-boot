package org.jeecg.community.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecg.autopoi.poi.excel.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 园区项目信息
 * @author: MxpIO
 * @version: V1.0
 */
@Data
@TableName("xy_park_project")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="xy_park_project对象", description="园区项目信息")
public class XyParkProject implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**项目编码*/
	@Excel(name = "项目编码", width = 15)
    @ApiModelProperty(value = "项目编码")
    private java.lang.String projectCode;
	/**项目名称*/
	@Excel(name = "项目名称", width = 15)
    @ApiModelProperty(value = "项目名称")
    private java.lang.String projectName;
	/**项目地址*/
	@Excel(name = "项目地址", width = 15)
    @ApiModelProperty(value = "项目地址")
    private java.lang.String projectAddress;
	/**土地使用证号*/
	@Excel(name = "土地使用证号", width = 15)
    @ApiModelProperty(value = "土地使用证号")
    private java.lang.String projectLanduseno;
	/**地块号*/
	@Excel(name = "地块号", width = 15)
    @ApiModelProperty(value = "地块号")
    private java.lang.String landNumber;
	/**土地面积*/
	@Excel(name = "土地面积", width = 15)
    @ApiModelProperty(value = "土地面积")
    private java.math.BigDecimal landArea;
	/**土地用途*/
	@Excel(name = "土地用途", width = 15)
    @ApiModelProperty(value = "土地用途")
    private java.lang.String landUsetype;
	/**土地使用年限*/
	@Excel(name = "土地使用年限", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "土地使用年限")
    private java.util.Date landUserdeadline;
	/**签订合同号*/
	@Excel(name = "签订合同号", width = 15)
    @ApiModelProperty(value = "签订合同号")
    private java.lang.String contractNo;
	/**行政单位*/
	@Excel(name = "行政单位", width = 15)
    @ApiModelProperty(value = "行政单位")
    private java.lang.String administrativeUnit;
	/**合同方式*/
	@Excel(name = "合同方式", width = 15)
    @ApiModelProperty(value = "合同方式")
    private java.lang.String contractType;
	/**联系人*/
	@Excel(name = "联系人", width = 15)
    @ApiModelProperty(value = "联系人")
    private java.lang.String contacts;
	/**联系方式*/
	@Excel(name = "联系方式", width = 15)
    @ApiModelProperty(value = "联系方式")
    private java.lang.String contactInfo;
	/**用地规划许可证*/
	@Excel(name = "用地规划许可证", width = 15)
    @ApiModelProperty(value = "用地规划许可证")
    private java.lang.String landusePermit;
	/**工程规划许可证*/
	@Excel(name = "工程规划许可证", width = 15)
    @ApiModelProperty(value = "工程规划许可证")
    private java.lang.String buildingPermit;
	/**工程施工许可证*/
	@Excel(name = "工程施工许可证", width = 15)
    @ApiModelProperty(value = "工程施工许可证")
    private java.lang.String buildingLicense;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String remarks;
}
