package org.jeecg.community.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 业主开票信息表
 * @author: jeecg-boot
 * @version: V1.0
 */
@ApiModel(value="xy_park_owner对象", description="业主信息表")
@Data
@TableName("xy_park_ownerbillinginfo")
public class XyParkOwnerbillinginfo implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
	@ApiModelProperty(value = "主键")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**业主编码*/
	@ApiModelProperty(value = "业主编码")
	private java.lang.String ownerCode;
	/**发票类型*/
	@ApiModelProperty(value = "发票类型")
	private java.lang.String invoiceType;
	/**发票抬头*/
	@ApiModelProperty(value = "发票抬头")
	private java.lang.String invoiceTitle;
	/**纳税人类别*/
	@ApiModelProperty(value = "纳税人类别")
	private java.lang.String taxpayerType;
	/**纳税人识别号*/
	@ApiModelProperty(value = "纳税人识别号")
	private java.lang.String taxpayerCode;
	/**开户银行*/
	@ApiModelProperty(value = "开户银行")
	private java.lang.String depositBank;
	/**银行账号*/
	@ApiModelProperty(value = "银行账号")
	private java.lang.String bankAccount;
	/**联系电话*/
	@ApiModelProperty(value = "联系电话")
	private java.lang.String contactNumber;
	/**开票地址*/
	@ApiModelProperty(value = "开票地址")
	private java.lang.String billingAddress;
}
