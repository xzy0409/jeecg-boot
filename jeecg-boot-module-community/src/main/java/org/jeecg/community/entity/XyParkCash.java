package org.jeecg.community.entity;

import java.math.BigDecimal;
import java.util.Date;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.system.base.entity.JeecgEntity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("xy_park_cash")
public class XyParkCash extends JeecgEntity {

	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "收支编号")
	@Excel(name="收支编号",width=15)
	private String cashCode;
	
	@ApiModelProperty(value = "收支账户")
	@Excel(name="收支账户",width=15)
	private String customerAccount;
	
	@ApiModelProperty(value = "收支属性")
	@Excel(name="收支属性",width=15,dicCode = "xy_park_cash_type")
	private String cashType;
	
	@ApiModelProperty(value = "发生对象")
	private String ownerCode;

	@TableField(exist = false)
	@Excel(name="业主信息",width=15)
	private String  ownerName;
	
	@ApiModelProperty(value = "发生金额")
	@Excel(name="发生金额",width=15,type=4)
	private BigDecimal amount;
	
	@ApiModelProperty(value = "手续费")
	@Excel(name="手续费",width=15,type=4)
	private BigDecimal fee;
	
	@ApiModelProperty(value = "到账金额")
	@Excel(name="到账金额",width=15,type=4)
	private BigDecimal actualAmount;
	
	@ApiModelProperty(value = "付款日期")
	//@Excel(name="付款日期",width=15)
	@Excel(name = "付款日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	private Date paymentDate;
	
	@ApiModelProperty(value = "到账日期")
	//@Excel(name="到账日期",width=15)
	@Excel(name = "到账日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	private Date arrivalDate;
	
	@ApiModelProperty(value = "账户")
	@Excel(name="账户",width=15)
	private String accountCode;
	
	@ApiModelProperty(value = "收支方式")
	@Excel(name="收支方式",width=15,dicCode = "xy_park_cash_method")
	private String cashMethod;
	
	@ApiModelProperty(value = "凭证号")
	@Excel(name="凭证号",width=15)
	private String voucherNo;
	
	@ApiModelProperty(value = "收据编号")
	@Excel(name="收据编号",width=15)
	private String receiptNo;
	
	@ApiModelProperty(value = "费项类型")
	private String costType;


	@TableField(exist = false)
	@Excel(name="费项名称",width=15)
	private String  costName;



	@Excel(name = "开票日期", width = 15)
    @ApiModelProperty(value = "开票日期")
	private String invoiceDate;
	
	@ApiModelProperty(value = "备注")
	@Excel(name="备注",width=15)
	private String remark;
	
	@ApiModelProperty(value = "已匹配金额")
	@TableField(exist = false)
	private BigDecimal checkAmount;
	
	@TableField(exist = false)
	private XyParkOwner xyParkOwner;
	
	@TableField(exist = false)
	private XyParkAccount xyParkAccount;
	
	@TableField(exist = false)
	private XyParkCostType xyParkCostType;

}
