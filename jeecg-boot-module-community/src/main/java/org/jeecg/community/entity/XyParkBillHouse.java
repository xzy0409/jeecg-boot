package org.jeecg.community.entity;

import java.math.BigDecimal;
import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.system.base.entity.JeecgEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("xy_park_bill_house")
public class XyParkBillHouse extends JeecgEntity {
	
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "账单编号")
	@Excel(name="账单编号",width=15)
	private String billCode;
	
	/**房源号*/
	@Excel(name = "房源号", width = 15)
    @ApiModelProperty(value = "房源号")
	private String houseCode;
	
	/**房源号*/
	@Excel(name = "项目名称", width = 15)
    @ApiModelProperty(value = "项目名称")
	private String projectName;
	
	/**房源号*/
	@Excel(name = "楼宇名称", width = 15)
    @ApiModelProperty(value = "楼宇名称")
	private String buildingName;
	
	/**楼层*/
	@Excel(name = "楼层", width = 15)
    @ApiModelProperty(value = "楼层")
    private String floor;
	
	/**建筑面积（㎡）*/
	@Excel(name = "建筑面积（㎡）", width = 15)
    @ApiModelProperty(value = "建筑面积（㎡）")
    private BigDecimal buildingArea;
	
	/**产权面积（㎡）*/
	@Excel(name = "产权面积（㎡）", width = 15)
    @ApiModelProperty(value = "产权面积（㎡）")
    private BigDecimal propertyArea;
}
