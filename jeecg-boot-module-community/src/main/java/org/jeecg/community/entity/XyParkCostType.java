package org.jeecg.community.entity;

import java.math.BigDecimal;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.system.base.entity.JeecgEntity;

import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("xy_park_cost_type")
public class XyParkCostType extends JeecgEntity {

	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "费项类型")
	@Excel(name="费项类型",width=15)
	private String costType;
	
	@ApiModelProperty(value = "费项名称")
	@Excel(name="费项名称",width=15)
	private String costName;
	
	@ApiModelProperty(value = "税率")
	@Excel(name="税率",width=15)
	private BigDecimal taxRate;

}
