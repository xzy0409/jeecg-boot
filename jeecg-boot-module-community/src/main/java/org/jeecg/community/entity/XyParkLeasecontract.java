package org.jeecg.community.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import org.jeecg.autopoi.poi.excel.annotation.Excel;

import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 新亚园区租赁合同
 * @Author: jeecg-boot
 * @Date:   2022-04-13
 * @Version: V1.0
 */
@ApiModel(value="xy_park_leasecontract对象", description="新亚园区租赁合同")
@Data
@TableName("xy_park_leasecontract")
public class XyParkLeasecontract implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
	/**签订日期*/
	@Excel(name = "签订日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "签订日期")
    private Date signingDate;
	/**合同编号*/
	@Excel(name = "合同编号", width = 15)
    @ApiModelProperty(value = "合同编号")
    private String contractCode;
	/**出租方*/
	@Excel(name = "出租方", width = 15)
    @ApiModelProperty(value = "出租方")
    private String item;
	/**出租方名称*/
	@Excel(name = "出租方名称", width = 15)
    @ApiModelProperty(value = "出租方名称")
    private String company;
	/**开户银行*/
	@Excel(name = "开户银行", width = 15)
    @ApiModelProperty(value = "开户银行")
    private String depositPark;
	/**开户账户*/
	@Excel(name = "开户账户", width = 15)
    @ApiModelProperty(value = "开户账户")
    private String parkNumber;
	/**承租方编号*/
	@Excel(name = "承租方编号", width = 15)
    @ApiModelProperty(value = "承租方编号")
    private String ownerCode;
	/**承租方*/
	@Excel(name = "承租方", width = 15)
    @ApiModelProperty(value = "承租方")
    private String ownerName;
	/**社会信用代码*/
	@Excel(name = "社会信用代码", width = 15)
    @ApiModelProperty(value = "社会信用代码")
    private String socialCreditcode;
	/**身份证号*/
	@Excel(name = "身份证号", width = 15)
    @ApiModelProperty(value = "身份证号")
    private String idNumber;
	/**联系人*/
	@Excel(name = "联系人", width = 15)
    @ApiModelProperty(value = "联系人")
    private String contractor;
	/**联系地址*/
	@Excel(name = "联系地址", width = 15)
    @ApiModelProperty(value = "联系地址")
    private String contactAddress;
	/**联系方式*/
	@Excel(name = "联系方式", width = 15)
    @ApiModelProperty(value = "联系方式")
    private String contactMethod;
	/**项目编号*/
	@Excel(name = "项目编号", width = 15)
    @ApiModelProperty(value = "项目编号")
    private String projectCode;
	/**项目名称*/
	@Excel(name = "项目名称", width = 15)
    @ApiModelProperty(value = "项目名称")
    private String projectName;
	/**项目位置*/
	@Excel(name = "项目位置", width = 15)
    @ApiModelProperty(value = "项目位置")
    private String projectAddress;
	/**土地编号*/
	@Excel(name = "土地编号", width = 15)
    @ApiModelProperty(value = "土地编号")
    private String landNumber;
	/**土地使用证号*/
	@Excel(name = "土地使用证号", width = 15)
    @ApiModelProperty(value = "土地使用证号")
    private String landUserpermit;
	/**租赁用途*/
	@Excel(name = "租赁用途", width = 15)
    @ApiModelProperty(value = "租赁用途")
    private String leasePurpose;
	/**租赁面积（㎡）*/
	@Excel(name = "租赁面积（㎡）", width = 15)
    @ApiModelProperty(value = "租赁面积（㎡）")
    private String leaseArea;
	/**租金标准*/
	@Excel(name = "租金标准", width = 15)
    @ApiModelProperty(value = "租金标准")
    private java.math.BigDecimal rentStandard;
	/**租金总额（小写）*/
	@Excel(name = "租金总额（小写）", width = 15)
    @ApiModelProperty(value = "租金总额（小写）")
    private java.math.BigDecimal totalRent;
	/**租金总额（大写）*/
	@Excel(name = "租金总额（大写）", width = 15)
    @ApiModelProperty(value = "租金总额（大写）")
    private String totalRentdetail;
	/**租金递增幅度*/
	@Excel(name = "租金递增幅度", width = 15)
    @ApiModelProperty(value = "租金递增幅度")
    private String increaseRent;
	/**缴纳方式*/
	@Excel(name = "缴纳方式", width = 15, dicCode = "xy_park_contract_cycle")
    @Dict(dicCode = "xy_park_contract_cycle")
    @ApiModelProperty(value = "缴纳方式")
    private String rentMethod;
	/**首次缴纳日期*/
	@Excel(name = "首次缴纳日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "首次缴纳日期")
    private Date firstPayDate;
	/**保证金*/
	@Excel(name = "保证金", width = 15)
    @ApiModelProperty(value = "保证金")
    private java.math.BigDecimal marginAmount;
	/**交房时间*/
	@Excel(name = "交房时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "交房时间")
    private Date deliverTime;
	/**租赁开始时间*/
	@Excel(name = "租赁开始时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "租赁开始时间")
    private Date leaseStarttime;
	/**租赁结束时间*/
	@Excel(name = "租赁结束时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "租赁结束时间")
    private Date leaseEndtime;
	/**其他约定事项*/
	@Excel(name = "其他约定事项", width = 15)
    @ApiModelProperty(value = "其他约定事项")
    private String contractArrange;
	/**合同附件*/
	@Excel(name = "合同附件", width = 15)
    @ApiModelProperty(value = "合同附件")
    private String contractFile;
	/**单据状态*/
	@Excel(name = "单据状态", width = 15)
    @ApiModelProperty(value = "单据状态")
    private String bpmStatus;
	/**合同类型*/
	@Excel(name = "合同类型", width = 15, dicCode = "xy_park_contract_type")
    @Dict(dicCode = "xy_park_contract_type")
    @ApiModelProperty(value = "合同类型")
    private String contractType;
	/**是否三七*/
	@Excel(name = "是否三七", width = 15)
    @ApiModelProperty(value = "是否三七")
    private String isThreeSeven;
	
	/**合同状态*/
	@Excel(name = "合同状态", width = 15)
    @ApiModelProperty(value = "合同状态")
	private String contractStatus;
}
