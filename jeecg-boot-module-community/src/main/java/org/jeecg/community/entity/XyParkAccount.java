package org.jeecg.community.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecg.autopoi.poi.excel.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 账户管理
 * @author: MxpIO
 * @version: V1.0
 */
@Data
@TableName("xy_park_account")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="xy_park_account对象", description="账户管理")
public class XyParkAccount implements Serializable {

	private static final long serialVersionUID = 3027824030771876158L;
	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**条目名称*/
	@Excel(name = "条目名称", width = 15)
    @ApiModelProperty(value = "条目名称")
    private java.lang.String item;
	/**收款公司*/
	@Excel(name = "收款公司", width = 15)
    @ApiModelProperty(value = "收款公司")
    private java.lang.String company;
	/**开户银行*/
	@Excel(name = "开户银行", width = 15)
    @ApiModelProperty(value = "开户银行")
    private java.lang.String depositPark;
	/**银行账户*/
	@Excel(name = "银行账户", width = 15)
    @ApiModelProperty(value = "银行账户")
    private java.lang.String parkNumber;
	/**纳税人类别*/
	@Excel(name = "纳税人类别", width = 15)
    @ApiModelProperty(value = "纳税人类别")
    private java.lang.String categoryOfTaxpayer;
	/**纳税人识别号*/
	@Excel(name = "纳税人识别号", width = 15)
    @ApiModelProperty(value = "纳税人识别号")
    private java.lang.String taxpayerId;
	
	@Excel(name = "注册地址", width = 15)
    @ApiModelProperty(value = "注册地址")
	private String companyAddr;
	
	@Excel(name = "法人", width = 15)
    @ApiModelProperty(value = "法人")
	private String corporation;
	
	@Excel(name = "法人联系方式", width = 15)
    @ApiModelProperty(value = "法人联系方式")
	private String corporateTel;
	
	@Excel(name = "联系地址", width = 15)
    @ApiModelProperty(value = "联系地址")
	private String corporateAddr;
}
