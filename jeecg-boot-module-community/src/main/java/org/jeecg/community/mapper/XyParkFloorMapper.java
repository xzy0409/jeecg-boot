package org.jeecg.community.mapper;

import java.util.List;
import org.jeecg.community.entity.XyParkFloor;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 楼层信息
 * @author: jeecg-boot
 * @version: V1.0
 */
@Mapper
public interface XyParkFloorMapper extends BaseMapper<XyParkFloor> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<XyParkFloor> selectByMainId(@Param("mainId") String mainId);
}
