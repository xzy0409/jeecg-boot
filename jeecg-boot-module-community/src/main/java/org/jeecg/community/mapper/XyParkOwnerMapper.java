package org.jeecg.community.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.community.entity.XyParkOwner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 业主信息表
 * @author: jeecg-boot
 * @version: V1.0
 */
@Mapper
public interface XyParkOwnerMapper extends BaseMapper<XyParkOwner> {

}
