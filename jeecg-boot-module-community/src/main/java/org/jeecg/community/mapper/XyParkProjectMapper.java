package org.jeecg.community.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.community.entity.XyParkProject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 园区项目信息
 * @author: MxpIO
 * @version: V1.0
 */
@Mapper
public interface XyParkProjectMapper extends BaseMapper<XyParkProject> {

}
