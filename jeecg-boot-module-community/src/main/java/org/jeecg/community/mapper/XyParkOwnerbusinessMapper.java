package org.jeecg.community.mapper;

import java.util.List;
import org.jeecg.community.entity.XyParkOwnerbusiness;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 业主工商信息
 * @author: jeecg-boot
 * @version: V1.0
 */
@Mapper
public interface XyParkOwnerbusinessMapper extends BaseMapper<XyParkOwnerbusiness> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<XyParkOwnerbusiness> selectByMainId(@Param("mainId") String mainId);
}
