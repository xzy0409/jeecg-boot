package org.jeecg.community.mapper;

import java.util.List;
import org.jeecg.community.entity.XyParkHousecapital;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 房源配属资产
 * @author: jeecg-boot
 * @version: V1.0
 */
@Mapper
public interface XyParkHousecapitalMapper extends BaseMapper<XyParkHousecapital> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<XyParkHousecapital> selectByMainId(@Param("mainId") String mainId);
}
