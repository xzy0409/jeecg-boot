package org.jeecg.community.mapper;

import java.util.List;
import org.jeecg.community.entity.XyParkIntentionhouse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 客户意向房源
 * @author: jeecg-boot
 * @version: V1.0
 */
@Mapper
public interface XyParkIntentionhouseMapper extends BaseMapper<XyParkIntentionhouse> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<XyParkIntentionhouse> selectByMainId(@Param("mainId") String mainId);
}
