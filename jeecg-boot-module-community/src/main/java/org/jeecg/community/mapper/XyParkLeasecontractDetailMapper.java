package org.jeecg.community.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.community.entity.XyParkLeasecontractDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 租赁合同房源信息
 * @Author: jeecg-boot
 * @Date:   2022-04-13
 * @Version: V1.0
 */
@Mapper
public interface XyParkLeasecontractDetailMapper extends BaseMapper<XyParkLeasecontractDetail> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<XyParkLeasecontractDetail> selectByMainId(@Param("mainId") String mainId);
}
