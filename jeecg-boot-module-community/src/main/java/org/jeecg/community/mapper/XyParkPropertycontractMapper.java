package org.jeecg.community.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.community.entity.XyParkPropertycontract;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 新亚园区物业合同
 * @Author: jeecg-boot
 * @Date:   2022-04-13
 * @Version: V1.0
 */
@Mapper
public interface XyParkPropertycontractMapper extends BaseMapper<XyParkPropertycontract> {

	void invalid(String id);

}
