package org.jeecg.community.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.community.entity.XyParkLeasecontract;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 新亚园区租赁合同
 * @Author: jeecg-boot
 * @Date:   2022-04-13
 * @Version: V1.0
 */
@Mapper
public interface XyParkLeasecontractMapper extends BaseMapper<XyParkLeasecontract> {

	void invalid(String id);
}
