package org.jeecg.community.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.community.entity.XyParkCash;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface XyParkCashMapper extends BaseMapper<XyParkCash> {

}
