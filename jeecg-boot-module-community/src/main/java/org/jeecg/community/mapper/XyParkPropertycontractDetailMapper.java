package org.jeecg.community.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.community.entity.XyParkPropertycontractDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 物业合同房源信息
 * @Author: jeecg-boot
 * @Date:   2022-04-13
 * @Version: V1.0
 */
@Mapper
public interface XyParkPropertycontractDetailMapper extends BaseMapper<XyParkPropertycontractDetail> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<XyParkPropertycontractDetail> selectByMainId(@Param("mainId") String mainId);
}
