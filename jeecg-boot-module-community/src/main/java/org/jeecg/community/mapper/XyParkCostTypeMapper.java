package org.jeecg.community.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.community.entity.XyParkCostType;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface XyParkCostTypeMapper extends BaseMapper<XyParkCostType> {

}
