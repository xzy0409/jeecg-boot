package org.jeecg.community.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.community.entity.XyParkLeasecontractPayinfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 租赁合同款项信息
 * @Author: jeecg-boot
 * @Date:   2022-04-13
 * @Version: V1.0
 */
@Mapper
public interface XyParkLeasecontractPayinfoMapper extends BaseMapper<XyParkLeasecontractPayinfo> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<XyParkLeasecontractPayinfo> selectByMainId(@Param("mainId") String mainId);
}
