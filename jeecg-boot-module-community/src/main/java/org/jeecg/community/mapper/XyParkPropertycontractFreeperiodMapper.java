package org.jeecg.community.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.community.entity.XyParkPropertycontractFreeperiod;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 免租期
 * @Author: jeecg-boot
 * @Date:   2022-04-13
 * @Version: V1.0
 */
@Mapper
public interface XyParkPropertycontractFreeperiodMapper extends BaseMapper<XyParkPropertycontractFreeperiod> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<XyParkPropertycontractFreeperiod> selectByMainId(@Param("mainId") String mainId);
}
