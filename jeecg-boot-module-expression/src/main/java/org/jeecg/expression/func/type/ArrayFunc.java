package org.jeecg.expression.func.type;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.googlecode.aviator.runtime.type.AviatorObject;
import com.googlecode.aviator.runtime.type.AviatorString;

@Component
public class ArrayFunc extends AbstractSpringAviatorVariadicFunction {

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "arr";
	}

	@Override
	public boolean disabled() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getText() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AviatorObject variadicCall(Map<String, Object> env, AviatorObject... args) {
		StringBuffer sb = new StringBuffer();
		for(AviatorObject arg : args){
			Object value = arg.getValue(env);
			if(value == null){
				value = "";
			}
			sb.append(value.toString());
			sb.append(",");
		}
		if(sb.length()>0){
			sb.setLength(sb.length() - 1);
		}
		return new AviatorString(sb.toString());
	}

}
