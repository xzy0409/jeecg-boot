package org.jeecg.expression.func.type;

import com.googlecode.aviator.runtime.function.AbstractVariadicFunction;

public abstract class AbstractSpringAviatorVariadicFunction extends AbstractVariadicFunction {

	@Override
	public abstract String getName();
	
	public abstract boolean disabled();
	
	public abstract String getText();

}
