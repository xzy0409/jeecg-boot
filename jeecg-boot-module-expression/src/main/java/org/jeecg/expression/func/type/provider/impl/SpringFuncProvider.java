package org.jeecg.expression.func.type.provider.impl;

import java.util.Collection;

import org.jeecg.expression.func.type.AbstractSpringAviatorFunction;
import org.jeecg.expression.func.type.AbstractSpringAviatorVariadicFunction;
import org.jeecg.expression.func.type.provider.FuncProvider;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.googlecode.aviator.AviatorEvaluator;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class SpringFuncProvider implements ApplicationContextAware, FuncProvider {

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		log.info("===========SpringAviatorFunction loading start ==========");
		Collection<AbstractSpringAviatorFunction> funcs=applicationContext.getBeansOfType(AbstractSpringAviatorFunction.class).values();
		Collection<AbstractSpringAviatorVariadicFunction> variadicFuncs=applicationContext.getBeansOfType(AbstractSpringAviatorVariadicFunction.class).values();
		for(AbstractSpringAviatorFunction func:funcs){
			if(func.disabled() || func.getName()==null){
				log.info("[panda-expression]Function==>"+func.getName()+"(disabled)");
				continue;
			}
			log.info("[panda-expression]Function==>"+func.getName()+"(enabled)");
			AviatorEvaluator.addFunction(func);
		}
		
		for(AbstractSpringAviatorVariadicFunction variadicFunc:variadicFuncs){
			if(variadicFunc.disabled() || variadicFunc.getName()==null){
				log.info("[panda-expression]Function==>"+variadicFunc.getName()+"(disabled)");
				continue;
			}
			log.info("[panda-expression]Function==>"+variadicFunc.getName()+"(enabled)");
			AviatorEvaluator.addFunction(variadicFunc);
		}
		log.info("===========SpringAviatorFunction  loading end  ==========");
	}

}
