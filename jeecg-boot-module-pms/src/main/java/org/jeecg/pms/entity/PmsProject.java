package org.jeecg.pms.entity;

import java.math.BigDecimal;
import java.util.Date;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecg.common.system.base.entity.JeecgEntity;
import org.springframework.format.annotation.DateTimeFormat;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("pms_project")
public class PmsProject extends JeecgEntity {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "项目编号")
	@Excel(name="项目编号",width=15)
	private String projectCode;

	@ApiModelProperty(value = "项目名称")
	@Excel(name="项目名称",width=15)
	private String projectName;

	@ApiModelProperty(value = "项目经理")
	@Excel(name="项目经理",width=15)
	@Dict(dictTable = "sys_user", dicCode = "username", dicText = "realname")
	private String projectManager;

	@ApiModelProperty(value = "开始时间")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@Excel(name="开始时间",width=15,format = "yyyy-MM-dd")
	private Date startTime;

	@ApiModelProperty(value = "结束时间")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@Excel(name="结束时间",width=15,format = "yyyy-MM-dd")
	private Date endTime;

	@ApiModelProperty(value = "项目进度")
	@Excel(name="项目进度（%）",width=10)
	private BigDecimal projectProgress;

	@ApiModelProperty(value = "项目助理")
	@Excel(name="项目助理",width=15)
	@Dict(dicCode = "username",dictTable="sys_user",dicText="realname")
	private String projectAssistant;

	@ApiModelProperty(value = "项目成员")
	@Excel(name="项目成员",width=30)
	@Dict(dicCode = "username",dictTable="sys_user",dicText="realname")
	private String projectMember;

	@ApiModelProperty(value = "抄送")
	@Excel(name="抄送",width=30)
	private String ccMember;

	@ApiModelProperty(value = "类型")
	@Excel(name="类型",width=15)
	@Dict(dicCode = "pms_project_type")
	private String projectType;

	@ApiModelProperty(value = "部门")
	@Excel(name="部门",width=15)
	@Dict(dictTable = "sys_depart", dicCode = "id", dicText = "depart_name")
	private String departCode;

	@ApiModelProperty(value = "状态")
	@Excel(name="状态",width=15)
	@Dict(dicCode = "pms_project_status")
	private String projectStatus;

	@ApiModelProperty(value = "描述")
	@Excel(name="描述",width=30)
	private String projectDesc;

	private String filePath;

    @ApiModelProperty(value = "计划工时")
    private BigDecimal planManHours;
    @ApiModelProperty(value = "父专案")
    private String parentProject;
    @ApiModelProperty(value = "产品")
    private String productCode;
    @ApiModelProperty(value = "专案效益")
    private BigDecimal projectBenefits;
    @ApiModelProperty(value = "目标人力成本(人天)")
    private BigDecimal laborCosts;


}
