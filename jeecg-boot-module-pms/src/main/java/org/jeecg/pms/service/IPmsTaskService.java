package org.jeecg.pms.service;

import java.util.List;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.common.system.base.service.JeecgService;
import org.jeecg.modules.online.config.exception.BusinessException;
import org.jeecg.pms.entity.PmsTask;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface IPmsTaskService extends JeecgService<PmsTask> {

	/**根节点父ID的值*/
	public static final String ROOT_PID_VALUE = "0";

	/**树节点有子节点状态值*/
	public static final String HASCHILD = "1";

	/**树节点无子节点状态值*/
	public static final String NOCHILD = "0";

	/**新增节点*/
	void addPmsTask(PmsTask pmsTask);

	/**修改节点*/
	void updatePmsTask(PmsTask pmsTask) throws JeecgBootException;

	/**删除节点*/
	void deletePmsTask(String id) throws JeecgBootException;

	/**查询所有数据，无分页*/
    List<PmsTask> queryTreeListNoPage(QueryWrapper<PmsTask> queryWrapper);

	List<PmsTask> queryTreeList(List<PmsTask> list);

	void handleTransientVariable(PmsTask task);

	/**重算任务时间*/
	void computeTaskTime(String projeckCode);

    /**
     * 导入任务
     * @param request
     * @param response
     */
    Result<?> importTasksByExcel(HttpServletRequest request, HttpServletResponse response)throws BusinessException;

}
