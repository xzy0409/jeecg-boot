package org.jeecg.pms.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.pms.entity.PmsTaskTime;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface PmsTaskTimeMapper extends BaseMapper<PmsTaskTime> {

}
