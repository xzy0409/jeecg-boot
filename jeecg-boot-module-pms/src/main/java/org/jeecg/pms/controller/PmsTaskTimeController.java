package org.jeecg.pms.controller;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.online.config.exception.BusinessException;
import org.jeecg.pms.entity.PmsTaskTime;
import org.jeecg.pms.service.IPmsTaskTimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * 项目管理模块-工时管理
 * @author: MxpIO
 * @version: V1.0
 */
@Api(tags = "项目管理模块-工时管理")
@RestController
@RequestMapping("/pms/taskTime")
@Slf4j
public class PmsTaskTimeController extends JeecgController<PmsTaskTime, IPmsTaskTimeService> {
    @Autowired
    private IPmsTaskTimeService pmsTaskTimeService;

    /**
     * 分页列表查询
     *
     * @param pmsTaskTime
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "项目管理-工时管理分页查询")
    @ApiOperation(value = "项目管理-工时管理分页查询", notes = "项目管理-工时管理分页查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(PmsTaskTime pmsTaskTime,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        QueryWrapper<PmsTaskTime> queryWrapper = QueryGenerator.initQueryWrapper(pmsTaskTime, req.getParameterMap());
        Page<PmsTaskTime> page = new Page<PmsTaskTime>(pageNo, pageSize);
        IPage<PmsTaskTime> pageList = pmsTaskTimeService.page(page, queryWrapper);
        log.info("查询当前页：" + pageList.getCurrent());
        log.info("查询当前页数量：" + pageList.getSize());
        log.info("查询结果数量：" + pageList.getRecords().size());
        log.info("数据总数：" + pageList.getTotal());
        return Result.OK(pageList);
    }

    /**
     * 添加
     *
     * @param pmsTaskTime
     * @return
     */
    @AutoLog(value = "项目管理-工时管理添加")
    @ApiOperation(value = "项目管理-工时管理添加", notes = "项目管理-工时管理添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody PmsTaskTime pmsTaskTime) {
        pmsTaskTimeService.save(pmsTaskTime);
        return Result.OK("添加成功！", pmsTaskTime);
    }

    /**
     * 编辑
     *
     * @param pmsTaskTime
     * @return
     */
    @AutoLog(value = "项目管理-工时管理编辑")
    @ApiOperation(value = "项目管理-工时管理编辑", notes = "项目管理-工时管理编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody PmsTaskTime pmsTaskTime) {
        pmsTaskTimeService.updateById(pmsTaskTime);
        return Result.OK("更新成功！", pmsTaskTime);
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "项目管理-通过id删除工时")
    @ApiOperation(value = "项目管理-通过id删除工时", notes = "项目管理-通过id删除工时")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        pmsTaskTimeService.removeById(id);
        return Result.OK("删除成功!", null);
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "项目管理-批量删除工时")
    @ApiOperation(value = "项目管理-批量删除工时", notes = "项目管理-批量删除工时")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.pmsTaskTimeService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!", null);
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "项目管理-通过id查询工时")
    @ApiOperation(value = "项目管理-通过id查询工时", notes = "项目管理-通过id查询工时")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        PmsTaskTime pmsTaskTime = pmsTaskTimeService.getById(id);
        if (pmsTaskTime == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(pmsTaskTime);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param pmsTaskTime
     */
    @RequestMapping(value = "/exportXls")
    @ApiOperation(value = "项目管理-工时导出excel", notes = "项目管理-工时导出excel")
    public ModelAndView exportXls(HttpServletRequest request, PmsTaskTime pmsTaskTime) {
        return super.exportXls(request, pmsTaskTime, PmsTaskTime.class, "项目管理模块-工时管理");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    @ApiOperation(value = "项目管理-工时导入excel", notes = "项目管理-工时导入excel")
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, PmsTaskTime.class);
    }

    /**
     * 项目管理增加工时计算
     * @param pmsTaskTime
     * @return
     * @throws BusinessException
     */
    @AutoLog(value = "项目管理-增加工时并计算")
    @ApiOperation(value = "项目管理-增加工时并计算", notes = "项目管理-增加工时并计算")
    @PostMapping(value = "/addWorkHours")
    public Result<?> addWorkHours(@RequestBody PmsTaskTime pmsTaskTime) throws BusinessException {
        pmsTaskTimeService.addWorkHours(pmsTaskTime);
        return Result.OK();
    }

    @AutoLog(value = "项目管理-批量增加工时并计算")
    @ApiOperation(value = "项目管理-批量增加工时并计算", notes = "项目管理-批量增加工时并计算")
    @PostMapping(value = "/addWorkHoursBatch")
    public Result<?> addWorkHoursBatch(@RequestBody List<PmsTaskTime> pmsTaskTimeList)throws BusinessException {
        if (CollectionUtils.isEmpty(pmsTaskTimeList)) {
            return Result.error("无请求数据");
        }
        pmsTaskTimeService.addWorkHoursBatch(pmsTaskTimeList);

        return Result.OK();
    }
}
