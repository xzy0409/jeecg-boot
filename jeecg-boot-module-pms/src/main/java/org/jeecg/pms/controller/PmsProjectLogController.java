package org.jeecg.pms.controller;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.pms.entity.PmsProjectLog;
import org.jeecg.pms.service.impl.PmsProjectLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;


/**
 * 项目管理模块-项目管理
 * 
 * @author MxpIO
 *
 */
@Slf4j
@Api(tags = "项目管理模块-项目日志管理")
@RestController
@RequestMapping("/pms/projectlog")
public class PmsProjectLogController extends JeecgController<PmsProjectLog, PmsProjectLogService> {
	
	@Autowired
	private PmsProjectLogService pmsProjectLogService;
	
	
	/**
     * 分页列表查询
     *
     * @param pmsProjectLog
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @ApiOperation(value = "获取项目日志列表", notes = "获取项目日志列表")
    @GetMapping(value = "/list")
    public Result<?> list(PmsProjectLog pmsProjectLog, @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo, @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                          HttpServletRequest req) {
        QueryWrapper<PmsProjectLog> queryWrapper = QueryGenerator.initQueryWrapper(pmsProjectLog, req.getParameterMap());
        Page<PmsProjectLog> page = new Page<PmsProjectLog>(pageNo, pageSize);

        IPage<PmsProjectLog> pageList = pmsProjectLogService.page(page, queryWrapper);
        log.info("查询当前页：" + pageList.getCurrent());
        log.info("查询当前页数量：" + pageList.getSize());
        log.info("查询结果数量：" + pageList.getRecords().size());
        log.info("数据总数：" + pageList.getTotal());
        return Result.OK(pageList);
    }
    
    /**
     * 添加
     *
     * @param pmsProjectLog
     * @return
     */
    @PostMapping(value = "/add")
    @AutoLog(value = "添加项目日志")
    @ApiOperation(value = "添加项目日志", notes = "添加项目日志")
    public Result<?> add(@RequestBody PmsProjectLog pmsProjectLog) {
    	pmsProjectLogService.save(pmsProjectLog);
        return Result.OK("添加成功！",pmsProjectLog);
    }

    /**
     * 编辑
     *
     * @param pmsProjectLog
     * @return
     */
    @PutMapping(value = "/edit")
    @ApiOperation(value = "编辑项目日志", notes = "编辑项目日志")
    @AutoLog(value = "编辑项目日志", operateType = CommonConstant.OPERATE_TYPE_EDIT)
    public Result<?> edit(@RequestBody PmsProjectLog pmsProjectLog) {
    	pmsProjectLogService.updateById(pmsProjectLog);
        return Result.OK("更新成功！",pmsProjectLog);
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "删除项目日志")
    @DeleteMapping(value = "/delete")
    @ApiOperation(value = "通过ID删除项目日志", notes = "通过ID删除项目日志")
    public Result<?> delete(@ApiParam(name = "id", value = "项目id", required = true) @RequestParam(name = "id", required = true) String id) {
    	pmsProjectLogService.removeById(id);
        return Result.OK("删除成功!",null);
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @DeleteMapping(value = "/deleteBatch")
    @ApiOperation(value = "批量删除项目日志", notes = "批量删除项目日志")
    public Result<?> deleteBatch(@ApiParam(name = "ids", value = "项目id数组", required = true) @RequestParam(name = "ids", required = true) String ids) {
    	pmsProjectLogService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功！",null);
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/queryById")
    @ApiOperation(value = "通过ID查询项目日志", notes = "通过ID查询项目日志")
    public Result<?> queryById(@ApiParam(name = "id", value = "项目id", required = true) @RequestParam(name = "id", required = true) String id) {
    	PmsProjectLog pmsProjectLog = pmsProjectLogService.getById(id);
        return Result.OK(pmsProjectLog);
    }

    /**
     * 导出excel
     *
     * @param request
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, PmsProjectLog pmsProjectLog) {
        return super.exportXls(request, pmsProjectLog, PmsProjectLog.class, "项目日志");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, PmsProjectLog.class);
    }

}
