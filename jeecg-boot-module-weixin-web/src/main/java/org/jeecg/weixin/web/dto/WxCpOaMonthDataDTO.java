package org.jeecg.weixin.web.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jeecg.weixin.cp.bean.oa.WxCpVacationConf;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class WxCpOaMonthDataDTO implements Serializable {
	
	private static final long serialVersionUID = 3165204905933234610L;

	private List<Map<String,Object>> datas;
	
	private List<Column> columns;
	
	@Data
	@AllArgsConstructor
	public static class Column implements Serializable {
		
		private static final long serialVersionUID = 2441059963262688639L;
		
		public Column(){
		}
		
		public Column(String field, String title, Integer width, boolean showOverflow){
			this.field = field;
			this.title = title;
			this.width = width;
			this.showOverflow = showOverflow;
		}

		private String field;
		private String title;
		private Integer width;
		private boolean showOverflow;
		
		private List<Column> children;
	}
	
	public void initColumn(List<WxCpVacationConf> vacationConfs){
		this.getColumns().add(new WxCpOaMonthDataDTO.Column("acctid","账户", 80, false));
		this.getColumns().add(new WxCpOaMonthDataDTO.Column("name","姓名", 80, false));
		this.getColumns().add(new WxCpOaMonthDataDTO.Column("departsName","部门名称", 100, true));
		
		//概况
		Column summaryInfo = new WxCpOaMonthDataDTO.Column();
		summaryInfo.setTitle("概况");
		summaryInfo.setChildren(new ArrayList<Column>());
		summaryInfo.getChildren().add(new WxCpOaMonthDataDTO.Column("workDays","应出勤天数", 80, false));
		summaryInfo.getChildren().add(new WxCpOaMonthDataDTO.Column("regularDays","正常天数", 80, false));
		summaryInfo.getChildren().add(new WxCpOaMonthDataDTO.Column("exceptDays","异常天数", 80, false));
		summaryInfo.getChildren().add(new WxCpOaMonthDataDTO.Column("standardWorkSec","标准工作时长（小时）", 80, false));
		summaryInfo.getChildren().add(new WxCpOaMonthDataDTO.Column("regularWorkSec","实际工作时长（小时）", 80, false));
		this.getColumns().add(summaryInfo);
		
		
		//异常
		Column exptionInfo = new WxCpOaMonthDataDTO.Column();
		exptionInfo.setTitle("异常");
		exptionInfo.setChildren(new ArrayList<Column>());
		exptionInfo.getChildren().add(new WxCpOaMonthDataDTO.Column("exceptionCount","异常（次）", 80, false));
		exptionInfo.getChildren().add(new WxCpOaMonthDataDTO.Column("exception1Count","迟到（次）", 80, false));
		exptionInfo.getChildren().add(new WxCpOaMonthDataDTO.Column("exception1Time","迟到时长（分钟）", 80, false));
		exptionInfo.getChildren().add(new WxCpOaMonthDataDTO.Column("exception2Count","早退（次）", 80, false));
		exptionInfo.getChildren().add(new WxCpOaMonthDataDTO.Column("exception2Time","早退时长（分钟）", 80, false));
		exptionInfo.getChildren().add(new WxCpOaMonthDataDTO.Column("exception3Count","缺卡（次）", 80, false));
		exptionInfo.getChildren().add(new WxCpOaMonthDataDTO.Column("exception4Count","旷工（次）", 80, false));
		exptionInfo.getChildren().add(new WxCpOaMonthDataDTO.Column("exception4Time","旷工时长（分钟）", 80, false));
		exptionInfo.getChildren().add(new WxCpOaMonthDataDTO.Column("exception5Count","地点异常（次）", 80, false));
		exptionInfo.getChildren().add(new WxCpOaMonthDataDTO.Column("exception6Count","设备异常（次）", 80, false));
		
		this.getColumns().add(exptionInfo);
		
		//假勤
		Column spItemInfo = new WxCpOaMonthDataDTO.Column();
		spItemInfo.setTitle("假勤信息");
		spItemInfo.setChildren(new ArrayList<Column>());
		for(WxCpVacationConf conf : vacationConfs){
    		spItemInfo.getChildren().add(new WxCpOaMonthDataDTO.Column("column_"+conf.getId(),conf.getName()+(conf.getTimeAttr()==0L?"（天）":"（小时）"), 80, false));
    	}
		spItemInfo.getChildren().add(new WxCpOaMonthDataDTO.Column("spItem2Count","补卡（次）", 80, false));
		spItemInfo.getChildren().add(new WxCpOaMonthDataDTO.Column("spItem100Count","外勤（次）", 80, false));
		spItemInfo.getChildren().add(new WxCpOaMonthDataDTO.Column("spItem3Count","出差（天）", 80, false));
		spItemInfo.getChildren().add(new WxCpOaMonthDataDTO.Column("spItem4Count","外出（小时）", 80, false));
		
		this.getColumns().add(spItemInfo);
		
		//加班
		Column overworkInfo = new WxCpOaMonthDataDTO.Column();
		overworkInfo.setTitle("加班信息");
		overworkInfo.setChildren(new ArrayList<Column>());
		overworkInfo.getChildren().add(new WxCpOaMonthDataDTO.Column("workdayOverSec","工作日（小时）", 80, false));
		overworkInfo.getChildren().add(new WxCpOaMonthDataDTO.Column("holidaysOverSec","节假日（小时）", 80, false));
		overworkInfo.getChildren().add(new WxCpOaMonthDataDTO.Column("restdaysOverSec","休息日（小时）", 80, false));
		this.getColumns().add(overworkInfo);
		
	}

}
