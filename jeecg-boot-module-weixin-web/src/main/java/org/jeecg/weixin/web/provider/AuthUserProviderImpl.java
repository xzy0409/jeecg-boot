package org.jeecg.weixin.web.provider;

import org.jeecg.modules.system.provider.IAuthUserProvider;
import org.jeecg.modules.system.provider.ThirdType;
import org.jeecg.weixin.cp.api.WxCpService;
import org.jeecg.weixin.cp.bean.WxCpOauth2UserInfo;
import org.jeecg.weixin.web.config.WxCpConfiguration;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class AuthUserProviderImpl implements IAuthUserProvider {

	@Override
	public String getUserIdByCode(String code) throws Exception {
		WxCpService wxCpService =  WxCpConfiguration.getCpService(1000011);
		WxCpOauth2UserInfo res =  wxCpService.getOauth2Service().getUserInfo(code);
		log.info(res.getUserId());
		return res.getUserId();
	}

	@Override
	public ThirdType getType() {
		return ThirdType.WEIXIN;
	}
}
