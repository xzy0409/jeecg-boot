package org.jeecg.weixin.web.provider;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.jeecg.common.api.dto.message.BusMessageDTO;
import org.jeecg.common.api.dto.message.MessageDTO;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.modules.system.provider.IThirdMessageProvider;
import org.jeecg.weixin.cp.api.WxCpMessageService;
import org.jeecg.weixin.cp.api.WxCpService;
import org.jeecg.weixin.cp.api.impl.WxCpMessageServiceImpl;
import org.jeecg.weixin.cp.bean.message.WxCpMessage;
import org.jeecg.weixin.web.config.WxCpConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class WxCpMessageProvider implements IThirdMessageProvider {

	@Value("${jeecg.wx.bpmn.agentId}")
	private Integer agentId;
	@Autowired
	private ISysBaseAPI iSysBaseAPI;

	@Override
	public void send(BusMessageDTO message) {
		try {
			WxCpService wxCpService = WxCpConfiguration.getCpService(agentId);
			WxCpMessageService wxCpMessageService = new WxCpMessageServiceImpl(wxCpService);
			String url = wxCpService.getOauth2Service().buildAuthorizationUrl(
					wxCpService.getWxCpConfigStorage().getDomain() + message.getThirdUrl(), null);
			String userStr = message.getToUser();
			StringBuffer toUser = new StringBuffer();
			if (userStr.contains(",")) {
				List<String> userList = Arrays.asList(userStr.split(","));
				for (String id : userList) {
					LoginUser _user = iSysBaseAPI.getUserByName(id);
					if (toUser.length() > 0) {
						toUser.append("|");
					}
					toUser = toUser.append(_user.getThirdId());
				}

			} else {
				LoginUser _user = iSysBaseAPI.getUserByName(userStr);
				toUser = toUser.append(_user.getThirdId());
			}

			WxCpMessage wxMsg = WxCpMessage.TEXTCARD().toUser(toUser.toString())
					.agentId(wxCpService.getWxCpConfigStorage().getAgentId()).title(message.getTitle())
					.description(message.getContent()).url(url).build();
			wxCpMessageService.send(wxMsg);
		} catch (Exception e) {
		}
	}

	@Override
	public void sendSimpleMsg(MessageDTO msg) {
		try {
			WxCpService wxCpService =  WxCpConfiguration.getCpService(agentId);
    		WxCpMessageService wxCpMessageService = new WxCpMessageServiceImpl(wxCpService);
    		
    		List<String> wxUsernames = new ArrayList<>();
    		List<String> users = Arrays.asList(msg.getToUser().split(","));
    		for(String username : users){
    			LoginUser _user = iSysBaseAPI.getUserByName(username);
    			if(_user != null){
    				wxUsernames.add(_user.getThirdId());
    			}
    		}
    		String wxUser = String.join("|", wxUsernames);
    		WxCpMessage message = WxCpMessage.TEXT().toUser(wxUser).agentId(wxCpService.getWxCpConfigStorage().getAgentId()).content(msg.getContent()).build();
    		wxCpMessageService.send(message);
		} catch (Exception e) {
		}
	}

}
