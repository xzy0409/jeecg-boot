package org.jeecg.weixin.common.util.http.jodd;

import jodd.http.HttpConnectionProvider;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import jodd.http.ProxyInfo;
import jodd.util.StringPool;
import org.jeecg.weixin.common.enums.WxType;
import org.jeecg.weixin.common.error.WxErrorException;
import org.jeecg.weixin.common.util.http.RequestHttp;
import org.jeecg.weixin.common.util.http.SimpleGetRequestExecutor;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * .
 *
 * @author ecoolper
 */
public class JoddHttpSimpleGetRequestExecutor extends SimpleGetRequestExecutor<HttpConnectionProvider, ProxyInfo> {
	public JoddHttpSimpleGetRequestExecutor(RequestHttp requestHttp) {
		super(requestHttp);
	}

	@Override
	public String execute(String uri, String queryParam, WxType wxType) throws WxErrorException, IOException {
		if (queryParam != null) {
			if (uri.indexOf('?') == -1) {
				uri += '?';
			}
			uri += uri.endsWith("?") ? queryParam : '&' + queryParam;
		}

		HttpRequest request = HttpRequest.get(uri);
		if (requestHttp.getRequestHttpProxy() != null) {
			requestHttp.getRequestHttpClient().useProxy(requestHttp.getRequestHttpProxy());
		}
		request.withConnectionProvider(requestHttp.getRequestHttpClient());
		HttpResponse response = request.send();
		response.charset(StandardCharsets.UTF_8.name());

		return handleResponse(wxType, response.bodyText());
	}

}
