package org.jeecg.common.system.base.service;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * Service基类
 * @author: dangzhenghui@163.com
 * @version: 1.0
 */
public interface JeecgService<T> extends IService<T> {
}
