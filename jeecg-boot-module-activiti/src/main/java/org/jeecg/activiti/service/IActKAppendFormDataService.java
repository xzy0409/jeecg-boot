package org.jeecg.activiti.service;

import org.jeecg.activiti.entity.ActKAppendFormData;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 流程节点附加表单数据service
 */
public interface IActKAppendFormDataService extends IService<ActKAppendFormData> {
}
