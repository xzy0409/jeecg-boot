package org.jeecg.activiti.service;

import org.jeecg.activiti.entity.ActKAppendForm;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 流程节点附加表单Service
 */
public interface IActKAppendFormService extends IService<ActKAppendForm> {
}
