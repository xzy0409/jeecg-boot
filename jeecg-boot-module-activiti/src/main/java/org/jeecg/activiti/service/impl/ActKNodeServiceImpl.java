package org.jeecg.activiti.service.impl;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.activiti.bpmn.converter.BpmnXMLConverter;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.bpmn.model.UserTask;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Model;
import org.apache.commons.collections.CollectionUtils;
import org.jeecg.activiti.entity.ActKNode;
import org.jeecg.activiti.entity.ActKNodeDesign;
import org.jeecg.activiti.mapper.ActKNodeMapper;
import org.jeecg.activiti.service.IActKNodeDesignService;
import org.jeecg.activiti.service.IActKNodeService;
import org.jeecg.common.api.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.api.client.util.Joiner;
import com.google.common.collect.Lists;

/**
 * 流程节点service实现类
 *
 * @author dousw
 * @version 1.0
 */

@Service
public class ActKNodeServiceImpl extends ServiceImpl<ActKNodeMapper, ActKNode> implements IActKNodeService {

	@Autowired
	private RepositoryService repositoryService;
	@Autowired
	private IActKNodeDesignService actKNodeDesignService;

	@Autowired
	private IActKNodeService iActKNodeService;

	@Override
	public Result<Object> getUserTaskNodeListByModelId(String modelId) {
		List<ActKNode> actKNodeList = Lists.newArrayList();
		// 获取模型
		Model modelData = this.repositoryService.getModel(modelId);
		byte[] bytes = this.repositoryService.getModelEditorSource(modelData.getId());
		if (bytes == null) {
			return Result.error("模型数据为空，请先成功设计流程并保存");
		}
		InputStream in = new ByteArrayInputStream(bytes);
		XMLInputFactory factory = XMLInputFactory.newFactory();
		XMLStreamReader reader;
		try {
			reader = factory.createXMLStreamReader(in);
		} catch (XMLStreamException e) {
			e.printStackTrace();
			return Result.error("读取流程图失败，modelId [ " + modelId + " ]");
		}
		BpmnModel model = new BpmnXMLConverter().convertToBpmnModel(reader);
		// 获取流程节点审批人
		Collection<FlowElement> flowElements = model.getMainProcess().getFlowElements();
		if (CollectionUtils.isNotEmpty(flowElements)) {
			for (FlowElement e : flowElements) {
				if (e instanceof UserTask) {
					UserTask userTask = (UserTask) e;

					ActKNode actKNode = new ActKNode();
					actKNode.setNodeId(userTask.getId());
					actKNode.setNodeName(userTask.getName());
					actKNode.setAssignee(userTask.getAssignee());
					actKNode.setCandidateUsers(Joiner.on(',').join(userTask.getCandidateUsers()));
					actKNode.setCandidateGroup(Joiner.on(',').join(userTask.getCandidateGroups()));
					actKNode.setIncoming(Joiner.on(',').join(userTask.getIncomingFlows()));
					actKNode.setOutgoing(Joiner.on(',').join(userTask.getOutgoingFlows()));
					actKNode.setModelId(modelId);
					actKNode.setPriority(userTask.getPriority());
					// actKNode.setProcessDefinitionId(StringUtils.EMPTY);
					// 设置任务节点类型
					ActKNodeDesign actKNodeDesign = this.actKNodeDesignService.getOne(new LambdaQueryWrapper<ActKNodeDesign>()
							.eq(ActKNodeDesign::getModelId, actKNode.getModelId())
							.eq(ActKNodeDesign::getNodeId, actKNode.getNodeId())
							);
					if(actKNodeDesign != null) {
						actKNode.setNodeType(actKNodeDesign.getNodeType());
						actKNode.setIsShowAppendForm(actKNodeDesign.getIsShowAppendForm());
						actKNode.setNodeScript(actKNodeDesign.getNodeScript());
					} else {
						actKNode.setNodeType(0);
						actKNode.setIsShowAppendForm(false);
						actKNode.setNodeScript("");
					}
					actKNodeList.add(actKNode);
				}
			}
		}
		return Result.OK(actKNodeList);
	}

}
