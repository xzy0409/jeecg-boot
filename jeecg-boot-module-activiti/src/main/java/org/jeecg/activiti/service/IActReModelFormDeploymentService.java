package org.jeecg.activiti.service;

import org.jeecg.activiti.entity.ActReModelFormDeployment;

import com.baomidou.mybatisplus.extension.service.IService;

public interface IActReModelFormDeploymentService extends IService<ActReModelFormDeployment> {

}
