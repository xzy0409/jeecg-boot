package org.jeecg.activiti.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.activiti.entity.ActReModelExtend;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.activiti.entity.ModelExtend;

public interface IActReModelExtendService extends IService<ActReModelExtend> {

    /**
     * 查询流程模型
     * @param page
     * @param modelE
     * @return
     */
    IPage<ModelExtend> selectListByPage(Page<ActReModelExtend> page, ModelExtend modelE);
}
