package org.jeecg.activiti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.activiti.entity.ActHiModelFormData;
import org.jeecg.activiti.entity.ActReModelFormData;

import java.util.List;
import java.util.Map;

public interface IActHiModelFormDataService extends IService<ActHiModelFormData> {

    /**
     * 将流程表单数据保存到历史
     * @param actReModelFormData
     */
    void saveHiFormData(ActReModelFormData actReModelFormData, String processInstanceId);

    /**
     * 查询附加表单数据
     * @param taskId
     * @param processInstanceId
     * @return
     */
    List<Map<String, Object>> handleNodeAppendForm(String taskId, String processInstanceId);
}
