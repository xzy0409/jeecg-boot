package org.jeecg.activiti.service;

import org.jeecg.activiti.entity.ActKNode;
import org.jeecg.common.api.vo.Result;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 流程节点service
 */
public interface IActKNodeService extends IService<ActKNode> {

	Result<Object> getUserTaskNodeListByModelId(String modelId);
}
