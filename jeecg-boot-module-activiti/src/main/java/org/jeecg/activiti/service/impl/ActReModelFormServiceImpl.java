package org.jeecg.activiti.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.activiti.entity.ActReModelForm;
import org.jeecg.activiti.mapper.ActReModelFormMapper;
import org.jeecg.activiti.service.IActReModelFormService;
import org.springframework.stereotype.Service;

@Service
public class ActReModelFormServiceImpl extends ServiceImpl<ActReModelFormMapper, ActReModelForm> implements IActReModelFormService {
}
