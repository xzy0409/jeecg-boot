package org.jeecg.activiti.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.jeecg.activiti.entity.ActReModelFormData;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * TODO
 *
 * @author dousw
 * @version 1.0
 */
@Mapper
public interface ActReModelFormDataMapper extends BaseMapper<ActReModelFormData> {

    /**
     * 查询流程表单数据
     * @param page
     * @param actReModelFormData
     * @return
     */
    List<ActReModelFormData> pageList(Page<ActReModelFormData> page, @Param("actReModelFormData") ActReModelFormData actReModelFormData);
}
