package org.jeecg.activiti.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.activiti.entity.ActKNodeDesign;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface ActKNodeDesignMapper extends BaseMapper<ActKNodeDesign> {

}
