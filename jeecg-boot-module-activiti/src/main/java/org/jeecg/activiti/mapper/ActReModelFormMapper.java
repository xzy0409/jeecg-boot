package org.jeecg.activiti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.jeecg.activiti.entity.ActReModelForm;

@Mapper
public interface ActReModelFormMapper extends BaseMapper<ActReModelForm> {

}
