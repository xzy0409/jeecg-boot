package org.jeecg.activiti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.activiti.entity.ActKProcessDuplicate;

public interface ActKProcessDuplicateMapper extends BaseMapper<ActKProcessDuplicate> {
}
