package org.jeecg.activiti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.activiti.entity.ActKHandleResult;
import org.apache.ibatis.annotations.Mapper;

/**
 * 任务节点处理结果Mapper
 */
@Mapper
public interface ActKHandleResultMapper extends BaseMapper<ActKHandleResult> {
}
