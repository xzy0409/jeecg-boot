package org.jeecg.activiti.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.jeecg.activiti.entity.ModelExtend;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface ActivitiModelMapper extends BaseMapper<ModelExtend> {
	
	public List<ModelExtend> getModelListAll(ModelExtend modelExtend);

	@Select("select arm.* from act_re_model_extend arme\n" +
            "        left join act_re_model arm on arme.model_id = arm.ID_\n" +
            "        where arme.is_del = 0 and arm.KEY_ = #{key}")
    List<Map<String, Object>> selectModelByKey(String key);
}
