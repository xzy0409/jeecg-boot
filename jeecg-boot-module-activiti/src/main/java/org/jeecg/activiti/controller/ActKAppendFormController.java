package org.jeecg.activiti.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import org.jeecg.activiti.entity.ActKAppendForm;
import org.jeecg.activiti.entity.ActKAppendFormDeployment;
import org.jeecg.activiti.entity.ActKNode;
import org.jeecg.activiti.service.IActKAppendFormDeploymentService;
import org.jeecg.activiti.service.IActKAppendFormService;
import org.jeecg.activiti.service.IActKNodeService;
import org.jeecg.common.api.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * TODO 流程节点附加表单管理
 *
 * @author dousw
 * @version 1.0
 */
@RestController
@RequestMapping("/activiti/appendForm")
@Api(tags = "流程节点附加表单管理")
public class ActKAppendFormController {

    @Autowired
    private IActKAppendFormService iActKAppendFormService;
	@Autowired
    private IActKAppendFormDeploymentService actKAppendFormDeploymentService;

    @Autowired
    private IActKNodeService iActKNodeService;

    @GetMapping("/getActKAppendForm")
    @ApiOperation(value = "查询流程节点附加表单", notes = "查询流程节点附加表单")
    public Result<Object> getActKAppendForm(@ApiParam("流程定义id") @RequestParam String modelId,
                                    @ApiParam("流程节点id") @RequestParam String nodeId){
        ActKAppendForm actKAppendForm = iActKAppendFormService.getOne(new QueryWrapper<ActKAppendForm>()
                .eq("model_id", modelId)
                .eq("node_id", nodeId));

        return Result.OK(actKAppendForm);
    }
	@GetMapping("/getActKAppendFormDeployment")
    @ApiOperation(value = "查询已发布的流程节点附加表单", notes = "查询已发布的流程节点附加表单")
    public Result<Object> getActKAppendFormDeployment(@ApiParam("流程定义id") @RequestParam String processDefinitionId,
    		@ApiParam("流程节点id") @RequestParam String nodeId){
    	ActKAppendFormDeployment actKAppendFormDeployment = this.actKAppendFormDeploymentService.getOne(new LambdaQueryWrapper<ActKAppendFormDeployment>()
    			.eq(ActKAppendFormDeployment::getProcessDefinitionId, processDefinitionId)
    			.eq(ActKAppendFormDeployment::getNodeId, nodeId)
    			);
    	return Result.OK(actKAppendFormDeployment);
    }

    /**
     * 保存/修改流程节点附加表单
     * @return
     */
    @PostMapping("/saveOrUpdate")
    @ApiOperation(value = "保存/修改流程节点附加表单", notes = "保存/修改流程节点附加表单")
    public Result<Object> saveOrUpdate(@RequestBody ActKAppendForm actKAppendForm){
        if (StringUtils.isEmpty(actKAppendForm.getId())) {
            iActKAppendFormService.save(actKAppendForm);

            iActKNodeService.update(new UpdateWrapper<ActKNode>()
                    .set("append_form_id", actKAppendForm.getId()).eq("node_id", actKAppendForm.getNodeId()));
        } else {
            iActKAppendFormService.updateById(actKAppendForm);
        }
        return Result.OK(actKAppendForm);
    }
}
