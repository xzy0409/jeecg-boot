package org.jeecg.activiti.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import org.apache.commons.lang3.StringUtils;
import org.jeecg.activiti.entity.ActKNode;
import org.jeecg.activiti.entity.ActKNodeDesign;
import org.jeecg.activiti.service.IActKNodeDesignService;
import org.jeecg.activiti.service.IActKNodeService;
import org.jeecg.activiti.service.impl.ActKNodeDesignServiceImpl;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.online.config.exception.ActScriptException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * TODO 流程节点管理
 *
 * @author dousw
 * @version 1.0
 */
@RestController
@RequestMapping("/activiti/node")
@Api(tags = "流程节点管理")
public class ActKNodeController {

    @Autowired
    private IActKNodeService iActKNodeService;
	@Autowired
    private IActKNodeDesignService actKNodeDesignService;

    @GetMapping("/getNodeListByProcessDefinitionId")
    @ApiOperation(value = "查询流程所有节点", notes = "查询流程所有节点")
    public Result<Object> getNodeListByProcessDefinitionId(@ApiParam("流程模型id") @RequestParam String processDefinitionId) {
        List<ActKNode> actKNodeList = iActKNodeService.list(new QueryWrapper<ActKNode>().eq("process_definition_id", processDefinitionId));
        return Result.OK(actKNodeList);
    }
	@GetMapping("/getNodeListByModelId")
    @ApiOperation(value = "查询流程所有节点", notes = "查询流程所有节点")
    public Result<Object> getNodeListByModelId(@ApiParam("流程模型id") @RequestParam String modelId) {
    	Result<Object> result = this.iActKNodeService.getUserTaskNodeListByModelId(modelId);
    	return result;
    }

    @PostMapping
    @ApiOperation(value = "修改节点任务类型", notes = "修改节点任务类型")
    public Result<Object> updateNode(@RequestBody ActKNode actKNode) {
    	ActKNodeDesign actKNodeDesign = new ActKNodeDesign();
    	actKNodeDesign.setModelId(actKNode.getModelId());
    	actKNodeDesign.setNodeId(actKNode.getNodeId());
    	actKNodeDesign.setNodeType(actKNode.getNodeType());
    	actKNodeDesign.setIsShowAppendForm(actKNode.getIsShowAppendForm());
    	boolean b = this.actKNodeDesignService.saveOrUpdate(actKNodeDesign, new LambdaQueryWrapper<ActKNodeDesign>()
    			.eq(ActKNodeDesign::getModelId, actKNode.getModelId())
    			.eq(ActKNodeDesign::getNodeId, actKNode.getNodeId()));
    	if(b) {
    		return Result.OK();
    	}
        return Result.error("保存失败");
    }

    @CrossOrigin(origins = {"*"})
    @PostMapping("/edit-node-script")
    @ApiOperation(value = "修改节点后置脚本", notes = "修改节点后置脚本")
    public Result<Object> editNodeScript(@RequestBody ActKNodeDesign design) throws ActScriptException {
        String nodeScript = design.getNodeScript();
       /* // todo   检测json脚本是否正确

        ActScript script = JSON.parseObject(nodeScript, ActScript.class);*/
        if(StringUtils.isBlank(nodeScript)){
        	LambdaQueryWrapper<ActKNodeDesign> qw = new LambdaQueryWrapper<ActKNodeDesign>()
                    .eq(ActKNodeDesign::getModelId, design.getModelId())
                    .eq(ActKNodeDesign::getNodeId, design.getNodeId());
        	ActKNodeDesign akDesign = actKNodeDesignService.getOne(qw);
        	akDesign.setNodeScript(null);
        	actKNodeDesignService.updateById(akDesign);
            return Result.OK();
        }
        ActKNodeDesignServiceImpl.validateScripts(nodeScript);


        ActKNodeDesign entity = new ActKNodeDesign();
        entity.setModelId(design.getModelId());
        entity.setNodeId(design.getNodeId());
        entity.setNodeScript(nodeScript);
        entity.setIsShowAppendForm(false);
        boolean b = this.actKNodeDesignService.saveOrUpdate(entity, new LambdaQueryWrapper<ActKNodeDesign>()
                .eq(ActKNodeDesign::getModelId, design.getModelId())
                .eq(ActKNodeDesign::getNodeId, design.getNodeId()));
        if(b) {
            return Result.OK();
        }
        return Result.error("保存失败");
    }

}
