package org.jeecg.activiti.controller;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.jeecg.activiti.entity.ActReModelForm;
import org.jeecg.activiti.entity.ActReModelFormDeployment;
import org.jeecg.activiti.entity.ModelExtend;
import org.jeecg.activiti.service.IActReModelFormDeploymentService;
import org.jeecg.activiti.service.IActReModelFormService;
import org.jeecg.activiti.service.IActivitiModelService;
import org.jeecg.common.api.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 流程表单管理
 */
@RestController
@RequestMapping("/activiti/form")
@Api(tags = "流程表单管理")
public class ActivitiFormController {

	@Autowired
	private IActReModelFormService iActReModelFormService;
	@Autowired
	private IActReModelFormDeploymentService iActReModelFormDeploymentService;
	@Autowired
    private IActivitiModelService activitiModelService;

	/**
	 * 我的任务 -&gt; 历史任务 -&gt; 点击历史按钮 -&gt; 附加单据表单
	 * 
	 * @param processDefinitionId
	 * @return
	 */
	@GetMapping("/getFormByProcessDefinitionId")
	@ApiOperation(value = "根据流程定义ID查询部署表单", notes = "根据流程定义ID查询部署表单")
	public Result<Object> getFormByProcessDefinitionId(@ApiParam(name = "processDefinitionId", value = "流程定义ID", required = true) @RequestParam(name = "processDefinitionId", required = true) String processDefinitionId) {
		ActReModelFormDeployment actReModelFormDeployment = this.iActReModelFormDeploymentService.getOne(new QueryWrapper<ActReModelFormDeployment>().eq("process_Definition_Id", processDefinitionId));
		return Result.OK(actReModelFormDeployment);
	}

	/**
	 * 流程管理 --&gt; 设计表单
	 * 
	 * @param modelId
	 * @return
	 */
	@GetMapping("/getFormByModelId")
	@ApiOperation(value = "根据模型ID查询设计表单", notes = "根据模型ID查询设计表单")
	public Result<Object> getFormByModelId(@ApiParam(name = "modelId", value = "模型定义ID", required = true) @RequestParam(name = "modelId", required = true) String modelId) {
		ActReModelForm actReModelForm = iActReModelFormService.getOne(new QueryWrapper<ActReModelForm>().eq("model_id", modelId));
		return Result.OK(actReModelForm);
	}
	
	/**
	 * 流程管理 --&gt; 设计表单
	 * 
	 * @param modelId
	 * @return
	 */
	@GetMapping("/getFormDeploymentByModelId")
	@ApiOperation(value = "根据模型ID查询设计表单", notes = "根据模型ID查询设计表单")
	public Result<Object> getFormDeploymentByModelId(@ApiParam(name = "modelId", value = "模型定义ID", required = true) @RequestParam(name = "modelId", required = true) String modelId,Boolean isShow) {
		ModelExtend modelExtend = new ModelExtend();
        modelExtend.setId(modelId);
		modelExtend.setIsShow(isShow);
		List<ModelExtend> modelExtendList = this.activitiModelService.getModelListAll(modelExtend);
		if(CollectionUtils.isEmpty(modelExtendList)) {
			return Result.error("请先发布流程");
		} else if (modelExtendList.size() > 1) {
			return Result.error("数据出错了...");
		}
		modelExtend = modelExtendList.get(0);
		ActReModelFormDeployment actReModelFormDeployment = this.iActReModelFormDeploymentService.getOne(new LambdaQueryWrapper<ActReModelFormDeployment>()
				.eq(ActReModelFormDeployment::getModelId, modelExtend.getId())
				.eq(ActReModelFormDeployment::getProcessDefinitionId, modelExtend.getProcessDefinitionId())
				);
		return Result.OK(actReModelFormDeployment);
	}

	/**
	 * 新增或修改表单
	 * 
	 * @param actReModelForm
	 * @return
	 */
	@PostMapping("/saveOrUpdateForm")
	@ApiOperation(value = "流程表单新增/修改", notes = "流程表单新增/修改")
	public Result<Object> saveOrUpdateForm(
			@ApiParam(name = "actReModelForm", value = "表单对象", required = true) @RequestBody ActReModelForm actReModelForm) {
		boolean result = true;

		if (StringUtils.isEmpty(actReModelForm.getId())) {
			result = iActReModelFormService.save(actReModelForm);
		} else {
			result = iActReModelFormService.updateById(actReModelForm);
		}

		return result == true ? Result.OK() : Result.error("表单保存失败。");
	}
}
