package org.jeecg.activiti.entity;

import org.jeecg.common.system.base.entity.JeecgEntity;

import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("act_k_node_design")
public class ActKNodeDesign extends JeecgEntity {

	private static final long serialVersionUID = 5469988721093999353L;

	/**
	 * 流程节点ID
	 */
	private String nodeId;

	/**
	 * 流程模型ID
	 */
	private String modelId;

	/**
	 * 节点类型（0:工作流、1:业务流）
	 */
	private int nodeType;

	/**
	 * 是否显示附加表单
	 */
	private Boolean isShowAppendForm;

	/**
	 * 节点脚本
	 */
	private String nodeScript;

}
