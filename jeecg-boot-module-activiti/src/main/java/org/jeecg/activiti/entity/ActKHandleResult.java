package org.jeecg.activiti.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import org.jeecg.common.system.base.entity.JeecgEntity;

/**
 * TODO 任务节点处理结果实体类
 *
 * @author dousw
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("act_k_handle_result")
public class ActKHandleResult extends JeecgEntity {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
     * 流程实例ID
     */
    private String processInstanceId;
    /**
     * 任务节点ID
     */
    private String nodeId;
    /**
     * 处理意见
     */
    private String comment;
    /**
     * 处理结果
     */
    private String result;
    /**
     * 任务ID
     */
    private String taskId;

    /**
     * 执行ID
     */
    private String executionId;
}
