package org.jeecg.activiti.entity;

import org.jeecg.common.system.base.entity.JeecgEntity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 流程节点表单管理表
 *
 * @author dousw
 * @version 1.0
 */

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("act_k_append_form_data")
public class ActKAppendFormData extends JeecgEntity {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
     * 节点ID
     */
    private String nodeId;
    /**
     * 附加表单
     */
    private String formId;
    /**
     * 流程ID
     */
    private String modelId;
    /**
     * 用户名称
     */
    private String username;
    /**
     * 流程实例ID
     */
    private String processInstanceId;
    /**
     * 表单数据
     */
    private String formData;

    /**
     * 执行ID
     */
    private String executionId;

    /**
     * 任务ID
     */
    private String taskId;

}
