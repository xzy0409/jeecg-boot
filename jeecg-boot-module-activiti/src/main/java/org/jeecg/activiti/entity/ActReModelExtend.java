package org.jeecg.activiti.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 流程模型扩展表
 */
@Data
@TableName("act_re_model_extend")
public class ActReModelExtend implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 流程模型ID
     */
    private String modelId;

    /**
     * 业务板块
     */
    private String businessPlate;

    /**
     * 模型图标
     */
    private String icon;

    /**
     * 是否显示（0、否 1、是）
     */
    private Boolean isShow;
    
    /**
     * 表单类型（0、自定义 1、Onl表单）
     */
    private String formType;
    
    /**
     * tableName
     */
    private String tableName;
    
    /**
     * 流程状态字段
     */
    private String flowStatusCol;

    /**
     * 流程描述
     */
    private String description;

    /**
     * 流程定义ID
     */
    private String processDefinitionId;

    /**
     * 流程是否发布（0、未发布 1、已发布）
     */
    private Integer status;
    /**
     * 流程发布时间
     */
    private Date publishTime;

    /**
     * 是否删除
     */
    private Boolean isDel;
    
    /**
     * 报表模板编码
     */
    private String reportCode;
    
    private String title;
    
    private String profiles;
}
