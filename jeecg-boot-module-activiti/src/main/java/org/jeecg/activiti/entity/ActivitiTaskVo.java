package org.jeecg.activiti.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class ActivitiTaskVo implements Serializable {

	private static final long serialVersionUID = -6036948143633228318L;
	private String modelName;
	private String modelKey;
	private String tableId;
	private String tableName;
	private String formDataId;
	private String formId;
	private String creator;
	private String instanceName;
	private String id;
	private String name;
	private Date createTime;
	private String assignee;
	private String processInstanceId;
	private String executionId;
	private String processDefinitionId;
	private String parentTaskId;
	private Integer nodeType;
	private String nodeId;
	
	private String modelExtendBusinessPlate;
	private Date formDataCreateTime;
	private String formData;
	private String modelId;
	private Integer status;

	/**
	 * 任务类型：1:普通任务;0:组任务
	 */
	private Integer taskType;
	
	/**
	 * 角色组（任务组）
	 */
	private List<String> roleList;
	
	/**
	 * 任务发起人查询条件（前台传参）
	 */
	private String taskInitiators;
	
	/**
	 * 任务发起人查询条件（后台查询）
	 */
	private List<String> taskInitiatorList;
	
	/**
	 * 表单
	 */
	private ActKAppendFormDeployment appendForm;

}
