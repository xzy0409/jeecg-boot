package org.jeecg.modules.online.cgreport.service;

import org.jeecg.modules.online.cgreport.entity.DiagramCombinationDetail;

import com.baomidou.mybatisplus.extension.service.IService;

public interface IDiagramCombinationDetailService extends IService<DiagramCombinationDetail> {

}
