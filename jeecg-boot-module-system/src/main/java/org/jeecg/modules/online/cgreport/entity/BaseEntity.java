package org.jeecg.modules.online.cgreport.entity;/*package org.jeecg.modules.online.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.GenericGenerator;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@MappedSuperclass
public class BaseEntity {
	
	@TableId(type = IdType.ASSIGN_ID)
	@ApiModelProperty(value = "主键")
	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid")
	private String id;

	@Excel(name = "创建人", width = 15)
	@ApiModelProperty(value = "创建人")
	@Column(name = "CREATE_BY")
	private String createBy;// "admin"

	@Excel(name = "创建日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "创建日期")
	@Column(name = "CREATE_TIME")
	private Date createTime;// "2019-04-28 16:47:37"

	@Excel(name = "更新人", width = 15)
	@ApiModelProperty(value = "更新人")
	@Column(name = "UPDATE_BY")
	private String updateBy;// "jeecg"

	@Excel(name = "更新日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "更新日期")
	@Column(name = "UPDATE_TIME")
	private Date updateTime;// "2020-05-06 10:24:17"

}
*/