package org.jeecg.modules.online.cgform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.online.cgform.entity.OnlCgformEnhanceJava;

public interface OnlCgformEnhanceJavaMapper
    extends BaseMapper<OnlCgformEnhanceJava>
{
}
