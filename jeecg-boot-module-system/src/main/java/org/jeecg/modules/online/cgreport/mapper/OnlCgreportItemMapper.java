package org.jeecg.modules.online.cgreport.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.online.cgreport.entity.OnlCgreportItem;

public interface OnlCgreportItemMapper extends BaseMapper<OnlCgreportItem> {
}
