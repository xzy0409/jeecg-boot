package org.jeecg.modules.online.cgreport.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.online.cgreport.entity.OnlCgreportItem;
import org.jeecg.modules.online.cgreport.mapper.OnlCgreportItemMapper;
import org.jeecg.modules.online.cgreport.service.IOnlCgreportItemService;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * TODO
 *
 * @author dousw
 * @version 1.0
 */
@Service("onlCgreportItemServiceImpl")
public class OnlCgreportItemServiceImpl extends ServiceImpl<OnlCgreportItemMapper, OnlCgreportItem> implements IOnlCgreportItemService {

    public List<Map<String, String>> getAutoListQueryInfo(String cgrheadId) {
        LambdaQueryWrapper<OnlCgreportItem> qw = new LambdaQueryWrapper<>();
        qw.eq(OnlCgreportItem::getCgrheadId, cgrheadId);
        qw.eq(OnlCgreportItem::getIsSearch, 1);
        List<OnlCgreportItem> items = this.list(qw);
        List<Map<String, String>> result = new ArrayList<>();
        int i = 0;

        Map<String,String> record;
        for(Iterator<OnlCgreportItem> itemIterator = items.iterator(); itemIterator.hasNext(); result.add(record)) {
            OnlCgreportItem item = itemIterator.next();
            record = new HashMap<>();
            record.put("label", item.getFieldTxt());
            if (oConvertUtils.isNotEmpty(item.getDictCode())) {
            	record.put("view", "list");
            } else {
            	record.put("view", item.getFieldType().toLowerCase());
            }

            record.put("mode", oConvertUtils.isEmpty(item.getSearchMode()) ? "single" : item.getSearchMode());
            record.put("field", item.getFieldName());
            ++i;
            if (i > 2) {
            	record.put("hidden", "1");
            }
        }

        return result;
    }
}
