package org.jeecg.modules.online.cgform.util;


public class FieldSlotType
{
    public static final String FILE = "file";
    public static final String IMAGE = "image";
    public static final String EDITOR = "editor";
    public static final String DATE = "date";
    public static final String PCA = "pca";
    public static final String MONTH = "month";
    public static final String POPUP = "popup";
    public static final String SWITCH = "switch";
    public static final String LINK_DOWN = "link_down";
    public static final String SEL_TREE = "sel_tree";
    public static final String CAT_TREE = "cat_tree";
    public static final String SEL_DEPART = "sel_depart";
    public static final String SEL_USER = "sel_user";
}
