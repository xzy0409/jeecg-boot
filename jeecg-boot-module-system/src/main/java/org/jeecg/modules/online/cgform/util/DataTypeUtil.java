package org.jeecg.modules.online.cgform.util;

import com.alibaba.fastjson.JSONObject;
import java.math.BigDecimal;
import java.util.Map;
import org.jeecg.modules.online.cgform.entity.OnlCgformField;
import org.jeecg.modules.online.config.util.DbType;

public class DataTypeUtil {

    public static boolean isNumberType(String type) {
        return "int".equals(type) || "double".equals(type) || "BigDecimal".equals(type) || "Integer".equals(type);
    }

    public static boolean isDateType(String type) {
        return "Date".equalsIgnoreCase(type) || "datetime".equalsIgnoreCase(type) || "Timestamp".equalsIgnoreCase(type);
    }

    public static String getSql(String databaseType, OnlCgformField onlCgformField, JSONObject jsonObject, Map<String, Object> map) {
        String dbType = onlCgformField.getDbType();
        String dbFieldName = onlCgformField.getDbFieldName();
        String fieldShowType = onlCgformField.getFieldShowType();
        if (jsonObject.get(dbFieldName) == null) {
            return "null";
        } else if (DbType.INT.equals(dbType)) {
            map.put(dbFieldName, jsonObject.getIntValue(dbFieldName));
            return "#{" + dbFieldName + ",jdbcType=INTEGER}";
        } else if (DbType.DOUBLE.equals(dbType)) {
            map.put(dbFieldName, jsonObject.getDoubleValue(dbFieldName));
            return "#{" + dbFieldName + ",jdbcType=DOUBLE}";
        } else if (DbType.BIG_DECIMAL.equals(dbType)) {
            map.put(dbFieldName, new BigDecimal(jsonObject.getString(dbFieldName)));
            return "#{" + dbFieldName + ",jdbcType=DECIMAL}";
        } else if (DbType.BLOB.equals(dbType)) {
            map.put(dbFieldName, jsonObject.getString(dbFieldName) != null ? jsonObject.getString(dbFieldName).getBytes() : null);
            return "#{" + dbFieldName + ",jdbcType=BLOB}";
        } else if (DbType.DATE.equals(dbType)) {
            String dbFieldNameStr = jsonObject.getString(dbFieldName);
            if ("ORACLE".equals(databaseType)) {
                if ("date".equals(fieldShowType)) {
                    map.put(dbFieldName, dbFieldNameStr.length() > 10 ? dbFieldNameStr.substring(0, 10) : dbFieldNameStr);
                    return "to_date(#{" + dbFieldName + "},'yyyy-MM-dd')";
                } else {
                    map.put(dbFieldName, dbFieldNameStr.length() == 10 ? jsonObject.getString(dbFieldName) + " 00:00:00" : dbFieldNameStr);
                    return "to_date(#{" + dbFieldName + "},'yyyy-MM-dd HH24:mi:ss')";
                }
            } else if ("POSTGRESQL".equals(databaseType)) {
                if ("date".equals(fieldShowType)) {
                    map.put(dbFieldName, dbFieldNameStr.length() > 10 ? dbFieldNameStr.substring(0, 10) : dbFieldNameStr);
                    return "CAST(#{" + dbFieldName + "} as TIMESTAMP)";
                } else {
                    map.put(dbFieldName, dbFieldNameStr.length() == 10 ? jsonObject.getString(dbFieldName) + " 00:00:00" : dbFieldNameStr);
                    return "CAST(#{" + dbFieldName + "} as TIMESTAMP)";
                }
            } else {
                map.put(dbFieldName, jsonObject.getString(dbFieldName));
                return "#{" + dbFieldName + "}";
            }
        } else {
            map.put(dbFieldName, jsonObject.getString(dbFieldName));
            return "#{" + dbFieldName + ",jdbcType=VARCHAR}";
        }
    }
}
