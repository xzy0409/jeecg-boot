package org.jeecg.modules.online.cgform.service.impl;

import org.jeecg.modules.online.cgform.entity.OnlSystem;
import org.jeecg.modules.online.cgform.mapper.OnlSystemMapper;
import org.jeecg.modules.online.cgform.service.IOnlSystemService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * 子系统
 * @author: MxpIO
 * @version: V1.0
 */
@Service
public class OnlSystemServiceImpl extends ServiceImpl<OnlSystemMapper, OnlSystem> implements IOnlSystemService {

}
