package org.jeecg.modules.online.cgreport.mapper;

import org.jeecg.modules.online.cgreport.entity.DiagramFieldConfiguration;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 图表配置
 */
public interface DiagramFieldConfigurationMapper extends BaseMapper<DiagramFieldConfiguration> {

}
