package org.jeecg.modules.online.cgform.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jeecg.autopoi.poi.handler.impl.ExcelDataHandlerDefaultImpl;
import org.jeecg.autopoi.poi.util.PoiPublicUtil;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.common.util.SpringContextUtils;
import org.jeecg.modules.online.cgform.entity.OnlCgformField;

public class ExcelDataHandlerUtil extends ExcelDataHandlerDefaultImpl {
	Map<String, OnlCgformField> onlCgformFieldMap;
	ISysBaseAPI sysBaseApi;
	String upLoadPath;
	String online;
	String uploadType;

	public ExcelDataHandlerUtil(List<OnlCgformField> fields, String upLoadPath, String uploadType) {
		this.onlCgformFieldMap = this.getOnlCgformFieldMapByFields(fields);
		this.upLoadPath = upLoadPath;
		this.online = "online";
		this.uploadType = uploadType;
		this.sysBaseApi = (ISysBaseAPI) SpringContextUtils.getBean(ISysBaseAPI.class);
	}

	private Map<String, OnlCgformField> getOnlCgformFieldMapByFields(List<OnlCgformField> fields) {
		Map<String, OnlCgformField> result = new HashMap<>();
		Iterator<OnlCgformField> iterator = fields.iterator();

		while (iterator.hasNext()) {
			OnlCgformField field = iterator.next();
			result.put(field.getDbFieldTxt(), field);
		}

		return result;
	}

	public void setMapValue(Map<String, Object> map, String originKey, Object value) {
		String var4 = this.a(originKey);
		if (value instanceof Double) {
			map.put(var4, PoiPublicUtil.doubleToString((Double) value));
		} else if (value instanceof byte[]) {
			byte[] var5 = (byte[]) ((byte[]) value);
			String var6 = SqlSymbolUtil.uploadOnlineImage(var5, this.upLoadPath, this.online,
					this.uploadType);
			if (var6 != null) {
				map.put(var4, var6);
			}
		} else {
			map.put(var4, value == null ? "" : value.toString());
		}

	}

	private String a(String var1) {
		return this.onlCgformFieldMap.containsKey(var1)
				? "$mainTable$" + ((OnlCgformField) this.onlCgformFieldMap.get(var1)).getDbFieldName()
				: "$subTable$" + var1;
	}
}
