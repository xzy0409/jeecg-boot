package org.jeecg.modules.online.cgform.entity;

import java.io.Serializable;
import java.util.Date;

public class OnlCgformHead implements Serializable {

	public String getId() {
		return id;
	}

	public String getTableName() {
		return tableName;
	}

	public Integer getTableType() {
		return tableType;
	}

	public Integer getTableVersion() {
		return tableVersion;
	}

	public String getTableTxt() {
		return tableTxt;
	}

	public String getIsCheckbox() {
		return isCheckbox;
	}

	public String getIsDbSynch() {
		return isDbSynch;
	}

	public String getIsPage() {
		return isPage;
	}

	public String getIsTree() {
		return isTree;
	}

	public String getIdSequence() {
		return idSequence;
	}

	public String getIdType() {
		return idType;
	}

	public String getQueryMode() {
		return queryMode;
	}

	public Integer getRelationType() {
		return relationType;
	}

	public String getSubTableStr() {
		return subTableStr;
	}

	public Integer getTabOrderNum() {
		return tabOrderNum;
	}

	public String getTreeParentIdField() {
		return treeParentIdField;
	}

	public String getTreeIdField() {
		return treeIdField;
	}

	public String getTreeFieldname() {
		return treeFieldname;
	}

	public String getFormCategory() {
		return formCategory;
	}

	public String getFormTemplate() {
		return formTemplate;
	}

	public String getThemeTemplate() {
		return themeTemplate;
	}

	public String getFormTemplateMobile() {
		return formTemplateMobile;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public String getCreateBy() {
		return createBy;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public Integer getCopyType() {
		return copyType;
	}

	public Integer getCopyVersion() {
		return copyVersion;
	}

	public String getPhysicId() {
		return physicId;
	}

	public Integer getHascopy() {
		return hascopy;
	}

	public Integer getScroll() {
		return scroll;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public void setTableType(Integer tableType) {
		this.tableType = tableType;
	}

	public void setTableVersion(Integer tableVersion) {
		this.tableVersion = tableVersion;
	}

	public void setTableTxt(String tableTxt) {
		this.tableTxt = tableTxt;
	}

	public void setIsCheckbox(String isCheckbox) {
		this.isCheckbox = isCheckbox;
	}

	public void setIsDbSynch(String isDbSynch) {
		this.isDbSynch = isDbSynch;
	}

	public void setIsPage(String isPage) {
		this.isPage = isPage;
	}

	public void setIsTree(String isTree) {
		this.isTree = isTree;
	}

	public void setIdSequence(String idSequence) {
		this.idSequence = idSequence;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public void setQueryMode(String queryMode) {
		this.queryMode = queryMode;
	}

	public void setRelationType(Integer relationType) {
		this.relationType = relationType;
	}

	public void setSubTableStr(String subTableStr) {
		this.subTableStr = subTableStr;
	}

	public void setTabOrderNum(Integer tabOrderNum) {
		this.tabOrderNum = tabOrderNum;
	}

	public void setTreeParentIdField(String treeParentIdField) {
		this.treeParentIdField = treeParentIdField;
	}

	public void setTreeIdField(String treeIdField) {
		this.treeIdField = treeIdField;
	}

	public void setTreeFieldname(String treeFieldname) {
		this.treeFieldname = treeFieldname;
	}

	public void setFormCategory(String formCategory) {
		this.formCategory = formCategory;
	}

	public void setFormTemplate(String formTemplate) {
		this.formTemplate = formTemplate;
	}

	public void setThemeTemplate(String themeTemplate) {
		this.themeTemplate = themeTemplate;
	}

	public void setFormTemplateMobile(String formTemplateMobile) {
		this.formTemplateMobile = formTemplateMobile;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public void setCopyType(Integer copyType) {
		this.copyType = copyType;
	}

	public void setCopyVersion(Integer copyVersion) {
		this.copyVersion = copyVersion;
	}

	public void setPhysicId(String physicId) {
		this.physicId = physicId;
	}

	public void setHascopy(Integer hascopy) {
		this.hascopy = hascopy;
	}

	public void setScroll(Integer scroll) {
		this.scroll = scroll;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getIsShowSubTable() {
		return isShowSubTable;
	}

	public void setIsShowSubTable(String isShowSubTable) {
		this.isShowSubTable = isShowSubTable;
	}

	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof OnlCgformHead))
			return false;
		OnlCgformHead onlcgformhead = (OnlCgformHead) o;
		if (!onlcgformhead.canEqual(this))
			return false;
		String s = getId();
		String s1 = onlcgformhead.getId();
		if (s != null ? !s.equals(s1) : s1 != null)
			return false;
		String s2 = getTableName();
		String s3 = onlcgformhead.getTableName();
		if (s2 != null ? !s2.equals(s3) : s3 != null)
			return false;
		Integer integer = getTableType();
		Integer integer1 = onlcgformhead.getTableType();
		if (integer != null ? !integer.equals(integer1) : integer1 != null)
			return false;
		Integer integer2 = getTableVersion();
		Integer integer3 = onlcgformhead.getTableVersion();
		if (integer2 != null ? !integer2.equals(integer3) : integer3 != null)
			return false;
		String s4 = getTableTxt();
		String s5 = onlcgformhead.getTableTxt();
		if (s4 != null ? !s4.equals(s5) : s5 != null)
			return false;
		String s6 = getIsCheckbox();
		String s7 = onlcgformhead.getIsCheckbox();
		if (s6 != null ? !s6.equals(s7) : s7 != null)
			return false;
		String s8 = getIsDbSynch();
		String s9 = onlcgformhead.getIsDbSynch();
		if (s8 != null ? !s8.equals(s9) : s9 != null)
			return false;
		String s10 = getIsPage();
		String s11 = onlcgformhead.getIsPage();
		if (s10 != null ? !s10.equals(s11) : s11 != null)
			return false;
		String s12 = getIsTree();
		String s13 = onlcgformhead.getIsTree();
		if (s12 != null ? !s12.equals(s13) : s13 != null)
			return false;
		String s14 = getIdSequence();
		String s15 = onlcgformhead.getIdSequence();
		if (s14 != null ? !s14.equals(s15) : s15 != null)
			return false;
		String s16 = getIdType();
		String s17 = onlcgformhead.getIdType();
		if (s16 != null ? !s16.equals(s17) : s17 != null)
			return false;
		String s18 = getQueryMode();
		String s19 = onlcgformhead.getQueryMode();
		if (s18 != null ? !s18.equals(s19) : s19 != null)
			return false;
		Integer integer4 = getRelationType();
		Integer integer5 = onlcgformhead.getRelationType();
		if (integer4 != null ? !integer4.equals(integer5) : integer5 != null)
			return false;
		String s20 = getSubTableStr();
		String s21 = onlcgformhead.getSubTableStr();
		if (s20 != null ? !s20.equals(s21) : s21 != null)
			return false;
		Integer integer6 = getTabOrderNum();
		Integer integer7 = onlcgformhead.getTabOrderNum();
		if (integer6 != null ? !integer6.equals(integer7) : integer7 != null)
			return false;
		String s22 = getTreeParentIdField();
		String s23 = onlcgformhead.getTreeParentIdField();
		if (s22 != null ? !s22.equals(s23) : s23 != null)
			return false;
		String s24 = getTreeIdField();
		String s25 = onlcgformhead.getTreeIdField();
		if (s24 != null ? !s24.equals(s25) : s25 != null)
			return false;
		String s26 = getTreeFieldname();
		String s27 = onlcgformhead.getTreeFieldname();
		if (s26 != null ? !s26.equals(s27) : s27 != null)
			return false;
		String s28 = getFormCategory();
		String s29 = onlcgformhead.getFormCategory();
		if (s28 != null ? !s28.equals(s29) : s29 != null)
			return false;
		String s30 = getFormTemplate();
		String s31 = onlcgformhead.getFormTemplate();
		if (s30 != null ? !s30.equals(s31) : s31 != null)
			return false;
		String s32 = getThemeTemplate();
		String s33 = onlcgformhead.getThemeTemplate();
		if (s32 != null ? !s32.equals(s33) : s33 != null)
			return false;
		String s34 = getFormTemplateMobile();
		String s35 = onlcgformhead.getFormTemplateMobile();
		if (s34 != null ? !s34.equals(s35) : s35 != null)
			return false;
		String s36 = getUpdateBy();
		String s37 = onlcgformhead.getUpdateBy();
		if (s36 != null ? !s36.equals(s37) : s37 != null)
			return false;
		Date date = getUpdateTime();
		Date date1 = onlcgformhead.getUpdateTime();
		if (date != null ? !date.equals(date1) : date1 != null)
			return false;
		String s38 = getCreateBy();
		String s39 = onlcgformhead.getCreateBy();
		if (s38 != null ? !s38.equals(s39) : s39 != null)
			return false;
		Date date2 = getCreateTime();
		Date date3 = onlcgformhead.getCreateTime();
		if (date2 != null ? !date2.equals(date3) : date3 != null)
			return false;
		Integer integer8 = getCopyType();
		Integer integer9 = onlcgformhead.getCopyType();
		if (integer8 != null ? !integer8.equals(integer9) : integer9 != null)
			return false;
		Integer integer10 = getCopyVersion();
		Integer integer11 = onlcgformhead.getCopyVersion();
		if (integer10 != null ? !integer10.equals(integer11) : integer11 != null)
			return false;
		String s40 = getPhysicId();
		String s41 = onlcgformhead.getPhysicId();
		if (s40 != null ? !s40.equals(s41) : s41 != null)
			return false;
		Integer integer12 = getScroll();
		Integer integer13 = onlcgformhead.getScroll();
		return integer12 != null ? integer12.equals(integer13) : integer13 == null;
	}

	protected boolean canEqual(Object other) {
		return other instanceof OnlCgformHead;
	}

	public int hashCode() {
		int i = 1;
		String s = getId();
		i = i * 59 + (s != null ? s.hashCode() : 43);
		String s1 = getTableName();
		i = i * 59 + (s1 != null ? s1.hashCode() : 43);
		Integer integer = getTableType();
		i = i * 59 + (integer != null ? integer.hashCode() : 43);
		Integer integer1 = getTableVersion();
		i = i * 59 + (integer1 != null ? integer1.hashCode() : 43);
		String s2 = getTableTxt();
		i = i * 59 + (s2 != null ? s2.hashCode() : 43);
		String s3 = getIsCheckbox();
		i = i * 59 + (s3 != null ? s3.hashCode() : 43);
		String s4 = getIsDbSynch();
		i = i * 59 + (s4 != null ? s4.hashCode() : 43);
		String s5 = getIsPage();
		i = i * 59 + (s5 != null ? s5.hashCode() : 43);
		String s6 = getIsTree();
		i = i * 59 + (s6 != null ? s6.hashCode() : 43);
		String s7 = getIdSequence();
		i = i * 59 + (s7 != null ? s7.hashCode() : 43);
		String s8 = getIdType();
		i = i * 59 + (s8 != null ? s8.hashCode() : 43);
		String s9 = getQueryMode();
		i = i * 59 + (s9 != null ? s9.hashCode() : 43);
		Integer integer2 = getRelationType();
		i = i * 59 + (integer2 != null ? integer2.hashCode() : 43);
		String s10 = getSubTableStr();
		i = i * 59 + (s10 != null ? s10.hashCode() : 43);
		Integer integer3 = getTabOrderNum();
		i = i * 59 + (integer3 != null ? integer3.hashCode() : 43);
		String s11 = getTreeParentIdField();
		i = i * 59 + (s11 != null ? s11.hashCode() : 43);
		String s12 = getTreeIdField();
		i = i * 59 + (s12 != null ? s12.hashCode() : 43);
		String s13 = getTreeFieldname();
		i = i * 59 + (s13 != null ? s13.hashCode() : 43);
		String s14 = getFormCategory();
		i = i * 59 + (s14 != null ? s14.hashCode() : 43);
		String s15 = getFormTemplate();
		i = i * 59 + (s15 != null ? s15.hashCode() : 43);
		String s16 = getThemeTemplate();
		i = i * 59 + (s16 != null ? s16.hashCode() : 43);
		String s17 = getFormTemplateMobile();
		i = i * 59 + (s17 != null ? s17.hashCode() : 43);
		String s18 = getUpdateBy();
		i = i * 59 + (s18 != null ? s18.hashCode() : 43);
		Date date = getUpdateTime();
		i = i * 59 + (date != null ? date.hashCode() : 43);
		String s19 = getCreateBy();
		i = i * 59 + (s19 != null ? s19.hashCode() : 43);
		Date date1 = getCreateTime();
		i = i * 59 + (date1 != null ? date1.hashCode() : 43);
		Integer integer4 = getCopyType();
		i = i * 59 + (integer4 != null ? integer4.hashCode() : 43);
		Integer integer5 = getCopyVersion();
		i = i * 59 + (integer5 != null ? integer5.hashCode() : 43);
		String s20 = getPhysicId();
		i = i * 59 + (s20 != null ? s20.hashCode() : 43);
		Integer integer6 = getScroll();
		i = i * 59 + (integer6 != null ? integer6.hashCode() : 43);
		return i;
	}

	public String toString() {
		return (new StringBuilder()).append("OnlCgformHead(id=").append(getId()).append(", tableName=")
				.append(getTableName()).append(", tableType=").append(getTableType()).append(", tableVersion=")
				.append(getTableVersion()).append(", tableTxt=").append(getTableTxt()).append(", isCheckbox=")
				.append(getIsCheckbox()).append(", isDbSynch=").append(getIsDbSynch()).append(", isPage=")
				.append(getIsPage()).append(", isTree=").append(getIsTree()).append(", idSequence=")
				.append(getIdSequence()).append(", idType=").append(getIdType()).append(", queryMode=")
				.append(getQueryMode()).append(", relationType=").append(getRelationType()).append(", subTableStr=")
				.append(getSubTableStr()).append(", tabOrderNum=").append(getTabOrderNum())
				.append(", treeParentIdField=").append(getTreeParentIdField()).append(", treeIdField=")
				.append(getTreeIdField()).append(", treeFieldname=").append(getTreeFieldname())
				.append(", formCategory=").append(getFormCategory()).append(", formTemplate=").append(getFormTemplate())
				.append(", themeTemplate=").append(getThemeTemplate()).append(", formTemplateMobile=")
				.append(getFormTemplateMobile()).append(", updateBy=").append(getUpdateBy()).append(", updateTime=")
				.append(getUpdateTime()).append(", createBy=").append(getCreateBy()).append(", createTime=")
				.append(getCreateTime()).append(", copyType=").append(getCopyType()).append(", copyVersion=")
				.append(getCopyVersion()).append(", physicId=").append(getPhysicId()).append(", hascopy=")
				.append(getHascopy()).append(", scroll=").append(getScroll()).append(", taskId=").append(getTaskId())
				.append(")").toString();
	}

	private static final long serialVersionUID = 1L;
	private String id;
	private String tableName;
	private Integer tableType;
	private Integer tableVersion;
	private String tableTxt;
	private String isCheckbox;
	private String isDbSynch;
	private String isPage;
	private String isTree;
	private String idSequence;
	private String idType;
	private String queryMode;
	private Integer relationType;
	private String subTableStr;
	private Integer tabOrderNum;
	private String treeParentIdField;
	private String treeIdField;
	private String treeFieldname;
	private String formCategory;
	private String formTemplate;
	private String themeTemplate;
	private String formTemplateMobile;
	private String updateBy;
	private Date updateTime;
	private String createBy;
	private Date createTime;
	private Integer copyType;
	private Integer copyVersion;
	private String physicId;
	private transient Integer hascopy;
	private Integer scroll;
	private transient String taskId;
	private String isShowSubTable;
}
