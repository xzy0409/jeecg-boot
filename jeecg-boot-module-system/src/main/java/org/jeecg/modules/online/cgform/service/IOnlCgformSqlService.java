package org.jeecg.modules.online.cgform.service;

import java.util.List;
import java.util.Map;

import org.jeecg.modules.online.cgform.entity.OnlCgformField;
import org.jeecg.modules.online.cgform.entity.OnlCgformHead;
import org.jeecg.modules.online.config.exception.BusinessException;

public interface IOnlCgformSqlService
{

    public void saveBatchOnlineTable(OnlCgformHead onlcgformhead, List<OnlCgformField> list, List<Map<String, Object>> list1);

    public void saveOrUpdateSubData(String s, OnlCgformHead onlcgformhead, List<OnlCgformField> list)
        throws BusinessException;
}
