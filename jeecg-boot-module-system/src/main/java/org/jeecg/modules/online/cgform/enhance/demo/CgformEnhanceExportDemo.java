package org.jeecg.modules.online.cgform.enhance.demo;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.common.system.vo.SysCategoryModel;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.online.cgform.enhance.CgformEnhanceJavaListInter;
import org.jeecg.modules.online.cgform.entity.OnlCgformField;
import org.jeecg.modules.online.cgform.service.IOnlCgformFieldService;
import org.jeecg.modules.online.config.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("cgformEnhanceExportDemo")
public class CgformEnhanceExportDemo implements CgformEnhanceJavaListInter {
    @Autowired
    ISysBaseAPI sysBaseAPI;
    @Autowired
    IOnlCgformFieldService onlCgformFieldService;

    public void execute(String tableName, List<Map<String,Object>> data) throws BusinessException {
        List<SysCategoryModel> var3 = this.sysBaseAPI.queryAllDSysCategory();
        Iterator<Map<String,Object>> var4 = data.iterator();

        while(var4.hasNext()) {
            Map<String,Object> var5 = var4.next();
            String var6 = oConvertUtils.getString(var5.get("fen_tree"));
            if (!oConvertUtils.isEmpty(var6)) {
                List<SysCategoryModel> var7 = var3.stream().filter((var1) -> {
                    return var1.getId().equals(var6);
                }).collect(Collectors.toList());
                if (var7 != null && var7.size() != 0) {
                    var5.put("fen_tree", ((SysCategoryModel)var7.get(0)).getName());
                }

                String var8 = oConvertUtils.getString(var5.get("sel_search"));
                if (!oConvertUtils.isEmpty(var8)) {
                    OnlCgformField var9 = this.onlCgformFieldService.queryFormFieldByTableNameAndField(tableName, "sel_search");
                    if (var9 != null && !oConvertUtils.isEmpty(var9.getDictTable())) {
                    	List<String> var10 = this.sysBaseAPI.queryTableDictByKeys(var9.getDictTable(), var9.getDictText(), var9.getDictField(), new String[]{var8});
                        if (var10 != null && var10.size() > 0) {
                            var5.put("sel_search", var10.get(0));
                        }
                    }
                }
            }
        }
    }
}
