package org.jeecg.modules.online.cgreport.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.online.cgreport.entity.DiagramConfiguration;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 图表配置
 */
public interface DiagramConfigurationMapper extends BaseMapper<DiagramConfiguration> {

	List selectBySql(@Param("sql") String sql);

}
