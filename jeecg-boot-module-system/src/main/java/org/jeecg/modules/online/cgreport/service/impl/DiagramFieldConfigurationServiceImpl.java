package org.jeecg.modules.online.cgreport.service.impl;

import org.jeecg.modules.online.cgreport.entity.DiagramFieldConfiguration;
import org.jeecg.modules.online.cgreport.mapper.DiagramFieldConfigurationMapper;
import org.jeecg.modules.online.cgreport.service.IDiagramFieldConfigurationService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class DiagramFieldConfigurationServiceImpl
		extends ServiceImpl<DiagramFieldConfigurationMapper, DiagramFieldConfiguration>
		implements IDiagramFieldConfigurationService {

}
