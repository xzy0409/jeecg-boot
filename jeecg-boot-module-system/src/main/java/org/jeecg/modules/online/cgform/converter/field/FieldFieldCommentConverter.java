package org.jeecg.modules.online.cgform.converter.field;

import java.util.List;
import java.util.Map;
import org.jeecg.common.system.vo.DictModel;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.online.cgform.converter.FieldCommentConverter;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FieldFieldCommentConverter implements FieldCommentConverter {
    protected String filed;
    protected List<DictModel> dictList;

    @Override
    public String converterToVal(String txt) {
        if (oConvertUtils.isNotEmpty(txt)) {

            for (DictModel dictModel : this.dictList) {
                if (dictModel.getText().equals(txt)) {
                    return dictModel.getValue();
                }
            }
        }

        return null;
    }

    @Override
    public String converterToTxt(String val) {
        if (oConvertUtils.isNotEmpty(val)) {

            for (DictModel dictModel : this.dictList) {
                if (dictModel.getValue().equals(val)) {
                    return dictModel.getText();
                }
            }
        }

        return null;
    }

    @Override
    public Map<String, String> getConfig() {
        return null;
    }
}
