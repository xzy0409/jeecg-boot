package org.jeecg.modules.online.cgform.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.online.cgform.entity.OnlCgformEnhanceJava;

public interface IOnlCgformEnhanceJavaService
    extends IService<OnlCgformEnhanceJava>
{
}
