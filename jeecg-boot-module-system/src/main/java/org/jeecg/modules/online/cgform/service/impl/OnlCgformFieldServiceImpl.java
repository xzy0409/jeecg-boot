package org.jeecg.modules.online.cgform.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.apache.shiro.SecurityUtils;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.online.cgform.common.CommonEntity;
import org.jeecg.modules.online.cgform.entity.OnlCgformField;
import org.jeecg.modules.online.cgform.entity.OnlCgformHead;
import org.jeecg.modules.online.cgform.mapper.OnlCgformFieldMapper;
import org.jeecg.modules.online.cgform.mapper.OnlCgformHeadMapper;
import org.jeecg.modules.online.cgform.model.TreeModel;
import org.jeecg.modules.online.cgform.service.IOnlCgformFieldService;
import org.jeecg.modules.online.cgform.util.SqlSymbolUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
public class OnlCgformFieldServiceImpl extends ServiceImpl<OnlCgformFieldMapper, OnlCgformField> implements IOnlCgformFieldService {
	@Resource
    private OnlCgformFieldMapper onlCgformFieldMapper;

    @Resource
    private OnlCgformHeadMapper cgformHeadMapper;

    public Map<String, Object> queryAutolistPage(String tableName, String headId, Map<String, Object> params, List<String> needList) {
        HashMap<String, Object> result = new HashMap<>();
        LambdaQueryWrapper<OnlCgformField> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(OnlCgformField::getCgformHeadId, headId);
        queryWrapper.orderByAsc(OnlCgformField::getOrderNum);
        List<OnlCgformField> onlCgformFields = this.list(queryWrapper);
        List<OnlCgformField> onlCgformFields1 = this.queryAvailableFields(tableName, true, onlCgformFields, needList);
        StringBuffer sbf = new StringBuffer();
        SqlSymbolUtil.getSelect(tableName, onlCgformFields1, sbf);
        String sql = SqlSymbolUtil.getByDataType(onlCgformFields, params, needList);
        String sql1 = SqlSymbolUtil.getByParams(params);
        sbf.append(" where 1=1  " + sql + sql1);
        Object column = params.get("column");
        if (column != null) {
            String columnStr = column.toString();
            String orderStr = params.get("order").toString();
            if (this.a(columnStr, onlCgformFields)) {
                sbf.append(" ORDER BY " + oConvertUtils.camelToUnderline(columnStr));
                if ("asc".equals(orderStr)) {
                    sbf.append(" asc");
                } else {
                    sbf.append(" desc");
                }
            }
        }

        Integer pageSzie = params.get("pageSize") == null ? 10 : Integer.parseInt(params.get("pageSize").toString());
        if (pageSzie == -521) {
            List<Map<String, Object>> onlCgformFieldList = this.onlCgformFieldMapper.queryListBySql(sbf.toString());
            log.info("---Online查询sql 不分页 :>>" + sbf.toString());
            if (onlCgformFieldList != null && onlCgformFieldList.size() != 0) {
                result.put("total", onlCgformFieldList.size());
                result.put("fieldList", onlCgformFields1);
                result.put("records", SqlSymbolUtil.transforRecords(onlCgformFieldList));
            } else {
                result.put("total", 0);
                result.put("fieldList", onlCgformFields1);
            }
        } else {
            Integer pageNo = params.get("pageNo") == null ? 1 : Integer.parseInt(params.get("pageNo").toString());
            Page<Map<String, Object>> page = new Page<>(pageNo, pageSzie);
            log.info("---Online查询sql:>>" + sbf.toString());
            IPage<Map<String, Object>> ipage = this.onlCgformFieldMapper.selectPageBySql(page, sbf.toString());
            result.put("total", ipage.getTotal());
            result.put("records", SqlSymbolUtil.transforRecords(ipage.getRecords()));
        }

        return result;
    }

    public Map<String, Object> queryAutoTreeNoPage(String tbname, String headId, Map<String, Object> params, List<String> needList, String pidField) {
        Map<String, Object> result = new HashMap<>();
        LambdaQueryWrapper<OnlCgformField> qw = new LambdaQueryWrapper<>();
        qw.eq(OnlCgformField::getCgformHeadId, headId);
        qw.orderByAsc(OnlCgformField::getOrderNum);
        List<OnlCgformField> fields = this.list(qw);
        List<OnlCgformField> fieldList = this.queryAvailableFields(tbname, true, fields, needList);
        StringBuffer sql = new StringBuffer();
        SqlSymbolUtil.getSelect(tbname, fieldList, sql);
        String dataTypes = SqlSymbolUtil.getByDataType(fields, params, needList);
        String paramsSql = SqlSymbolUtil.getByParams(params);
        sql.append(" where 1=1  " + dataTypes + paramsSql);
        Object column = params.get("column");
        if (column != null) {
            String var14 = column.toString();
            String order = params.get("order").toString();
            if (this.a(var14, fields)) {
            	sql.append(" ORDER BY " + oConvertUtils.camelToUnderline(var14));
                if ("asc".equals(order)) {
                	sql.append(" asc");
                } else {
                	sql.append(" desc");
                }
            }
        }

        Integer pageSize = params.get("pageSize") == null ? 10 : Integer.parseInt(params.get("pageSize").toString());
        if (pageSize == -521) {
        	List<Map<String, Object>> records = this.onlCgformFieldMapper.queryListBySql(sql.toString());
            if ("true".equals(params.get("hasQuery"))) {
                List<Map<String, Object>> var16 = new ArrayList<>();
                Iterator<Map<String, Object>> iterator = records.iterator();

                while(true) {
                    while(iterator.hasNext()) {
                    	Map<String, Object> record = iterator.next();
                        String pid = record.get(pidField).toString();
                        if (pid != null && !"0".equals(pid)) {
                        	Map<String, Object> var20 = this.getParentRecord(pid, tbname, headId, needList, pidField);
                            if (var20 != null && var20.size() > 0 && !var16.contains(var20)) {
                                var16.add(var20);
                            }
                        } else if (!var16.contains(record)) {
                            var16.add(record);
                        }
                    }

                    records = var16;
                    break;
                }
            }

            log.info("---Online查询sql 不分页 :>>" + sql.toString());
            if (records != null && records.size() != 0) {
            	result.put("total", records.size());
            	result.put("fieldList", fieldList);
            	result.put("records", SqlSymbolUtil.transforRecords(records));
            } else {
            	result.put("total", 0);
            	result.put("fieldList", fieldList);
            }
        } else {
            Integer pageNo = params.get("pageNo") == null ? 1 : Integer.parseInt(params.get("pageNo").toString());
            Page<Map<String, Object>> page = new Page<Map<String, Object>>(pageNo, pageSize);
            log.info("---Online查询sql:>>" + sql.toString());
            IPage<Map<String, Object>> iPage = this.onlCgformFieldMapper.selectPageBySql(page, sql.toString());
            result.put("total", iPage.getTotal());
            result.put("records", SqlSymbolUtil.transforRecords(iPage.getRecords()));
        }

        return result;
    }

    private Map<String, Object> getParentRecord(String pid, String tbname, String headId, List<String> needList, String pidField) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("id", pid);
        LambdaQueryWrapper<OnlCgformField> qw = new LambdaQueryWrapper<OnlCgformField>();
        qw.eq(OnlCgformField::getCgformHeadId, headId);
        qw.orderByAsc(OnlCgformField::getOrderNum);
        List<OnlCgformField> onlCgformFields = this.list(qw);
        List<OnlCgformField> availableFields = this.queryAvailableFields(tbname, true, onlCgformFields, needList);
        StringBuffer sql = new StringBuffer();
        SqlSymbolUtil.getSelect(tbname, availableFields, sql);
        String whereAppend = SqlSymbolUtil.getByDataType(onlCgformFields, params, needList);
        sql.append(" where 1=1  " + whereAppend + "and id='" + pid + "'");
        List<Map<String, Object>> list = this.onlCgformFieldMapper.queryListBySql(sql.toString());
        if (list != null && list.size() > 0) {
            Map<String, Object> parent = list.get(0);
            return parent != null && parent.get(pidField) != null && !"0".equals(parent.get(pidField)) ? this.getParentRecord(parent.get(pidField).toString(), tbname, headId, needList, pidField) : parent;
        } else {
            return null;
        }
    }

    public void saveFormData(String code, String tbname, JSONObject json, boolean isCrazy) {
        LambdaQueryWrapper<OnlCgformField> qw = new LambdaQueryWrapper<OnlCgformField>();
        qw.eq(OnlCgformField::getCgformHeadId, code);
        List<OnlCgformField> fields = this.list(qw);
        if (isCrazy) {
            ((OnlCgformFieldMapper)this.baseMapper).executeInsertSQL(SqlSymbolUtil.getCrazyInsertSql(tbname, fields, json));
        } else {
            ((OnlCgformFieldMapper)this.baseMapper).executeInsertSQL(SqlSymbolUtil.getInsertSql(tbname, fields, json));
        }

    }

    public void saveTreeFormData(String code, String tbname, JSONObject json, String hasChildField, String pidField) {
        LambdaQueryWrapper<OnlCgformField> qw = new LambdaQueryWrapper<OnlCgformField>();
        qw.eq(OnlCgformField::getCgformHeadId, code);
        List<OnlCgformField> fields = this.list(qw);
        Iterator<OnlCgformField> iterator = fields.iterator();

        while(true) {
            while(iterator.hasNext()) {
                OnlCgformField field = iterator.next();
                if (hasChildField.equals(field.getDbFieldName()) && field.getIsShowForm() != 1) {
                	field.setIsShowForm(1);
                    json.put(hasChildField, "0");
                } else if (pidField.equals(field.getDbFieldName()) && oConvertUtils.isEmpty(json.get(pidField))) {
                	field.setIsShowForm(1);
                    json.put(pidField, "0");
                }
            }

            Map<String, Object> var10 = SqlSymbolUtil.getInsertSql(tbname, fields, json);
            ((OnlCgformFieldMapper)this.baseMapper).executeInsertSQL(var10);
            if (!"0".equals(json.getString(pidField))) {
                ((OnlCgformFieldMapper)this.baseMapper).editFormData("update " + tbname + " set " + hasChildField + " = '1' where id = '" + json.getString(pidField) + "'");
            }

            return;
        }
    }

    public void saveFormData(List<OnlCgformField> fieldList, String tbname, JSONObject json) {
        Map<String, Object> var4 = SqlSymbolUtil.getInsertSql(tbname, fieldList, json);
        ((OnlCgformFieldMapper)this.baseMapper).executeInsertSQL(var4);
    }

    public void editFormData(String code, String tbname, JSONObject json, boolean isCrazy) {
        LambdaQueryWrapper<OnlCgformField> qw = new LambdaQueryWrapper<>();
        qw.eq(OnlCgformField::getCgformHeadId, code);
        List<OnlCgformField> fields = this.list(qw);
        if (isCrazy) {
            this.baseMapper.executeUpdatetSQL(SqlSymbolUtil.getCrazyUpdateSql(tbname, fields, json));
        } else {
            this.baseMapper.executeUpdatetSQL(SqlSymbolUtil.getUpdateSql(tbname, fields, json));
        }
    }
    
    public void editFormBpmStatus(String code, String tbname, String bpmStatus){
    	Map<String, Object> hashMap = new HashMap<>();
    	String sql = "update " + SqlSymbolUtil.getSubstring(tbname) + " set bpm_status = '"+ bpmStatus +"' where 1=1  " + " AND " + "id" + "=" + "'" + code + "'";
        log.info("--动态表单编辑sql-->" + sql);
        hashMap.put("execute_sql_string", sql);
    	this.baseMapper.executeUpdatetSQL(hashMap);
    }
    
	@Override
	public void editFormData(String formDataId, String tableName, Map<String, Object> data) {
		StringBuffer sql = new StringBuffer("update " + SqlSymbolUtil.getSubstring(tableName) + " set ");
		for(Entry<String, Object> entry : data.entrySet()){
			if(entry.getValue() instanceof String){
				sql = sql.append(entry.getKey()+" = '" + entry.getValue().toString() + "',");
			}else if(entry.getValue() instanceof Number) {
				sql = sql.append(entry.getKey()+" = " + entry.getValue().toString() + ",");
			}
		}
		sql.setLength(sql.length()-1);
		sql.append(" where 1=1  " + " AND " + "id" + "=" + "'" + formDataId + "'");
		Map<String, Object> hashMap = new HashMap<>();
		log.info("--动态表单编辑sql-->" + sql.toString());
        hashMap.put("execute_sql_string", sql.toString());
    	this.baseMapper.executeUpdatetSQL(hashMap);
	}

    public void editTreeFormData(String code, String tbname, JSONObject json, String hasChildField, String pidField) {
        String var6 = SqlSymbolUtil.getSubstring(tbname);
        String var7 = "select * from " + var6 + " where id = '" + json.getString("id") + "'";
        Map<String, Object> var8 = ((OnlCgformFieldMapper)this.baseMapper).queryFormData(var7);
        Map<String, Object> var9 = SqlSymbolUtil.getValueType(var8);
        String var10 = var9.get(pidField).toString();
        LambdaQueryWrapper<OnlCgformField> var11 = new LambdaQueryWrapper<OnlCgformField>();
        var11.eq(OnlCgformField::getCgformHeadId, code);
        List<OnlCgformField> var12 = this.list(var11);
        Iterator<OnlCgformField> var13 = var12.iterator();

        while(var13.hasNext()) {
            OnlCgformField var14 = var13.next();
            if (pidField.equals(var14.getDbFieldName()) && oConvertUtils.isEmpty(json.get(pidField))) {
                var14.setIsShowForm(1);
                json.put(pidField, "0");
            }
        }

        Map<String, Object> var16 = SqlSymbolUtil.getUpdateSql(tbname, var12, json);
        ((OnlCgformFieldMapper)this.baseMapper).executeUpdatetSQL(var16);
        if (!var10.equals(json.getString(pidField))) {
            if (!"0".equals(var10)) {
                String var17 = "select count(*) from " + var6 + " where " + pidField + " = '" + var10 + "'";
                Integer var15 = ((OnlCgformFieldMapper)this.baseMapper).queryCountBySql(var17);
                if (var15 == null || var15 == 0) {
                    ((OnlCgformFieldMapper)this.baseMapper).editFormData("update " + var6 + " set " + hasChildField + " = '0' where id = '" + var10 + "'");
                }
            }

            if (!"0".equals(json.getString(pidField))) {
                ((OnlCgformFieldMapper)this.baseMapper).editFormData("update " + var6 + " set " + hasChildField + " = '1' where id = '" + json.getString(pidField) + "'");
            }
        }

    }

    public Map<String, Object> queryFormData(String code, String tbname, String id) {
        LambdaQueryWrapper<OnlCgformField> var4 = new LambdaQueryWrapper<OnlCgformField>();
        var4.eq(OnlCgformField::getCgformHeadId, code);
        var4.eq(OnlCgformField::getIsShowForm, 1);
        List<OnlCgformField> var5 = this.list(var4);
        String var6 = SqlSymbolUtil.getSelectSql(tbname, var5, id);
        return this.onlCgformFieldMapper.queryFormData(var6);
    }

    @Transactional(
            rollbackFor = {Exception.class}
    )
    public void deleteAutoListMainAndSub(OnlCgformHead head, String ids) {
        if (head.getTableType() == 2) {
            String headId = head.getId();
            String tbname = head.getTableName();
            String var5 = "tableName";
            String var6 = "linkField";
            String var7 = "linkValueStr";
            String var8 = "mainField";
            ArrayList var9 = new ArrayList();
            if (oConvertUtils.isNotEmpty(head.getSubTableStr())) {
                String[] var10 = head.getSubTableStr().split(",");
                int var11 = var10.length;

                for(int var12 = 0; var12 < var11; ++var12) {
                    String var13 = var10[var12];
                    OnlCgformHead var14 = (OnlCgformHead)this.cgformHeadMapper.selectOne((new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, var13));
                    if (var14 != null) {
                        LambdaQueryWrapper<OnlCgformField> var15 = (LambdaQueryWrapper<OnlCgformField>)((LambdaQueryWrapper<OnlCgformField>)(new LambdaQueryWrapper<OnlCgformField>()).eq(OnlCgformField::getCgformHeadId, var14.getId())).eq(OnlCgformField::getMainTable, head.getTableName());
                        List<OnlCgformField> var16 = this.list(var15);
                        if (var16 != null && var16.size() != 0) {
                            OnlCgformField var17 = (OnlCgformField)var16.get(0);
                            Map var18 = new HashMap();
                            var18.put(var6, var17.getDbFieldName());
                            var18.put(var8, var17.getMainField());
                            var18.put(var5, var13);
                            var18.put(var7, "");
                            var9.add(var18);
                        }
                    }
                }

                LambdaQueryWrapper<OnlCgformField> var24 = new LambdaQueryWrapper<OnlCgformField>();
                var24.eq(OnlCgformField::getCgformHeadId, headId);
                List<OnlCgformField> var25 = this.list(var24);
                String[] var26 = ids.split(",");
                String[] var27 = var26;
                int var29 = var26.length;
                int var31 = 0;

                label52:
                while(true) {
                    if (var31 >= var29) {
                        Iterator var28 = var9.iterator();

                        while(true) {
                            if (!var28.hasNext()) {
                                break label52;
                            }

                            Map var30 = (Map)var28.next();
                            this.deleteAutoList((String)var30.get(var5), (String)var30.get(var6), (String)var30.get(var7));
                        }
                    }

                    String var32 = var27[var31];
                    String var33 = SqlSymbolUtil.getSelectSql(tbname, var25, var32);
                    Map<String, Object> var34 = this.onlCgformFieldMapper.queryFormData(var33);
                    new ArrayList();
                    Iterator var20 = var9.iterator();

                    while(var20.hasNext()) {
                        Map var21 = (Map)var20.next();
                        Object var22 = var34.get(((String)var21.get(var8)).toLowerCase());
                        if (var22 == null) {
                            var22 = var34.get(((String)var21.get(var8)).toUpperCase());
                        }

                        if (var22 != null) {
                            String var23 = (String)var21.get(var7) + String.valueOf(var22) + ",";
                            var21.put(var7, var23);
                        }
                    }

                    ++var31;
                }
            }

            this.deleteAutoListById(head.getTableName(), ids);
        }

    }

    public void deleteAutoListById(String tbname, String ids) {
        this.deleteAutoList(tbname, "id", ids);
    }

    public void deleteAutoList(String tbname, String linkField, String linkValue) {
        if (linkValue != null && !"".equals(linkValue)) {
            String[] var4 = linkValue.split(",");
            StringBuffer var5 = new StringBuffer();
            String[] var6 = var4;
            int var7 = var4.length;

            for(int var8 = 0; var8 < var7; ++var8) {
                String var9 = var6[var8];
                if (var9 != null && !"".equals(var9)) {
                    var5.append("'" + var9 + "',");
                }
            }

            String var10 = var5.toString();
            String var11 = "DELETE FROM " + SqlSymbolUtil.getSubstring(tbname) + " where " + linkField + " in(" + var10.substring(0, var10.length() - 1) + ")";
            log.info("--删除sql-->" + var11);
            this.onlCgformFieldMapper.deleteAutoList(var11);
        }

    }

    public List<Map<String, String>> getAutoListQueryInfo(String code) {
        LambdaQueryWrapper<OnlCgformField> qw = new LambdaQueryWrapper<>();
        qw.eq(OnlCgformField::getCgformHeadId, code);
        qw.eq(OnlCgformField::getIsQuery, 1);
        qw.orderByAsc(OnlCgformField::getOrderNum);
        List<OnlCgformField> fields = this.list(qw);
        List<Map<String, String>> columns = new ArrayList<>();
        int i = 0;

        Map<String, String> column;
        for(Iterator<OnlCgformField> iterator = fields.iterator(); iterator.hasNext(); columns.add(column)) {
            OnlCgformField field = iterator.next();
            column = new HashMap<String, String>();
            column.put("label", field.getDbFieldTxt());
            column.put("field", field.getDbFieldName());
            column.put("mode", field.getQueryMode());
            String[] dictTexts;
            String dictTable;
            if ("1".equals(field.getQueryConfigFlag())) {
            	column.put("config", "1");
            	column.put("view", field.getQueryShowType());
            	column.put("defValue", field.getQueryDefVal());
                if ("cat_tree".equals(field.getFieldShowType())) {
                	column.put("pcode", field.getQueryDictField());
                } else if ("sel_tree".equals(field.getFieldShowType())) {
                	dictTexts = field.getQueryDictText().split(",");
                    dictTable = field.getQueryDictTable() + "," + dictTexts[2] + "," + dictTexts[0];
                    column.put("dict", dictTable);
                    column.put("pidField", dictTexts[1]);
                    column.put("hasChildField", dictTexts[3]);
                    column.put("pidValue", field.getQueryDictField());
                } else {
                	column.put("dictTable", field.getQueryDictTable());
                	column.put("dictCode", field.getQueryDictField());
                	column.put("dictText", field.getQueryDictText());
                }
            } else {
            	column.put("view", field.getFieldShowType());
                if ("cat_tree".equals(field.getFieldShowType())) {
                	column.put("pcode", field.getDictField());
                } else if ("sel_tree".equals(field.getFieldShowType())) {
                	dictTexts = field.getDictText().split(",");
                	dictTable = field.getDictTable() + "," + dictTexts[2] + "," + dictTexts[0];
                    column.put("dict", dictTable);
                    column.put("pidField", dictTexts[1]);
                    column.put("hasChildField", dictTexts[3]);
                    column.put("pidValue", field.getDictField());
                } else if ("popup".equals(field.getFieldShowType())) {
                	column.put("dictTable", field.getDictTable());
                	column.put("dictCode", field.getDictField());
                	column.put("dictText", field.getDictText());
                }

                column.put("mode", field.getQueryMode());
            }

            ++i;
            if (i > 2) {
            	column.put("hidden", "1");
            }
        }

        return columns;
    }

    public List<OnlCgformField> queryFormFields(String code, boolean isform) {
        LambdaQueryWrapper<OnlCgformField> qw = new LambdaQueryWrapper<>();
        qw.eq(OnlCgformField::getCgformHeadId, code);
        if (isform) {
        	qw.eq(OnlCgformField::getIsShowForm, 1);
        }

        return this.list(qw);
    }

    public List<OnlCgformField> queryFormFieldsByTableName(String tableName) {
        OnlCgformHead head = this.cgformHeadMapper.selectOne(new LambdaQueryWrapper<OnlCgformHead>().eq(OnlCgformHead::getTableName, tableName));
        if (head != null) {
            LambdaQueryWrapper<OnlCgformField> qw = new LambdaQueryWrapper<>();
            qw.eq(OnlCgformField::getCgformHeadId, head.getId());
            return this.list(qw);
        } else {
            return null;
        }
    }

    public OnlCgformField queryFormFieldByTableNameAndField(String tableName, String fieldName) {
        OnlCgformHead head = this.cgformHeadMapper.selectOne(new LambdaQueryWrapper<OnlCgformHead>().eq(OnlCgformHead::getTableName, tableName));
        if (head != null) {
            LambdaQueryWrapper<OnlCgformField> qw = new LambdaQueryWrapper<>();
            qw.eq(OnlCgformField::getCgformHeadId, head.getId());
            qw.eq(OnlCgformField::getDbFieldName, fieldName);
            if (this.list(qw) != null && this.list(qw).size() > 0) {
                return this.list(qw).get(0);
            }
        }

        return null;
    }

    public Map<String, Object> queryFormData(List<OnlCgformField> fieldList, String tbname, String id) {
        String sql = SqlSymbolUtil.getSelectSql(tbname, fieldList, id);
        return this.onlCgformFieldMapper.queryFormData(sql);
    }
    
    public Map<String, Object> queryBpmData(String tableId, String tbname, String id){
    	LambdaQueryWrapper<OnlCgformField> qw = new LambdaQueryWrapper<>();
    	qw.eq(OnlCgformField::getCgformHeadId, tableId);
    	List<OnlCgformField> field = this.list(qw);
    	String sql = SqlSymbolUtil.getSelectSql(tbname, field, id);
    	return this.onlCgformFieldMapper.queryFormData(sql);
    }

    public List<Map<String, Object>> querySubFormData(List<OnlCgformField> fieldList, String tbname, String linkField, String value) {
        String sql = SqlSymbolUtil.getSelectSql(tbname, fieldList, linkField, value);
        return this.onlCgformFieldMapper.queryListData(sql);
    }

    public IPage<Map<String, Object>> selectPageBySql(Page<Map<String, Object>> page, String sql) {
        return ((OnlCgformFieldMapper)this.baseMapper).selectPageBySql(page, sql);
    }

    public List<String> selectOnlineHideColumns(String tbname) {
        String var2 = "online:" + tbname + ":%";
        LoginUser var3 = (LoginUser)SecurityUtils.getSubject().getPrincipal();
        String var4 = var3.getId();
        List<String> var5 = ((OnlCgformFieldMapper)this.baseMapper).selectOnlineHideColumns(var4, var2);
        return this.a(var5);
    }

    public List<OnlCgformField> queryAvailableFields(String cgFormId, String tbname, String taskId, boolean isList) {
        LambdaQueryWrapper<OnlCgformField> qw = new LambdaQueryWrapper<OnlCgformField>();
        qw.eq(OnlCgformField::getCgformHeadId, cgFormId);
        if (isList) {
        	qw.eq(OnlCgformField::getIsShowList, 1);
        } else {
        	qw.eq(OnlCgformField::getIsShowForm, 1);
        }

        qw.orderByAsc(OnlCgformField::getOrderNum);
        List<OnlCgformField> fields = this.list(qw);
        String var7 = "online:" + tbname + "%";
        LoginUser loginUser = (LoginUser)SecurityUtils.getSubject().getPrincipal();
        String userId = loginUser.getId();
        List<String> var10 = new ArrayList<>();
        List<String> var11;
        if (oConvertUtils.isEmpty(taskId)) {
            var11 = ((OnlCgformFieldMapper)this.baseMapper).selectOnlineHideColumns(userId, var7);
            if (var11 != null && var11.size() != 0 && var11.get(0) != null) {
                var10.addAll(var11);
            }
        } else if (oConvertUtils.isNotEmpty(taskId)) {
            var11 = ((OnlCgformFieldMapper)this.baseMapper).selectFlowAuthColumns(tbname, taskId, "1");
            if (var11 != null && var11.size() > 0 && var11.get(0) != null) {
                var10.addAll(var11);
            }
        }

        if (var10.size() == 0) {
            return fields;
        } else {
            List<OnlCgformField> var14 = new ArrayList<>();

            for(int var12 = 0; var12 < fields.size(); ++var12) {
                OnlCgformField var13 = fields.get(var12);
                if (this.b(var13.getDbFieldName(), var10)) {
                    var14.add(var13);
                }
            }

            return var14;
        }
    }

    public List<String> queryDisabledFields(String tbname) {
        String var2 = "online:" + tbname + "%";
        LoginUser var3 = (LoginUser)SecurityUtils.getSubject().getPrincipal();
        String var4 = var3.getId();
        List<String> var5 = ((OnlCgformFieldMapper)this.baseMapper).selectOnlineDisabledColumns(var4, var2);
        return this.a(var5);
    }

    public List<String> queryDisabledFields(String tbname, String taskId) {
        if (oConvertUtils.isEmpty(taskId)) {
            return null;
        } else {
            List<String> var3 = ((OnlCgformFieldMapper)this.baseMapper).selectFlowAuthColumns(tbname, taskId, "2");
            return this.a(var3);
        }
    }

    private List<String> a(List<String> list) {
        List<String> var2 = new ArrayList<String>();
        if (list != null && list.size() != 0 && list.get(0) != null) {
            Iterator<String> var3 = list.iterator();

            while(var3.hasNext()) {
                String var4 = (String)var3.next();
                if (!oConvertUtils.isEmpty(var4)) {
                    String var5 = var4.substring(var4.lastIndexOf(":") + 1);
                    if (!oConvertUtils.isEmpty(var5)) {
                        var2.add(var5);
                    }
                }
            }

            return var2;
        } else {
            return var2;
        }
    }

    public List<OnlCgformField> queryAvailableFields(String tbname, boolean isList, List list, List needList) {
        List<OnlCgformField> var5 = new ArrayList<>();
        String var6 = "online:" + tbname + "%";
        LoginUser var7 = (LoginUser)SecurityUtils.getSubject().getPrincipal();
        String var8 = var7.getId();
        List<String> var9 = ((OnlCgformFieldMapper)this.baseMapper).selectOnlineHideColumns(var8, var6);
        boolean var10 = true;
        if (var9 == null || var9.size() == 0 || var9.get(0) == null) {
            var10 = false;
        }

        Iterator var11 = list.iterator();

        while(true) {
            while(var11.hasNext()) {
                OnlCgformField var12 = (OnlCgformField)var11.next();
                String var13 = var12.getDbFieldName();
                if (needList != null && needList.contains(var13)) {
                    var12.setIsQuery(1);
                    var5.add(var12);
                } else {
                    if (isList) {
                        if (var12.getIsShowList() != 1) {
                            if (oConvertUtils.isNotEmpty(var12.getMainTable()) && oConvertUtils.isNotEmpty(var12.getMainField())) {
                                var5.add(var12);
                            }
                            continue;
                        }
                    } else if (var12.getIsShowForm() != 1) {
                        continue;
                    }

                    if (var10) {
                        if (this.b(var13, var9)) {
                            var5.add(var12);
                        }
                    } else {
                        var5.add(var12);
                    }
                }
            }

            return var5;
        }
    }

    private boolean b(String var1, List<String> var2) {
        boolean var3 = true;

        for(int var4 = 0; var4 < var2.size(); ++var4) {
            String var5 = (String)var2.get(var4);
            if (!oConvertUtils.isEmpty(var5)) {
                String var6 = var5.substring(var5.lastIndexOf(":") + 1);
                if (!oConvertUtils.isEmpty(var6) && var6.equals(var1)) {
                    var3 = false;
                }
            }
        }

        return var3;
    }

    public boolean a(String var1, List<OnlCgformField> var2) {
        boolean var3 = false;
        Iterator<OnlCgformField> var4 = var2.iterator();

        while(var4.hasNext()) {
            OnlCgformField var5 = var4.next();
            if (oConvertUtils.camelToUnderline(var1).equals(var5.getDbFieldName())) {
                var3 = true;
                break;
            }
        }

        return var3;
    }

    public void executeInsertSQL(Map<String, Object> params) {
    	this.baseMapper.executeInsertSQL(params);
    }

    public void executeUpdatetSQL(Map<String, Object> params) {
    	this.baseMapper.executeUpdatetSQL(params);
    }

    public List<TreeModel> queryDataListByLinkDown(CommonEntity linkDown) {
        return this.baseMapper.queryDataListByLinkDown(linkDown);
    }

    public void updateTreeNodeNoChild(String tableName, String filed, String id) {
        Map<String, Object> map = SqlSymbolUtil.getTreeUpdateNoChird(tableName, filed, id);
        this.baseMapper.executeUpdatetSQL(map);
    }

    public String queryTreeChildIds(OnlCgformHead head, String ids) {
        String var3 = head.getTreeParentIdField();
        String var4 = head.getTableName();
        String[] var5 = ids.split(",");
        StringBuffer var6 = new StringBuffer();
        String[] var7 = var5;
        int var8 = var5.length;

        for(int var9 = 0; var9 < var8; ++var9) {
            String var10 = var7[var9];
            if (var10 != null && !var6.toString().contains(var10)) {
                if (var6.toString().length() > 0) {
                    var6.append(",");
                }

                var6.append(var10);
                this.a(var10, var3, var4, var6);
            }
        }

        return var6.toString();
    }

    private StringBuffer a(String var1, String var2, String var3, StringBuffer var4) {
        String var5 = "select * from " + SqlSymbolUtil.getSubstring(var3) + " where " + var2 + "= '" + var1 + "'";
        List<Map<String, Object>> var6 = this.onlCgformFieldMapper.queryListBySql(var5);
        Map<String, Object> var8;
        if (var6 != null && var6.size() > 0) {
            for(Iterator<Map<String, Object>>  var7 = var6.iterator(); var7.hasNext(); this.a(var8.get("id").toString(), var2, var3, var4)) {
                var8 = var7.next();
                if (!var4.toString().contains(var8.get("id").toString())) {
                    var4.append(",").append(var8.get("id"));
                }
            }
        }

        return var4;
    }

}
