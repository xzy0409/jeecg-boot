package org.jeecg.modules.online.cgform.entity;

import java.io.Serializable;

public class OnlCgformEnhanceJs
    implements Serializable
{

    public OnlCgformEnhanceJs()
    {
    }

    public String getId()
    {
        return id;
    }

    public String getCgformHeadId()
    {
        return cgformHeadId;
    }

    public String getCgJsType()
    {
        return cgJsType;
    }

    public String getCgJs()
    {
        return cgJs;
    }

    public String getContent()
    {
        return content;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public void setCgformHeadId(String cgformHeadId)
    {
        this.cgformHeadId = cgformHeadId;
    }

    public void setCgJsType(String cgJsType)
    {
        this.cgJsType = cgJsType;
    }

    public void setCgJs(String cgJs)
    {
        this.cgJs = cgJs;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public boolean equals(Object o)
    {
        if(o == this)
            return true;
        if(!(o instanceof OnlCgformEnhanceJs))
            return false;
        OnlCgformEnhanceJs onlcgformenhancejs = (OnlCgformEnhanceJs)o;
        if(!onlcgformenhancejs.canEqual(this))
            return false;
        String s = getId();
        String s1 = onlcgformenhancejs.getId();
        if(s != null ? !s.equals(s1) : s1 != null)
            return false;
        String s2 = getCgformHeadId();
        String s3 = onlcgformenhancejs.getCgformHeadId();
        if(s2 != null ? !s2.equals(s3) : s3 != null)
            return false;
        String s4 = getCgJsType();
        String s5 = onlcgformenhancejs.getCgJsType();
        if(s4 != null ? !s4.equals(s5) : s5 != null)
            return false;
        String s6 = getCgJs();
        String s7 = onlcgformenhancejs.getCgJs();
        if(s6 != null ? !s6.equals(s7) : s7 != null)
            return false;
        String s8 = getContent();
        String s9 = onlcgformenhancejs.getContent();
        return s8 != null ? s8.equals(s9) : s9 == null;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof OnlCgformEnhanceJs;
    }

    public int hashCode()
    {
        int i = 1;
        String s = getId();
        i = i * 59 + (s != null ? s.hashCode() : 43);
        String s1 = getCgformHeadId();
        i = i * 59 + (s1 != null ? s1.hashCode() : 43);
        String s2 = getCgJsType();
        i = i * 59 + (s2 != null ? s2.hashCode() : 43);
        String s3 = getCgJs();
        i = i * 59 + (s3 != null ? s3.hashCode() : 43);
        String s4 = getContent();
        i = i * 59 + (s4 != null ? s4.hashCode() : 43);
        return i;
    }

    public String toString()
    {
        return (new StringBuilder()).append("OnlCgformEnhanceJs(id=").append(getId()).append(", cgformHeadId=").append(getCgformHeadId()).append(", cgJsType=").append(getCgJsType()).append(", cgJs=").append(getCgJs()).append(", content=").append(getContent()).append(")").toString();
    }

    private static final long serialVersionUID = 1L;
    private String id;
    private String cgformHeadId;
    private String cgJsType;
    private String cgJs;
    private String content;
}
