package org.jeecg.modules.online.cgreport.service;

import org.jeecg.modules.online.cgreport.entity.DiagramFieldConfiguration;

import com.baomidou.mybatisplus.extension.service.IService;

public interface IDiagramFieldConfigurationService extends IService<DiagramFieldConfiguration> {

}
