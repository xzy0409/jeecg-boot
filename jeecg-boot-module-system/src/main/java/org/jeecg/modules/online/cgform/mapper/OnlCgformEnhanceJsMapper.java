package org.jeecg.modules.online.cgform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.online.cgform.entity.OnlCgformEnhanceJs;

public interface OnlCgformEnhanceJsMapper
    extends BaseMapper<OnlCgformEnhanceJs>
{
}
