package org.jeecg.modules.online.cgreport.entity;

public class CommonConstants {

	/**
	 * 图表数据类型
	 */
	public static final String DATA_TYPE_JSON = "json";
	public static final String DATA_TYPE_SQL = "sql";

	/**
	 * 响应客户端信息
	 */
	public static final String MSG_EXIST = "该值不可用，系统中已存在！";
	public static final String MSG_SUCCESS_ADD = "添加成功！";
	public static final String MSG_SUCCESS_EDIT = "编辑成功！";
	public static final String MSG_SUCCESS_DELTET = "删除成功！";
	public static final String MSG_SUCCESS_DELTET_BATCH = "批量删除成功！";
	public static final String MSG_SUCCESS_PARSING = "解析成功！";

	public static final String MSG_ERROR_OPERATE = "操作失败！";
	public static final String MSG_ERROR_NO_DATA_FOUND = "未找到对应数据！";
	public static final String MSG_ERROR_PARSING = "解析失败！";

}
