package org.jeecg.modules.online.cgform.enhance.demo;

import com.alibaba.fastjson.JSONObject;

import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import org.jeecg.modules.online.cgform.enhance.CgformEnhanceJavaInter;
import org.jeecg.modules.online.config.exception.BusinessException;
import org.springframework.stereotype.Component;

@Component("cgformEnhanceBeanDemo")
@Slf4j
public class CgformEnhanceBeanDemo implements CgformEnhanceJavaInter {

    public CgformEnhanceBeanDemo() {
    }

    public int execute(String tableName, JSONObject json) throws BusinessException {
        log.info("--------我是自定义java增强测试bean-----------");
        log.info("--------当前表单 tableName=> " + tableName);
        log.info("--------当前表单 JSON数据=> " + json.toJSONString());
        if (json.containsKey("phone")) {
            json.put("phone", "18611100000");
        }

        return 1;
    }

    public int execute(String tableName, Map<String, Object> map) throws BusinessException {
        return 1;
    }
}
