package org.jeecg.modules.online.cgreport.entity;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import org.jeecg.modules.online.cgreport.entity.ChartsData;
import org.jeecg.modules.online.cgreport.entity.GroupData;

@Data
public class TempletData<T,D,H,I,E> {

	private T templet;
	
	private List<GroupData<ChartsData<D,H,I>>> groups = new ArrayList<>();

	private List<E> queryConfigList;
}
