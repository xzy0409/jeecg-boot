package org.jeecg.modules.online.cgreport.mapper;

import org.jeecg.modules.online.cgreport.entity.DiagramCombinationQueryConfig;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 查询配置
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface DiagramCombinationQueryConfigMapper extends BaseMapper<DiagramCombinationQueryConfig> {

}
