package org.jeecg.modules.online.cgreport.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.online.cgreport.entity.FormHeadConfiguration;
import org.jeecg.modules.online.cgreport.service.IFormHeadConfigurationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 表单配置
 */
@Api(tags = "表单配置")
@RestController
@RequestMapping("/form/formConfiguration")
public class FormHeadConfigurationController {

    @Autowired
    private IFormHeadConfigurationService iFormHeadConfigurationService;

    @AutoLog(value = "表单配置-列表查询")
    @ApiOperation(value = "表单配置-列表查询", notes = "表单配置-列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryList(@RequestParam("tableTypes") String tableTypes, HttpServletRequest req) {
        List<FormHeadConfiguration> list = new ArrayList<>();
        FormHeadConfiguration formHeadConfiguration = new FormHeadConfiguration();
        formHeadConfiguration.setCopyType(0);

        String[] tps = tableTypes.split(",");

        for (String tp : tps) {
            formHeadConfiguration.setTableType(Integer.parseInt(tp));

            QueryWrapper<FormHeadConfiguration> queryWrapper = QueryGenerator.initQueryWrapper(formHeadConfiguration, req.getParameterMap());
            list.addAll(iFormHeadConfigurationService.list(queryWrapper));
        }

        return Result.OK(list);
    }

    @AutoLog(value = "表单配置-列表查询")
    @ApiOperation(value = "表单配置-列表查询", notes = "表单配置-列表查询")
    @GetMapping(value = "/sublist")
    public Result<?> querySubList(@RequestParam("subTableStr") String subTableStr, HttpServletRequest req) {
        Map<String, Object> params = new HashMap<>();
        params.put("table_name", subTableStr);
        List<FormHeadConfiguration> list = iFormHeadConfigurationService.listByMap(params);
        params.put("table_name", list.get(0).getSubTableStr());
        list = iFormHeadConfigurationService.listByMap(params);
        return Result.OK(list);
    }
}
