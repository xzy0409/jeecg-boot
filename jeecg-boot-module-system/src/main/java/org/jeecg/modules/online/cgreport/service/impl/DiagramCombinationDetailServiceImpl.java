package org.jeecg.modules.online.cgreport.service.impl;

import org.jeecg.modules.online.cgreport.entity.DiagramCombinationDetail;
import org.jeecg.modules.online.cgreport.mapper.DiagramCombinationDetailMapper;
import org.jeecg.modules.online.cgreport.service.IDiagramCombinationDetailService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class DiagramCombinationDetailServiceImpl
		extends ServiceImpl<DiagramCombinationDetailMapper, DiagramCombinationDetail>
		implements IDiagramCombinationDetailService {

}
