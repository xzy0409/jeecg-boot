package org.jeecg.modules.online.cgreport.entity;

import java.util.List;

import lombok.Data;

@Data
public class ChartsData<D, H, I> {

	private Object data;

	private String dictOptions;// {}

	private H head;
	
	private Object param;

	private List<I> items;

}
