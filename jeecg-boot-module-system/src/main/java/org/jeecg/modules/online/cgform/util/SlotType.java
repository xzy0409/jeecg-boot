package org.jeecg.modules.online.cgform.util;


public class SlotType
{
    public static final String FILE_SLOT = "fileSlot";
    public static final String IMG_SLOT = "imgSlot";
    public static final String HTML_SLOT = "htmlSlot";
    public static final String DATE_SLOT = "dateSlot";
    public static final String FIELD_SLOT = "fieldHref_";
    public static final String PCA_SLOT = "pcaSlot";
}
