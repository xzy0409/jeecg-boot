package org.jeecg.modules.online.cgreport.service.impl;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * TODO
 *
 * @author dousw
 * @version 1.0
 */
@Slf4j
@Service("cgReportExcelService")
public class CgReportExcelService {

    public HSSFWorkbook exportExcel(String title, Collection<?> titleSet, Collection<?> dataSet) {
        HSSFWorkbook var4 = null;

        try {
            if (titleSet == null || titleSet.size() == 0) {
                throw new Exception("读取表头失败！");
            }

            if (title == null) {
                title = "";
            }

            var4 = new HSSFWorkbook();
            HSSFSheet var5 = var4.createSheet(title);
            int var6 = 0;
            int var7 = 0;
            Row var8 = var5.createRow(var6);
            var8.setHeight((short)450);
            HSSFCellStyle var9 = this.getTitleStyle(var4);
            List var10 = (List)titleSet;
            Iterator var11 = dataSet.iterator();

            Map var13;
            for(Iterator var12 = var10.iterator(); var12.hasNext(); ++var7) {
                var13 = (Map)var12.next();
                String var14 = (String)var13.get("field_txt");
                Cell var15 = var8.createCell(var7);
                HSSFRichTextString var16 = new HSSFRichTextString(var14);
                var15.setCellValue(var16);
                var15.setCellStyle(var9);
            }

            HSSFCellStyle var21 = this.getOneStyle(var4);

            while(var11.hasNext()) {
                var7 = 0;
                ++var6;
                var8 = var5.createRow(var6);
                var13 = (Map)var11.next();

                for(Iterator var23 = var10.iterator(); var23.hasNext(); ++var7) {
                    Map var24 = (Map)var23.next();
                    String var25 = (String)var24.get("field_name");
                    String var17 = var13.get(var25) == null ? "" : var13.get(var25).toString();
                    Cell var18 = var8.createCell(var7);
                    HSSFRichTextString var19 = new HSSFRichTextString(var17);
                    var18.setCellStyle(var21);
                    var18.setCellValue(var19);
                }
            }

            for(int var22 = 0; var22 < var10.size(); ++var22) {
                var5.autoSizeColumn(var22);
            }
        } catch (Exception var20) {
            log.error(var20.getMessage(), var20);
        }

        return var4;
    }

	/**
	 * exce表头单元格样式处理
	 * 
	 * @param workbook
	 * @return
	 */
	private HSSFCellStyle getTitleStyle(HSSFWorkbook workbook) {
		// 产生Excel表头
		HSSFCellStyle titleStyle = workbook.createCellStyle();
		titleStyle.setBorderLeft(BorderStyle.THIN); // 左边框
		titleStyle.setBorderRight(BorderStyle.THIN); // 右边框
		titleStyle.setBorderBottom(BorderStyle.THIN); // 底边框
		titleStyle.setBorderTop(BorderStyle.THIN); // 顶边框
		titleStyle.setAlignment(HorizontalAlignment.CENTER);
		titleStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index); // 填充的背景颜色
		titleStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND); // 填充图案

		return titleStyle;
	}

    private void a(int var1, int var2, HSSFWorkbook var3) {
        HSSFSheet var4 = var3.getSheetAt(0);
        HSSFCellStyle var5 = this.getOneStyle(var3);

        for(int var6 = 1; var6 <= var1; ++var6) {
            Row var7 = var4.createRow(var6);

            for(int var8 = 0; var8 < var2; ++var8) {
                var7.createCell(var8).setCellStyle(var5);
            }
        }

    }

    private HSSFCellStyle getTwoStyle(HSSFWorkbook workbook) {
		// 产生Excel表头
		HSSFCellStyle style = workbook.createCellStyle();
		style.setBorderLeft(BorderStyle.THIN); // 左边框
		style.setBorderRight(BorderStyle.THIN); // 右边框
		style.setBorderBottom(BorderStyle.THIN);
		style.setBorderTop(BorderStyle.THIN);
		style.setFillForegroundColor(IndexedColors.LIGHT_TURQUOISE.index); // 填充的背景颜色
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND); // 填充图案
		return style;
	}

    private HSSFCellStyle getOneStyle(HSSFWorkbook workbook) {
		// 产生Excel表头
		HSSFCellStyle style = workbook.createCellStyle();
		style.setBorderLeft(BorderStyle.THIN); // 左边框
		style.setBorderRight(BorderStyle.THIN); // 右边框
		style.setBorderBottom(BorderStyle.THIN);
		style.setBorderTop(BorderStyle.THIN);
		return style;
	}
}
