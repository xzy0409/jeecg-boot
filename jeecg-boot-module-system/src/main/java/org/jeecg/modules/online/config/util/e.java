package org.jeecg.modules.online.config.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class e {
    protected static Map<String, String> a = new HashMap<>();

    public e() {
    }

    private static String a(String var0, int var1) {
        String var2 = var0;
        Iterator<String> iterator = a.keySet().iterator();

        while(iterator.hasNext()) {
            String key = iterator.next();
            String value = a.get(key);
            if (var1 == 1) {
                var2 = var0.replaceAll(key, value);
            } else if (var1 == 2) {
                var2 = var0.replaceAll(value, key);
            }
        }

        return var2;
    }

    static {
        a.put("class", "clazz");
    }
}
