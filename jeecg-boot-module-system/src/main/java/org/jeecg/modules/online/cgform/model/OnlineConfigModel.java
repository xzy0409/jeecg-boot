package org.jeecg.modules.online.cgform.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Data;
import org.jeecg.common.system.vo.DictModel;
import org.jeecg.modules.online.cgform.entity.OnlCgformButton;

@Data
public class OnlineConfigModel {
    private String code;
    private String formTemplate;
    private String description;
    private String currentTableName;
    private Integer tableType;
    private String paginationFlag;
    private String checkboxFlag;
    private Integer scrollFlag;
    private List<OnlColumn> columns;
    private List<String> hideColumns;
    private Map<String, List<DictModel>> dictOptions = new HashMap<>();
    private List<OnlCgformButton> cgButtonList;
    private List<HrefSlots> fieldHrefSlots;
    private String enhanceJs;
    private List<OnlForeignKey> foreignKeys;
    private String pidField;
    private String hasChildrenField;
    private String textField;
}
