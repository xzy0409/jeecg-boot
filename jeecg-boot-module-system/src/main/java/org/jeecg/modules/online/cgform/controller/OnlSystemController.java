package org.jeecg.modules.online.cgform.controller;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.online.cgform.entity.OnlSystem;
import org.jeecg.modules.online.cgform.service.IOnlSystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

 /**
 * 子系统
 * @author: MxpIO
 * @version: V1.0
 */
@Api(tags="子系统")
@RestController
@RequestMapping("/system/onlSystem")
public class OnlSystemController extends JeecgController<OnlSystem, IOnlSystemService> {
	@Autowired
	private IOnlSystemService onlSystemService;
	
	/**
	 * 分页列表查询
	 *
	 * @param onlSystem
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "子系统-分页列表查询")
	@ApiOperation(value="子系统-分页列表查询", notes="子系统-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(OnlSystem onlSystem,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<OnlSystem> queryWrapper = QueryGenerator.initQueryWrapper(onlSystem, req.getParameterMap());
		Page<OnlSystem> page = new Page<OnlSystem>(pageNo, pageSize);
		IPage<OnlSystem> pageList = onlSystemService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param onlSystem
	 * @return
	 */
	@AutoLog(value = "子系统-添加")
	@ApiOperation(value="子系统-添加", notes="子系统-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody OnlSystem onlSystem) {
		onlSystemService.save(onlSystem);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param onlSystem
	 * @return
	 */
	@AutoLog(value = "子系统-编辑")
	@ApiOperation(value="子系统-编辑", notes="子系统-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody OnlSystem onlSystem) {
		onlSystemService.updateById(onlSystem);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "子系统-通过id删除")
	@ApiOperation(value="子系统-通过id删除", notes="子系统-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		onlSystemService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "子系统-批量删除")
	@ApiOperation(value="子系统-批量删除", notes="子系统-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.onlSystemService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "子系统-通过id查询")
	@ApiOperation(value="子系统-通过id查询", notes="子系统-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		OnlSystem onlSystem = onlSystemService.getById(id);
		if(onlSystem==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(onlSystem);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param onlSystem
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, OnlSystem onlSystem) {
        return super.exportXls(request, onlSystem, OnlSystem.class, "子系统");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, OnlSystem.class);
    }

}
