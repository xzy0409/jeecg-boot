package org.jeecg.modules.online.cgreport.entity;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.system.base.entity.JeecgEntity;

import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 查询配置
 * @author: jeecg-boot
 * @version: V1.0
 */
@Data
@TableName("cs_diagram_comb_query_config")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="cs_diagram_comb_query_config组合报表查询配置", description="组合报表查询配置")
public class DiagramCombinationQueryConfig extends JeecgEntity {
	
	private static final long serialVersionUID = 6866692573738306236L;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**字段名*/
	@Excel(name = "字段名", width = 15)
    @ApiModelProperty(value = "字段名")
    private java.lang.String field;
	/**字段文本*/
	@Excel(name = "字段文本", width = 15)
    @ApiModelProperty(value = "字段文本")
    private java.lang.String label;
	/**控件类型*/
	@Excel(name = "控件类型", width = 15)
    @ApiModelProperty(value = "控件类型")
    private java.lang.String viewType;
	/**查询类型*/
	@Excel(name = "查询类型", width = 15)
    @ApiModelProperty(value = "查询类型")
    private java.lang.String mode;
	/**字典Table*/
	@Excel(name = "字典Table", width = 15)
    @ApiModelProperty(value = "字典Table")
    private java.lang.String dictTable;
	/**字典Code*/
	@Excel(name = "字典Code", width = 15)
    @ApiModelProperty(value = "字典Code")
    private java.lang.String dictCode;
	/**字典Text*/
	@Excel(name = "字典Text", width = 15)
    @ApiModelProperty(value = "字典Text")
    private java.lang.String dictText;
	/**默认值*/
	@Excel(name = "默认值", width = 15)
    @ApiModelProperty(value = "默认值")
    private java.lang.String defValue;
	/**是否启用*/
	@Excel(name = "是否启用", width = 15)
    @ApiModelProperty(value = "是否启用")
    private java.lang.String config;
	/**组合报表ID*/
	@Excel(name = "组合报表ID", width = 15)
    @ApiModelProperty(value = "组合报表ID")
    private java.lang.String graphreportTempletId;

    /**排序*/
    @Excel(name = "排序", width = 15)
    @ApiModelProperty(value = "排序")
    private int orderNum;
}
