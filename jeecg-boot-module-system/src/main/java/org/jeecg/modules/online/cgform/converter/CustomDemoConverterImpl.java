package org.jeecg.modules.online.cgform.converter;

import java.util.Map;
import org.springframework.stereotype.Component;

@Component("customDemoConverter")
public class CustomDemoConverterImpl implements FieldCommentConverter {


    @Override
    public String converterToVal(String txt) {
        return "管理员1".equals(txt) ? "admin" : txt;
    }

    @Override
    public String converterToTxt(String val) {
        if (val != null) {
            if ("admin".equals(val)) {
                return "管理员1";
            }

            if ("scott".equals(val)) {
                return "管理员2";
            }
        }

        return val;
    }

    @Override
    public Map<String, String> getConfig() {
        return null;
    }
}
