package org.jeecg.modules.online.cgreport.service;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.util.Collection;

public interface ICgReportExcelServiceImpl {
    HSSFWorkbook exportExcel(String var1, Collection<?> var2, Collection<?> var3);
}
