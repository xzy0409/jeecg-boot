package org.jeecg.modules.online.config.util;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

import org.jeecg.common.constant.DataBaseConstant;
import org.jeecg.common.util.SpringContextUtils;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.online.config.exception.DBException;
import org.jeecg.modules.online.config.service.DbTableHandleI;
import org.jeecg.modules.online.config.service.impl.MysqlTableHandle;
import org.jeecg.modules.online.config.service.impl.OracleTableHandle;
import org.jeecg.modules.online.config.service.impl.PgTableHandle;
import org.jeecg.modules.online.config.service.impl.SqlServerTableHandle;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TableUtil {
    public static String DATABASE_TYPE = "";

    public TableUtil() {
    }

    public static DbTableHandleI getTableHandle() throws SQLException, DBException {
    	DbTableHandleI dbTableHandle = null;
        String databaseType = getDatabaseType();
        
        switch(databaseType) {
            case DataBaseConstant.DB_TYPE_ORACLE:
            	dbTableHandle = new OracleTableHandle();
                break;
            case DataBaseConstant.DB_TYPE_POSTGRESQL:
            	dbTableHandle = new PgTableHandle();
                break;
            case DataBaseConstant.DB_TYPE_MYSQL:
            	dbTableHandle = new MysqlTableHandle();
                break;
            case DataBaseConstant.DB_TYPE_SQLSERVER:
            	dbTableHandle = new SqlServerTableHandle();
        }

        return dbTableHandle;
    }

    public static Connection getConnection() throws SQLException {
        DataSource ds = SpringContextUtils.getApplicationContext().getBean(DataSource.class);
        return ds.getConnection();
    }

    public static String getDatabaseType() throws SQLException, DBException {
        if (oConvertUtils.isNotEmpty(DATABASE_TYPE)) {
            return DATABASE_TYPE;
        } else {
            DataSource ds = SpringContextUtils.getApplicationContext().getBean(DataSource.class);
            return getDatabaseType(ds);
        }
    }

    public static boolean isOracle() {
        try {
            return DataBaseConstant.DB_TYPE_ORACLE.equals(getDatabaseType());
        } catch (SQLException | DBException e) {
            e.printStackTrace();
        }

        return false;
    }

    public static String getDatabaseType(DataSource ds) throws SQLException, DBException {
        if ("".equals(DATABASE_TYPE)) {
            Connection conn = ds.getConnection();

            try {
                DatabaseMetaData dbMetaData = conn.getMetaData();
                String dbProductName = dbMetaData.getDatabaseProductName().toLowerCase();
                if (dbProductName.indexOf("mysql") >= 0) {
                	DATABASE_TYPE = DataBaseConstant.DB_TYPE_MYSQL;
                } else if (dbProductName.indexOf("oracle") >= 0) {
                	DATABASE_TYPE = DataBaseConstant.DB_TYPE_ORACLE;
                } else if (dbProductName.indexOf("sqlserver") < 0 && dbProductName.indexOf("sql server") < 0) {
                    if (dbProductName.indexOf("postgresql") < 0) {
                        throw new DBException("数据库类型:[" + dbProductName + "]不识别!");
                    }

                    DATABASE_TYPE = DataBaseConstant.DB_TYPE_POSTGRESQL;
                } else {
                	DATABASE_TYPE = DataBaseConstant.DB_TYPE_SQLSERVER;
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            } finally {
            	conn.close();
            }
        }

        return DATABASE_TYPE;
    }

    public static String getDatabaseType(Connection conn) throws SQLException, DBException {
        if ("".equals(DATABASE_TYPE)) {
            DatabaseMetaData var1 = conn.getMetaData();
            String var2 = var1.getDatabaseProductName().toLowerCase();
            if (var2.indexOf("mysql") >= 0) {
            	DATABASE_TYPE = DataBaseConstant.DB_TYPE_MYSQL;
            } else if (var2.indexOf("oracle") >= 0) {
            	DATABASE_TYPE = DataBaseConstant.DB_TYPE_ORACLE;
            } else if (var2.indexOf("sqlserver") < 0 && var2.indexOf("sql server") < 0) {
                if (var2.indexOf("postgresql") < 0) {
                    throw new DBException("数据库类型:[" + var2 + "]不识别!");
                }

                DATABASE_TYPE = DataBaseConstant.DB_TYPE_POSTGRESQL;
            } else {
            	DATABASE_TYPE = DataBaseConstant.DB_TYPE_SQLSERVER;
            }
        }

        return DATABASE_TYPE;
    }

    /*public static String getDatabaseType(Session session) throws SQLException, DBException {
        return getDatabaseType();
    }*/

    public static String fixTableName(String tableName, String databaseType) {
        switch(databaseType) {
            case DataBaseConstant.DB_TYPE_ORACLE:
            	return tableName.toUpperCase();
            case DataBaseConstant.DB_TYPE_POSTGRESQL:
            	return tableName.toLowerCase();
            default:
            	return tableName;
        }
    }

    public static Boolean isTableExist(String tableName) {
        Connection conn = null;
        ResultSet rs = null;

        Boolean isTableExist;
        try {
            String[] var3 = new String[]{"TABLE"};
            conn = getConnection();
            DatabaseMetaData databaseMetaData = conn.getMetaData();
            String databaseProductName = databaseMetaData.getDatabaseProductName().toUpperCase();
            String dbTableName = fixTableName(tableName, databaseProductName);
            rs = databaseMetaData.getTables(null, null, dbTableName, var3);
            if (rs.next()) {
            	log.info("数据库表：【" + tableName + "】已存在");
            	isTableExist = true;
                return isTableExist;
            }

            isTableExist = false;
        } catch (SQLException e) {
            throw new RuntimeException();
        } finally {
            try {
                if (rs != null) {
                	rs.close();
                }

                if (conn != null) {
                	conn.close();
                }
            } catch (SQLException e) {
            	log.error(e.getMessage(), e);
            }

        }

        return isTableExist;
    }

    public static Map<String, Object> a(List<Map<String, Object>> var0) {
        Map<String, Object> result = new HashMap<>();

        for(int var2 = 0; var2 < var0.size(); ++var2) {
        	result.put(var0.get(var2).get("column_name").toString(), var0.get(var2));
        }

        return result;
    }

    public static String getDialect() throws SQLException, DBException {
        String databaseType = getDatabaseType();
        return getJdbcDriver(databaseType);
    }

    public static String getJdbcDriver(String databaseType) throws SQLException, DBException {
        String driver = "org.hibernate.dialect.MySQL5InnoDBDialect";
        switch(databaseType) {
            case DataBaseConstant.DB_TYPE_ORACLE:
            	driver = "org.hibernate.dialect.OracleDialect";
                break;
            case DataBaseConstant.DB_TYPE_POSTGRESQL:
            	driver = "org.hibernate.dialect.PostgreSQLDialect";
                break;
            case DataBaseConstant.DB_TYPE_SQLSERVER:
            	driver = "org.hibernate.dialect.SQLServerDialect";
        }
        return driver;
    }

    public static String c(String var0) {
        return var0;
    }
}
