package org.jeecg.modules.system.util;

import java.math.BigDecimal;

public class OnlineUtil {

	public static Object calculation(Object o1, Object o2, String op){
		Object result = null;
		if(op == null){
			op = "";
		}
		switch (op) {
			case "count":
				result = new BigDecimal(o1.toString()).add(BigDecimal.ONE);
				break;
			case "sum":
				BigDecimal b1 = new BigDecimal(o1.toString());
				BigDecimal b2 = new BigDecimal(o2.toString());
				result = b1.add(b2);
				break;
			case "avg":
				BigDecimal ba1 = new BigDecimal(o1.toString());
				BigDecimal ba2 = new BigDecimal(o2.toString());
				result = ba1.add(ba2);
				break;
			default:
				result = o2;
				break;
		}
		return result;
	}
}
