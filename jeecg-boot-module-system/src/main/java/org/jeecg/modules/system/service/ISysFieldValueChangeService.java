package org.jeecg.modules.system.service;

import org.jeecg.modules.system.entity.SysFieldValueChange;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 记录字段值的变化
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface ISysFieldValueChangeService extends IService<SysFieldValueChange> {

}
