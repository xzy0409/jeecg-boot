package org.jeecg.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.system.entity.SysCheckRule;

/**
 * 编码校验规则
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface SysCheckRuleMapper extends BaseMapper<SysCheckRule> {

}
