package org.jeecg.modules.system.mapper;

import org.jeecg.modules.system.entity.SysParameter;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 系统参数
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface SysParameterMapper extends BaseMapper<SysParameter> {

}
