package org.jeecg.modules.system.mapper;

import org.jeecg.modules.system.entity.SysDepartRolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 部门角色权限
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface SysDepartRolePermissionMapper extends BaseMapper<SysDepartRolePermission> {

}
