package org.jeecg.modules.system.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;

@Data
@TableName("sys_serial_number")
public class SerialNumber {
	
	@TableId
	private String snExpression;
	
	private String currentRecord;

}
