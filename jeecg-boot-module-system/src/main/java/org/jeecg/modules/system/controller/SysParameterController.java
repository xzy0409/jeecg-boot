package org.jeecg.modules.system.controller;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.system.entity.SysParameter;
import org.jeecg.modules.system.service.ISysParameterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

 /**
 * 系统参数
 * @author: jeecg-boot
 * @version: V1.0
 */
@Api(tags="系统参数")
@RestController
@RequestMapping("/a/sysParameter")
public class SysParameterController extends JeecgController<SysParameter, ISysParameterService> {
	@Autowired
	private ISysParameterService sysParameterService;
	
	/**
	 * 分页列表查询
	 *
	 * @param sysParameter
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "系统参数-分页列表查询")
	@ApiOperation(value="系统参数-分页列表查询", notes="系统参数-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(SysParameter sysParameter,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<SysParameter> queryWrapper = QueryGenerator.initQueryWrapper(sysParameter, req.getParameterMap());
		Page<SysParameter> page = new Page<SysParameter>(pageNo, pageSize);
		IPage<SysParameter> pageList = sysParameterService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param sysParameter
	 * @return
	 */
	@AutoLog(value = "系统参数-添加")
	@ApiOperation(value="系统参数-添加", notes="系统参数-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody SysParameter sysParameter) {
		sysParameterService.save(sysParameter);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param sysParameter
	 * @return
	 */
	@AutoLog(value = "系统参数-编辑")
	@ApiOperation(value="系统参数-编辑", notes="系统参数-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody SysParameter sysParameter) {
		sysParameterService.updateById(sysParameter);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "系统参数-通过id删除")
	@ApiOperation(value="系统参数-通过id删除", notes="系统参数-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		sysParameterService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "系统参数-批量删除")
	@ApiOperation(value="系统参数-批量删除", notes="系统参数-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.sysParameterService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "系统参数-通过id查询")
	@ApiOperation(value="系统参数-通过id查询", notes="系统参数-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		SysParameter sysParameter = sysParameterService.getById(id);
		if(sysParameter==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(sysParameter);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param sysParameter
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, SysParameter sysParameter) {
        return super.exportXls(request, sysParameter, SysParameter.class, "系统参数");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, SysParameter.class);
    }

}
