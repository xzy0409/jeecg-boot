package org.jeecg.modules.system.mapper;

import org.jeecg.modules.system.entity.SerialNumber;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface SerialNumberMapper extends BaseMapper<SerialNumber> {

}
