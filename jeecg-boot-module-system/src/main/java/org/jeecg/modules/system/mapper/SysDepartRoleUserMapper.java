package org.jeecg.modules.system.mapper;

import org.jeecg.modules.system.entity.SysDepartRoleUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 部门角色人员信息
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface SysDepartRoleUserMapper extends BaseMapper<SysDepartRoleUser> {

}
