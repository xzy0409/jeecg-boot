package org.jeecg.modules.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.system.entity.SysFillRule;

/**
 * 填值规则
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface ISysFillRuleService extends IService<SysFillRule> {

}
