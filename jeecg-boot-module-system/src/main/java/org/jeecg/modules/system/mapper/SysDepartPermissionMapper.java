package org.jeecg.modules.system.mapper;

import org.jeecg.modules.system.entity.SysDepartPermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 部门权限表
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface SysDepartPermissionMapper extends BaseMapper<SysDepartPermission> {

}
