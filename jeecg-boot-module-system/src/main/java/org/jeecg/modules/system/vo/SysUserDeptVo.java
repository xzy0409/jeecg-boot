package org.jeecg.modules.system.vo;

import lombok.Data;

@Data
public class SysUserDeptVo {
	
	private String username;
	
	private String departName;
	
	private String realname;
	
}
