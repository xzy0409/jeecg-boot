package org.jeecg.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.system.entity.SysFillRule;

/**
 * 填值规则
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface SysFillRuleMapper extends BaseMapper<SysFillRule> {

}
