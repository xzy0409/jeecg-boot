package org.jeecg.modules.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.system.entity.SysPosition;

/**
 * 职务表
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface ISysPositionService extends IService<SysPosition> {

}
