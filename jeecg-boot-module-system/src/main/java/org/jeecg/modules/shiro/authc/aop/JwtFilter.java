package org.jeecg.modules.shiro.authc.aop;

import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.web.filter.authc.BasicHttpAuthenticationFilter;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.system.util.JwtUtil;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.SpringContextUtils;
import org.jeecg.modules.shiro.authc.JwtToken;
import org.jeecg.modules.shiro.vo.DefContants;
import org.jeecg.modules.system.entity.SysUser;
import org.jeecg.modules.system.provider.IAuthUserProvider;
import org.jeecg.modules.system.service.ISysUserService;
import org.jeecg.modules.system.util.TenantContext;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

import lombok.extern.slf4j.Slf4j;

/**
 * 鉴权登录拦截器
 * @author: Scott
 **/
@Slf4j
public class JwtFilter extends BasicHttpAuthenticationFilter {
	
	//private AuthUserProvider authUserProvider;

	/**
	 * 执行登录认证
	 *
	 * @param request
	 * @param response
	 * @param mappedValue
	 * @return
	 */
	@Override
	protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
		try {
			executeLogin(request, response);
			return true;
		} catch (Exception e) {
			throw new AuthenticationException("Token失效，请重新登录", e);
		}
	}

	/**
	 *
	 */
	@Override
	protected boolean executeLogin(ServletRequest request, ServletResponse response) {
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		String token = httpServletRequest.getHeader(DefContants.X_ACCESS_TOKEN);
		
		//设计器放行
		if(token == null && ((HttpServletRequest) request).getRequestURL().toString().endsWith("kFormDesign/index")){
			token = request.getParameter("token");
		}
		String loginType = httpServletRequest.getHeader(DefContants.LOGIN_TYPE);
		log.info(((HttpServletRequest) request).getRequestURL().toString());
		String code = request.getParameter("code");
		log.info(code);
		log.info(token);
		if(code != null && (token == null || "".equals(token) || "null".equals(token))){
			Map<String, IAuthUserProvider> providers = SpringContextUtils.getBeansOfType(IAuthUserProvider.class);
			RedisUtil redisUtil = SpringContextUtils.getBean(RedisUtil.class);
			
				
			ISysUserService sysUserService = SpringContextUtils.getBean(ISysUserService.class);
			loginType = "1";
			token = String.valueOf(redisUtil.get(CommonConstant.PREFIX_THIRD_USER_TOKEN + code));
			log.info(token);
			if(token == null || "".equals(token) || "null".equals(token)){
				for(Entry<String, IAuthUserProvider> entry : providers.entrySet()){
					log.info(token);
					String userId;
					try {
						userId = entry.getValue().getUserIdByCode(code);
					} catch (Exception e) {
						e.printStackTrace();
						continue;
					}
					log.info(userId);
					LambdaQueryWrapper<SysUser> queryWrapper = new LambdaQueryWrapper<>();
					queryWrapper.eq(SysUser::getThirdId,userId);
					
					SysUser sysUser = sysUserService.getOne(queryWrapper);
					token = JwtUtil.sign(sysUser.getUsername(), sysUser.getPassword());
					// 设置token缓存有效时间
					redisUtil.set(CommonConstant.PREFIX_USER_TOKEN + token, token);
					redisUtil.set(CommonConstant.PREFIX_THIRD_USER_TOKEN + code, token);
					redisUtil.expire(CommonConstant.PREFIX_USER_TOKEN + token, JwtUtil.EXPIRE_TIME*2 / 1000);
					redisUtil.expire(CommonConstant.PREFIX_THIRD_USER_TOKEN + code, JwtUtil.EXPIRE_TIME*2 / 1000);
					log.info(token);
					if(token == null){
						continue;
					}else{
						break;
					}
				}
			}

			
			
			/*authUserProvider = SpringContextUtils.getBean(AuthUserProvider.class);
			RedisUtil redisUtil = SpringContextUtils.getBean(RedisUtil.class);
			ISysUserService sysUserService = SpringContextUtils.getBean(ISysUserService.class);
			loginType = "1";
			token = String.valueOf(redisUtil.get(CommonConstant.PREFIX_THIRD_USER_TOKEN + code));
			log.info(token);
			if(token == null || "".equals(token) || "null".equals(token)){
				log.info(token);
				String userId = authUserProvider.getUserIdByCode(code);
				log.info(userId);
				LambdaQueryWrapper<SysUser> queryWrapper = new LambdaQueryWrapper<>();
				queryWrapper.eq(SysUser::getThirdId,userId);
				
				SysUser sysUser = sysUserService.getOne(queryWrapper);
				token = JwtUtil.sign(sysUser.getUsername(), sysUser.getPassword());
				// 设置token缓存有效时间
				redisUtil.set(CommonConstant.PREFIX_USER_TOKEN + token, token);
				redisUtil.set(CommonConstant.PREFIX_THIRD_USER_TOKEN + code, token);
				redisUtil.expire(CommonConstant.PREFIX_USER_TOKEN + token, JwtUtil.EXPIRE_TIME*2 / 1000);
				redisUtil.expire(CommonConstant.PREFIX_THIRD_USER_TOKEN + code, JwtUtil.EXPIRE_TIME*2 / 1000);
				request.setAttribute("token", token);
				log.info(token);
			}*/
		}
		request.setAttribute("token", token);
		log.info(token);
		JwtToken jwtToken = new JwtToken(token, loginType);
		// 提交给realm进行登入，如果错误他会抛出异常并被捕获
		getSubject(request, response).login(jwtToken);
		// 如果没有抛出异常则代表登入成功，返回true
		return true;
	}

	/**
	 * 对跨域提供支持
	 */
	@Override
	protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		HttpServletResponse httpServletResponse = (HttpServletResponse) response;
		httpServletResponse.setHeader("Access-control-Allow-Origin", httpServletRequest.getHeader("Origin"));
		httpServletResponse.setHeader("Access-Control-Allow-Methods", "GET,POST,OPTIONS,PUT,DELETE");
		httpServletResponse.setHeader("Access-Control-Allow-Headers", httpServletRequest.getHeader("Access-Control-Request-Headers"));
		// 跨域时会首先发送一个option请求，这里我们给option请求直接返回正常状态
		if (httpServletRequest.getMethod().equals(RequestMethod.OPTIONS.name())) {
			httpServletResponse.setStatus(HttpStatus.OK.value());
			return false;
		}
		//update-begin-author:taoyan date:20200708 for:多租户用到
		String tenant_id = httpServletRequest.getHeader(DefContants.TENANT_ID);
		TenantContext.setTenant(tenant_id);
		//update-end-author:taoyan date:20200708 for:多租户用到
		return super.preHandle(request, response);
	}
}
