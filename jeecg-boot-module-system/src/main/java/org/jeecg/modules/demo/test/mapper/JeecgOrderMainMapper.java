package org.jeecg.modules.demo.test.mapper;

import org.jeecg.modules.demo.test.entity.JeecgOrderMain;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 订单
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface JeecgOrderMainMapper extends BaseMapper<JeecgOrderMain> {

}
