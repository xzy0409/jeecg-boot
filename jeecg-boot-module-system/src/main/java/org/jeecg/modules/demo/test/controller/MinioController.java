package org.jeecg.modules.demo.test.controller;

import io.minio.GetObjectArgs;
import io.minio.GetPresignedObjectUrlArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

/**
 * Minio文件服务测试
 * @author scott
 */
@RestController
@RequestMapping("/test/minio")
public class MinioController {

    //minio服务的IP端口
    private static String url = "http://111.225.222.176:9000";
    private static String accessKey = "admin";
    private static String secretKey = "jeecg1357";
    private static String bucketName = "jeecgtest";

    /**
     * 上传文件到minio服务
     *
     * @param file
     * @return
     */
    @PostMapping("upload")
    public String upload(@RequestParam("file") MultipartFile file) {
        try {
            MinioClient minioClient = //new MinioClient(url, accessKey, secretKey);
                    getMinioClient();
            InputStream is = file.getInputStream(); //得到文件流
            String fileName = "/upload/img/" + file.getOriginalFilename(); //文件名
         // String contentType = file.getContentType();  //类型
           // minioClient.putObject(bucketName, fileName, is, contentType); //把文件放置Minio桶(文件夹)
            PutObjectArgs objectArgs = getPutObjectArgs( is, fileName );
            minioClient.putObject( objectArgs );

            return "上传成功";
        } catch (Exception e) {
            return "上传失败";
        }
    }

    private PutObjectArgs getPutObjectArgs(InputStream is, String fileName) throws IOException {
        return PutObjectArgs.builder().object(fileName)
                        .bucket(bucketName)
                        .contentType("application/octet-stream")
                        .stream(is,is.available(),-1).build();
    }

    private PutObjectArgs getPutObjectArgs( String fileName,String bucketName) throws IOException {
        return PutObjectArgs.builder().object(fileName)
                .bucket(bucketName)
                .contentType("application/octet-stream").build();
    }

    @NotNull
    private MinioClient getMinioClient() {
        return MinioClient.builder()
                .endpoint(url)
                .credentials(accessKey, secretKey)
                .build();
    }

    /**
     * 下载minio服务的文件
     *
     * @param response
     * @return
     */
    @GetMapping("download")
    public String download(HttpServletResponse response) {
        try {
            MinioClient minioClient = getMinioClient();//new MinioClient(url, accessKey, secretKey);


            GetObjectArgs objectArgs = GetObjectArgs.builder().object("11.jpg")
                    .bucket(bucketName).build();
            InputStream fileInputStream = minioClient.getObject(objectArgs);
            response.setHeader("Content-Disposition", "attachment;filename=" + "11.jpg");
            response.setContentType("application/force-download");
            response.setCharacterEncoding("UTF-8");
            IOUtils.copy(fileInputStream, response.getOutputStream());
            return "下载完成";
        } catch (Exception e) {
            e.printStackTrace();
            return "下载失败";
        }
    }

    /**
     * 获取minio文件的下载地址
     *
     * @return
     */
    @GetMapping("url")
    public String getUrl() {
        try {
            MinioClient minioClient = getMinioClient();
            GetPresignedObjectUrlArgs objectArgs = GetPresignedObjectUrlArgs.builder().object("11.jpg")
                    .bucket(bucketName)
                    .expiry(10000).build();
            String url = minioClient.getPresignedObjectUrl(objectArgs);
            return url;
        } catch (Exception e) {
            e.printStackTrace();
            return "获取失败";
        }
    }
}
