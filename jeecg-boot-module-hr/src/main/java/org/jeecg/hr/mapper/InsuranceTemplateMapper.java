package org.jeecg.hr.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.hr.entity.InsuranceTemplate;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface InsuranceTemplateMapper extends BaseMapper<InsuranceTemplate> {

}
