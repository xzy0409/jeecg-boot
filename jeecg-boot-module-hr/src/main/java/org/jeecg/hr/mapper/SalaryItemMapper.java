package org.jeecg.hr.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.hr.entity.SalaryItem;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface SalaryItemMapper extends BaseMapper<SalaryItem> {

}
