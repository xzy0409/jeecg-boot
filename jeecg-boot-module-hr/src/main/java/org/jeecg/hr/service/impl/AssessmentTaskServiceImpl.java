package org.jeecg.hr.service.impl;

import org.jeecg.hr.entity.AssessmentTask;
import org.jeecg.hr.mapper.AssessmentTaskMapper;
import org.jeecg.hr.mapper.AssessmentTaskResultMapper;
import org.jeecg.hr.service.AssessmentTaskService;
import org.jeecg.modules.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class AssessmentTaskServiceImpl extends ServiceImpl<AssessmentTaskMapper, AssessmentTask> implements AssessmentTaskService {
	
	@Autowired
	private AssessmentTaskResultMapper assessmentTaskResultMapper;
	
	@Autowired
	private ISysUserService sysUserService;

	@Override
	public void handleDetails(IPage<AssessmentTask> pageList) {
		for(AssessmentTask task : pageList.getRecords()){
			task.setResults(assessmentTaskResultMapper.selectByTaskCode(task.getTaskCode()));
			task.setUser(sysUserService.getUserByName(task.getUsername()));
			task.setLeaderUser(sysUserService.getUserByName(task.getLeader()));
		}
	}


}
