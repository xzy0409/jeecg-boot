package org.jeecg.hr.service;

import java.io.Serializable;
import java.util.Collection;

import org.jeecg.hr.entity.InsuranceTemplate;

import com.baomidou.mybatisplus.extension.service.IService;

public interface InsuranceTemplateService extends IService<InsuranceTemplate> {

	/**
	 * 删除一对多
	 */
	public void delInsuranceTemplate (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);

	/**
	 * 新增一对多
	 * @param insuranceTemplate
	 */
	public void saveInsuranceTemplateWithDetail(InsuranceTemplate insuranceTemplate);

	/**
	 * 修改一对多
	 * @param insuranceTemplate
	 */
	public void updateInsuranceTemplateWithDetail(InsuranceTemplate insuranceTemplate);


}
