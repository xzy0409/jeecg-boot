package org.jeecg.hr.service;

import java.util.List;

import org.jeecg.hr.entity.AssessmentProc;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author: MxpIO
 */
public interface AssessmentProcService extends IService<AssessmentProc> {

	public List<AssessmentProc> selectByPlanCode(String planCode);
}
