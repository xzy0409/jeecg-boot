package org.jeecg.hr.service;

import org.jeecg.hr.entity.AssessmentInstance;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

public interface AssessmentInstanceService extends IService<AssessmentInstance> {

	void handleDetails(IPage<AssessmentInstance> pageList);

	AssessmentInstance startInstance(String planCode) throws Exception;

}
