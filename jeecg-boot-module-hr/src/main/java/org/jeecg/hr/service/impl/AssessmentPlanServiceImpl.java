package org.jeecg.hr.service.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.jeecg.hr.entity.AssessmentIndex;
import org.jeecg.hr.entity.AssessmentPlan;
import org.jeecg.hr.entity.AssessmentProc;
import org.jeecg.hr.entity.AssessmentRule;
import org.jeecg.hr.entity.AssessmentUser;
import org.jeecg.hr.mapper.AssessmentIndexMapper;
import org.jeecg.hr.mapper.AssessmentPlanMapper;
import org.jeecg.hr.mapper.AssessmentProcMapper;
import org.jeecg.hr.mapper.AssessmentRuleMapper;
import org.jeecg.hr.mapper.AssessmentUserMapper;
import org.jeecg.hr.service.AssessmentPlanService;
import org.jeecg.modules.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class AssessmentPlanServiceImpl extends ServiceImpl<AssessmentPlanMapper, AssessmentPlan> implements AssessmentPlanService {

	@Autowired
	private AssessmentPlanMapper assessmentPlanMapper;
	
	@Autowired
	private AssessmentIndexMapper assessmentIndexMapper;
	
	@Autowired
	private AssessmentProcMapper assessmentProcMapper;

	@Autowired
	private AssessmentUserMapper assessmentUserMapper;
	
	@Autowired
	private AssessmentRuleMapper assessmentRuleMapper;
	
	@Autowired
	private ISysUserService sysUserService;
	
	@Override
	@Transactional
	public void delAssessmentPlan(String id) {
		AssessmentPlan assessmentPlan = assessmentPlanMapper.selectById(id);
		assessmentIndexMapper.deleteByPlanCode(assessmentPlan.getPlanCode());
		assessmentProcMapper.deleteByPlanCode(assessmentPlan.getPlanCode());
		assessmentUserMapper.deleteByPlanCode(assessmentPlan.getPlanCode());
		assessmentRuleMapper.deleteByPlanCode(assessmentPlan.getPlanCode());
		assessmentPlanMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			AssessmentPlan assessmentPlan = assessmentPlanMapper.selectById(id);
			assessmentIndexMapper.deleteByPlanCode(assessmentPlan.getPlanCode());
			assessmentProcMapper.deleteByPlanCode(assessmentPlan.getPlanCode());
			assessmentUserMapper.deleteByPlanCode(assessmentPlan.getPlanCode());
			assessmentRuleMapper.deleteByPlanCode(assessmentPlan.getPlanCode());
			assessmentPlanMapper.deleteById(id);
		}
	}
	
	@Override
	@Transactional
	public void saveAssessmentPlanWithDetail(AssessmentPlan assessmentPlan) {
		assessmentPlanMapper.insert(assessmentPlan);
		if(assessmentPlan.getIndexes() != null && assessmentPlan.getIndexes().size() > 0) {
			for(AssessmentIndex entity:assessmentPlan.getIndexes()) {
				entity.setPlanCode(assessmentPlan.getPlanCode());
				assessmentIndexMapper.insert(entity);
			}
		}
		
		if(assessmentPlan.getProcs() != null && assessmentPlan.getProcs().size() > 0) {
			for(AssessmentProc entity:assessmentPlan.getProcs()) {
				entity.setPlanCode(assessmentPlan.getPlanCode());
				assessmentProcMapper.insert(entity);
			}
		}
		
		if(assessmentPlan.getUsers() != null && assessmentPlan.getUsers().size() > 0) {
			for(AssessmentUser entity:assessmentPlan.getUsers()) {
				entity.setPlanCode(assessmentPlan.getPlanCode());
				assessmentUserMapper.insert(entity);
			}
		}
		
		if(assessmentPlan.getRules() != null && assessmentPlan.getRules().size() > 0) {
			for(AssessmentRule entity:assessmentPlan.getRules()) {
				entity.setPlanCode(assessmentPlan.getPlanCode());
				assessmentRuleMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void updateAssessmentPlanWithDetail(AssessmentPlan assessmentPlan) {
		assessmentPlanMapper.updateById(assessmentPlan);
		
		//1.先删除子表数据
		assessmentIndexMapper.deleteByPlanCode(assessmentPlan.getPlanCode());
		assessmentProcMapper.deleteByPlanCode(assessmentPlan.getPlanCode());
		assessmentUserMapper.deleteByPlanCode(assessmentPlan.getPlanCode());
		assessmentRuleMapper.deleteByPlanCode(assessmentPlan.getPlanCode());
		
		//2.子表数据重新插入
		if(assessmentPlan.getIndexes() != null && assessmentPlan.getIndexes().size() > 0) {
			for(AssessmentIndex entity : assessmentPlan.getIndexes()) {
				entity.setPlanCode(assessmentPlan.getPlanCode());
				assessmentIndexMapper.insert(entity);
			}
		}
		
		if(assessmentPlan.getProcs() != null && assessmentPlan.getProcs().size() > 0) {
			for(AssessmentProc entity : assessmentPlan.getProcs()) {
				entity.setPlanCode(assessmentPlan.getPlanCode());
				assessmentProcMapper.insert(entity);
			}
		}
		
		if(assessmentPlan.getUsers() != null && assessmentPlan.getUsers().size() > 0) {
			for(AssessmentUser entity : assessmentPlan.getUsers()) {
				entity.setPlanCode(assessmentPlan.getPlanCode());
				assessmentUserMapper.insert(entity);
			}
		}
		
		if(assessmentPlan.getRules() != null && assessmentPlan.getRules().size() > 0) {
			for(AssessmentRule entity : assessmentPlan.getRules()) {
				entity.setPlanCode(assessmentPlan.getPlanCode());
				assessmentRuleMapper.insert(entity);
			}
		}
	}

	@Override
	public void handleDetails(IPage<AssessmentPlan> pageList) {
		for(AssessmentPlan assessmentPlan : pageList.getRecords()){
			assessmentPlan.setIndexes(assessmentIndexMapper.selectByPlanCode(assessmentPlan.getPlanCode()));
			assessmentPlan.setProcs(assessmentProcMapper.selectByPlanCode(assessmentPlan.getPlanCode()));
			List<AssessmentUser> users = assessmentUserMapper.selectByPlanCode(assessmentPlan.getPlanCode());
			List<String> usernames = users.stream().map(AssessmentUser :: getUsername).collect(Collectors.toList());
			Map<String, Map<String, String>> map = sysUserService.getDepNamesByUserNames(usernames);
			users.forEach(item -> {
				item.setUserTxt(map.get("userMap").get(item.getUsername()));
				item.setDeptTxt(map.get("deptMap").get(item.getUsername()));
			});
			assessmentPlan.setUsers(users);
			assessmentPlan.setRules(assessmentRuleMapper.selectByPlanCode(assessmentPlan.getPlanCode()));
			assessmentPlan.setPlanQuantity(assessmentUserMapper.selectCount(new QueryWrapper<AssessmentUser>().eq("plan_code", assessmentPlan.getPlanCode())));
		}
	}
	
}
