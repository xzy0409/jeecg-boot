package org.jeecg.hr.service;

import org.jeecg.common.system.base.service.JeecgService;
import org.jeecg.hr.entity.OfficeDoc;

public interface OfficeDocService extends JeecgService<OfficeDoc> {

}
