package org.jeecg.hr.service;

import org.jeecg.hr.entity.AssessmentTask;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

public interface AssessmentTaskService extends IService<AssessmentTask> {

	void handleDetails(IPage<AssessmentTask> pageList);

}
