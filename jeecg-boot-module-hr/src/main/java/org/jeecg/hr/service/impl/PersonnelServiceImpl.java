package org.jeecg.hr.service.impl;

import java.io.Serializable;
import java.util.Collection;

import javax.transaction.Transactional;

import org.jeecg.hr.entity.Personnel;
import org.jeecg.hr.entity.PersonnelInsuranceItem;
import org.jeecg.hr.entity.PersonnelSalaryItem;
import org.jeecg.hr.mapper.PersonnelInsuranceItemMapper;
import org.jeecg.hr.mapper.PersonnelMapper;
import org.jeecg.hr.mapper.PersonnelSalaryItemMapper;
import org.jeecg.hr.service.PersonnelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class PersonnelServiceImpl extends ServiceImpl<PersonnelMapper, Personnel> implements PersonnelService {
	
	@Autowired
	private PersonnelMapper personnelMapper;
	
	@Autowired
	private PersonnelSalaryItemMapper personnelSalaryItemMapper;
	
	@Autowired
	private PersonnelInsuranceItemMapper personnelInsuranceItemMapper;
	
	@Override
	@Transactional
	public void delPersonnel(String id) {
		Personnel personnel = personnelMapper.selectById(id);
		personnelSalaryItemMapper.deleteByUsername(personnel.getUsername());
		personnelInsuranceItemMapper.deleteByUsername(personnel.getUsername());
		personnelMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			Personnel personnel = personnelMapper.selectById(id);
			personnelSalaryItemMapper.deleteByUsername(personnel.getUsername());
			personnelInsuranceItemMapper.deleteByUsername(personnel.getUsername());
			personnelMapper.deleteById(id);
		}
	}
	
	@Override
	@Transactional
	public void savePersonnelWithDetail(Personnel personnel) {
		personnelMapper.insert(personnel);
		if(personnel.getPersonnelInsuranceItems() != null && personnel.getPersonnelInsuranceItems().size() > 0) {
			for(PersonnelInsuranceItem entity : personnel.getPersonnelInsuranceItems()) {
				entity.setUsername(personnel.getUsername());
				personnelInsuranceItemMapper.insert(entity);
			}
		}
		
		if(personnel.getPersonnelSalaryItems() != null && personnel.getPersonnelSalaryItems().size() > 0) {
			for(PersonnelSalaryItem entity : personnel.getPersonnelSalaryItems()) {
				entity.setUsername(personnel.getUsername());
				personnelSalaryItemMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void updatePersonnelWithDetail(Personnel personnel) {
		personnelMapper.updateById(personnel);
		
		//1.先删除子表数据
		personnelSalaryItemMapper.deleteByUsername(personnel.getUsername());
		personnelInsuranceItemMapper.deleteByUsername(personnel.getUsername());
		
		//2.子表数据重新插入
		if(personnel.getPersonnelInsuranceItems() != null && personnel.getPersonnelInsuranceItems().size() > 0) {
			for(PersonnelInsuranceItem entity : personnel.getPersonnelInsuranceItems()) {
				entity.setUsername(personnel.getUsername());
				personnelInsuranceItemMapper.insert(entity);
			}
		}
		
		if(personnel.getPersonnelSalaryItems() != null && personnel.getPersonnelSalaryItems().size() > 0) {
			for(PersonnelSalaryItem entity : personnel.getPersonnelSalaryItems()) {
				entity.setUsername(personnel.getUsername());
				personnelSalaryItemMapper.insert(entity);
			}
		}
	}
}
