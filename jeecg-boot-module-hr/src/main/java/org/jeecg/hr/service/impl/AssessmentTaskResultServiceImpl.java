package org.jeecg.hr.service.impl;

import org.jeecg.hr.entity.AssessmentTaskResult;
import org.jeecg.hr.mapper.AssessmentTaskResultMapper;
import org.jeecg.hr.service.AssessmentTaskResultService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class AssessmentTaskResultServiceImpl extends ServiceImpl<AssessmentTaskResultMapper, AssessmentTaskResult> implements AssessmentTaskResultService {

}
