package org.jeecg.hr.service;

import org.jeecg.common.system.base.service.JeecgService;
import org.jeecg.hr.entity.SalaryItem;

public interface SalaryItemService extends JeecgService<SalaryItem> {

	void saveWithDetail(SalaryItem salaryItem);

	void updateByIdWithDetail(SalaryItem salaryItem);

}
