package org.jeecg.hr.service;

import java.util.List;

import org.jeecg.hr.entity.AssessmentUser;

import com.baomidou.mybatisplus.extension.service.IService;

public interface AssessmentUserService extends IService<AssessmentUser> {

	public List<AssessmentUser> selectByPlanCode(String planCode);
}
