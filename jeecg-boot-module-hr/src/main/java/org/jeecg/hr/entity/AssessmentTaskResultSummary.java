package org.jeecg.hr.entity;

import java.math.BigDecimal;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.system.base.entity.JeecgEntity;
import org.jeecg.modules.system.entity.SysUser;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("hr_assessment_task_result_summary")
public class AssessmentTaskResultSummary extends JeecgEntity {
	
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "任务编号")
	@Excel(name="任务编号",width=15)
	private String instanceCode;
	
	@ApiModelProperty(value = "用户")
	@Excel(name="用户",width=15)
	private String username;
	
	@TableField(exist = false)
	private SysUser user;
	
	@ApiModelProperty(value = "分值")
	@Excel(name="分值",width=15)
	private BigDecimal score;
	
	@ApiModelProperty(value = "最终分值")
	@Excel(name="最终分值",width=15)
	private BigDecimal finalScore;
	
	@ApiModelProperty(value = "绩效比例")
	@Excel(name="绩效比例",width=15)
	private BigDecimal assessmentScore;
	
	@ApiModelProperty(value = "绩效级别")
	@Excel(name="绩效级别",width=15)
	private String levelDesc;
	
}
