package org.jeecg.hr.entity;

import java.math.BigDecimal;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.system.base.entity.JeecgEntity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("hr_salary_template_detail")
public class SalaryTemplateDetail extends JeecgEntity {
	
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "方案编号")
	@Excel(name="方案编号",width=15)
	private String templateCode;
	
	@ApiModelProperty(value = "项目编号")
	@Excel(name="项目编号",width=15)
	private String salaryItemCode;
	
	@ApiModelProperty(value = "上限")
	@Excel(name="上限",width=15)
	private BigDecimal upperLimit;
	
	@ApiModelProperty(value = "下限")
	@Excel(name="下限",width=15)
	private BigDecimal lowerLimit;

	@ApiModelProperty(value = "备注")
	@Excel(name="备注",width=15)
	private String remark;
	
	@TableField(exist = false)
	private SalaryItem salaryItem;
}
