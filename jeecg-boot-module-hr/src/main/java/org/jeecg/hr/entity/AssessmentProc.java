package org.jeecg.hr.entity;

import java.math.BigDecimal;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.system.base.entity.JeecgEntity;

import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("hr_assessment_proc")
public class AssessmentProc extends JeecgEntity {
	
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "计划编号")
	@Excel(name="计划编号",width=15)
	private String planCode;
	
	@ApiModelProperty(value = "考评分类")
	@Excel(name="考评分类",width=15)
	private String procName;
	
	@ApiModelProperty(value = "考评人")
	@Excel(name="考评人",width=15)
	private String assignee;
	
	@ApiModelProperty(value = "权重（%）")
	@Excel(name="权重（%）",width=15)
	private BigDecimal weight;
	
	

}
