package org.jeecg.hr.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.system.base.entity.JeecgEntity;
import org.jeecg.modules.system.entity.SysUser;
import org.springframework.format.annotation.DateTimeFormat;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("hr_assessment_task")
public class AssessmentTask extends JeecgEntity {

	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "任务编号")
	@Excel(name="任务编号",width=15)
	private String instanceCode;
	
	@ApiModelProperty(value = "节点编号")
	@Excel(name="节点编号",width=15)
	private String taskCode;
	
	@ApiModelProperty(value = "被考评人")
	@Excel(name="被考评人",width=15)
	private String username;
	
	@ApiModelProperty(value = "考评人")
	@Excel(name="考评人",width=15)
	private String leader;
	
	@ApiModelProperty(value = "节点权重（%）")
	@Excel(name="节点权重（%）",width=15)
	private BigDecimal weight;
	
	@ApiModelProperty(value = "状态")
	@Excel(name="状态",width=15)
	private String taskStatus;
	
	@ApiModelProperty(value = "得分")
	@Excel(name="得分",width=15)
	private BigDecimal scale;
	
	@ApiModelProperty(value = "考评完成时间")
	@Excel(name = "考评完成时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date taskCompleteTime;
	
	@TableField(exist = false)
	private SysUser user;
	
	@TableField(exist = false)
	private SysUser leaderUser;
	
	@TableField(exist = false)
	private List<AssessmentTaskResult> results;
	
}
