package org.jeecg.hr.entity;

import java.math.BigDecimal;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.system.base.entity.JeecgEntity;

import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("hr_assessment_index")
public class AssessmentIndex extends JeecgEntity {
	
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "计划编号")
	@Excel(name="计划编号",width=15)
	private String planCode;
	
	@ApiModelProperty(value = "分类")
	@Excel(name="分类",width=15)
	private String indexName;
	
	@ApiModelProperty(value = "考核指标")
	@Excel(name="考核指标",width=15)
	private String indexDesc;
	
	@ApiModelProperty(value = "权重（%）")
	@Excel(name="权重（%）",width=15)
	private BigDecimal weight;
	
}
