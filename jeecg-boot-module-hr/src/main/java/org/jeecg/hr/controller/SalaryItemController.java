package org.jeecg.hr.controller;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.hr.entity.SalaryItem;
import org.jeecg.hr.service.SalaryItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * HR模块-薪酬项目
 * 
 * @author MxpIO
 *
 */
@Slf4j
@Api(tags = "薪酬项目")
@RestController
@RequestMapping("/hr/salary/item")
public class SalaryItemController extends JeecgController<SalaryItem, SalaryItemService> {
	
	@Autowired
	private SalaryItemService salaryItemService;
	
	/**
     * 分页列表查询
     *
     * @param salaryItem
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @ApiOperation(value = "获取薪酬项目数据列表", notes = "获取薪酬项目数据列表")
    @GetMapping(value = "/list")
    public Result<?> list(SalaryItem salaryItem, @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo, @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                          HttpServletRequest req) {
        QueryWrapper<SalaryItem> queryWrapper = QueryGenerator.initQueryWrapper(salaryItem, req.getParameterMap());
        Page<SalaryItem> page = new Page<SalaryItem>(pageNo, pageSize);

        IPage<SalaryItem> pageList = salaryItemService.page(page, queryWrapper);
        log.info("查询当前页：" + pageList.getCurrent());
        log.info("查询当前页数量：" + pageList.getSize());
        log.info("查询结果数量：" + pageList.getRecords().size());
        log.info("数据总数：" + pageList.getTotal());
        return Result.OK(pageList);
    }
    
    /**
     * 添加
     *
     * @param salaryItem
     * @return
     */
    @PostMapping(value = "/add")
    @AutoLog(value = "添加薪酬项目")
    @ApiOperation(value = "添加薪酬项目", notes = "添加薪酬项目")
    public Result<?> add(@RequestBody SalaryItem salaryItem) {
    	salaryItemService.saveWithDetail(salaryItem);
        return Result.OK("添加成功！",salaryItem);
    }

    /**
     * 编辑
     *
     * @param salaryItem
     * @return
     */
    @PutMapping(value = "/edit")
    @ApiOperation(value = "编辑薪酬项目", notes = "编辑薪酬项目")
    @AutoLog(value = "编辑薪酬项目", operateType = CommonConstant.OPERATE_TYPE_EDIT)
    public Result<?> edit(@RequestBody SalaryItem salaryItem) {
    	salaryItemService.updateByIdWithDetail(salaryItem);
        return Result.OK("更新成功！",salaryItem);
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "删除薪酬项目")
    @DeleteMapping(value = "/delete")
    @ApiOperation(value = "通过ID删除薪酬项目", notes = "通过ID删除薪酬项目")
    public Result<?> delete(@ApiParam(name = "id", value = "项目id", required = true) @RequestParam(name = "id", required = true) String id) {
    	salaryItemService.removeById(id);
        return Result.OK("删除成功!",null);
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @DeleteMapping(value = "/deleteBatch")
    @ApiOperation(value = "批量删除薪酬项目", notes = "批量删除薪酬项目")
    public Result<?> deleteBatch(@ApiParam(name = "ids", value = "项目id数组", required = true) @RequestParam(name = "ids", required = true) String ids) {
    	salaryItemService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功！",null);
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/queryById")
    @ApiOperation(value = "通过ID查询薪酬项目", notes = "通过ID查询薪酬项目")
    public Result<?> queryById(@ApiParam(name = "id", value = "项目id", required = true) @RequestParam(name = "id", required = true) String id) {
    	SalaryItem salaryItem = salaryItemService.getById(id);
        return Result.OK(salaryItem);
    }

    /**
     * 导出excel
     *
     * @param request
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, SalaryItem salaryItem) {
        return super.exportXls(request, salaryItem, SalaryItem.class, "薪酬项目数据");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, SalaryItem.class);
    }

}
