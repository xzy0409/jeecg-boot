package org.jeecg.feishu.common.exception;

import lombok.Getter;

public class FeishuException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	@Getter
    private int code;

    public FeishuException() {
    }

    public FeishuException(int code, String message) {
        this(code, message, null);
    }

    public FeishuException(int code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }
    public FeishuException(Throwable cause) {
        super(cause);
    }

}