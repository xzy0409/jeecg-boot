package org.jeecg.feishu.common.config.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

import org.jeecg.feishu.common.config.IStore;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class DefaultStore implements IStore {
    private ConcurrentMap<String, Value> cm;

    public DefaultStore() {
        this.cm = new ConcurrentHashMap<>(64);
    }

    @Override
    public Object get(String key) {
        log.debug("get key:{}", key);
        Value v = this.cm.get(key);
        if (v == null || new Date().after(v.end)) {
            return null;
        }
        return v.value;
    }

    @Override
    public void put(String key, Object value, int expire, TimeUnit timeUnit) {
        log.debug("put key:{}, value:{}, expire:{} second", key, value, timeUnit.toSeconds(expire));
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, (int) timeUnit.toSeconds(expire));
        Value v = new Value(value, calendar.getTime());
        this.cm.put(key, v);
    }

    static class Value {
        Object value;
        Date end;

        public Value(Object value, Date time) {
            this.value = value;
            this.end = time;
        }
    }
}
