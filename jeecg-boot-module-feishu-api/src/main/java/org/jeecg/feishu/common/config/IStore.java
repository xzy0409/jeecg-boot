package org.jeecg.feishu.common.config;

import java.util.concurrent.TimeUnit;

import org.jeecg.feishu.common.exception.FeishuException;

/**
 * 飞书请求数据缓存
 */
public interface IStore {

    Object get(String key) throws FeishuException;

    void put(String key, Object value, int expire, TimeUnit timeUnit) throws FeishuException;

}
