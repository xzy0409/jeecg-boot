package org.jeecg.feishu.api.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * 员工同步信息
 */
@Data
@ToString
public class EmployeeSyncDto implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
     * 租户内用户的唯一标识
     */
    @JSONField(name = "user_id")
    private String userId;
    /**
     * 用户名
     */
    @JSONField(name = "name")
    private String name;
    /**
     * 邮箱
     */
    @JSONField(name = "email")
    private String email;
    /**
     * 手机号
     */
    @JSONField(name = "mobile")
    private String mobile;
    /**
     * 性别0：保密 1：男 2：女
     */
    @JSONField(name = "gender")
    private int gender;
    /**
     * 用户所属部门的ID列表
     */
    @JSONField(name = "department_ids")
    private List<String> departmentIds;
    /**
     * 用户的直接主管的用户ID
     */
    @JSONField(name = "leader_user_id")
    private String leaderUserId;
    /**
     * 城市
     */
    @JSONField(name = "city")
    private String city;
    /**
     * 国家
     */
    @JSONField(name = "country")
    private String country;
    /**
     * 工作位置
     */
    @JSONField(name = "work_station")
    private String workStation;
    /**
     * 入职时间
     */
    @JSONField(name = "join_time")
    private Long joinTime;
    /**
     * 工号
     */
    @JSONField(name = "employee_no")
    private String employeeNo;
    /**
     * 员工类型,可选值有：  1：正式员工 2：实习生 3：外包 4：劳务 5：顾问
     */
    @JSONField(name = "employee_type")
    private Integer employeeType;
    /**
     * 自定义属性
     */
    @JSONField(name = "custom_attrs")
    private UserCustomAttr[] customAttrs;
    /**
     * 用户状态
     */
    @JSONField(name = "status", serialize = false)
    private UserStatus status;

    /**
     * 是否冻结用户, 更新用户数据时请求体参数
     */
    @JSONField(name = "is_frozen", deserialize = false)
    private Boolean isFrozen;

    /**
     * 用户状态
     */
    @Data
    @ToString
    @NoArgsConstructor
    public static class UserStatus implements Serializable {

        /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		/**
         * 1冻结，0未冻结
         */
        private final int IS_FROZEN_FLAG = 0b00000001;
        /**
         * 1离职，0在职
         */
        private final int IS_RESIGNED_FLAG = 0b00000010;
        /**
         * 1未激活，0已激活
         */
        private final int IS_ACTIVATED_FLAG = 0b00000100;

        /**
         * 是否冻结
         */
        @JSONField(name = "is_frozen")
        private Boolean isFrozen = false;
        /**
         * 是否离职
         */
        @JSONField(name = "is_resigned")
        private Boolean isResigned = false;
        /**
         * 是否激活
         */
        @JSONField(name = "is_activated")
        private Boolean isActivated = true;

        public UserStatus(int status) {
            setFrozenByBit(status);
            setActivatedByBit(status);
            setResignedByBit(status);
        }

        public void setFrozenByBit(int frozen) {
            isFrozen = (IS_FROZEN_FLAG & frozen) == frozen;
        }

        public void setResignedByBit(int resigned) {
            isResigned = (IS_RESIGNED_FLAG & resigned) == resigned;
        }

        public void setActivatedByBit(int activated) {
            isActivated = !((IS_ACTIVATED_FLAG & activated) == activated);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            UserStatus that = (UserStatus) o;
            return isFrozen.equals(that.isFrozen) &&
                    isResigned.equals(that.isResigned) &&
                    isActivated.equals(that.isActivated);
        }

        @Override
        public int hashCode() {
            return Objects.hash(isFrozen, isResigned, isActivated);
        }
    }

    /**
     * 自定义属性
     */
    @Data
    @ToString
    public class UserCustomAttr implements Serializable {
        /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		/**
         * 自定义属性类型  示例值："TEXT"
         */
        @JSONField(name = "type")
        private String type;
        /**
         * 自定义属性ID  示例值："DemoId"
         */
        @JSONField(name = "id")
        private String id;
        /**
         * 自定义属性取值
         */
        @JSONField(name = "value")
        private UserCustomAttrValue value;
    }

    /**
     * 自定义属性值
     */
    @Data
    @ToString
    public class UserCustomAttrValue implements Serializable {
        /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		/**
         * 自定义文本
         */
        @JSONField(name = "text")
        private String text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmployeeSyncDto that = (EmployeeSyncDto) o;
        return gender == that.gender &&
                joinTime == that.joinTime &&
                employeeType == that.employeeType &&
                Objects.equals(userId, that.userId) &&
                Objects.equals(name, that.name) &&
                Objects.equals(email, that.email) &&
                Objects.equals(mobile, that.mobile) &&
                Objects.equals(departmentIds, that.departmentIds) &&
                Objects.equals(leaderUserId, that.leaderUserId) &&
                Objects.equals(city, that.city) &&
                Objects.equals(country, that.country) &&
                Objects.equals(workStation, that.workStation) &&
                Objects.equals(employeeNo, that.employeeNo) &&
                Arrays.equals(customAttrs, that.customAttrs) &&
                Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(userId, name, email, mobile, gender, departmentIds, leaderUserId, city, country, workStation, joinTime, employeeNo, employeeType, status);
        result = 31 * result + Arrays.hashCode(customAttrs);
        return result;
    }
}
