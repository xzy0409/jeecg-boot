package org.jeecg.feishu.api.provider;

import org.jeecg.feishu.api.dto.WebAccessToken;
import org.jeecg.feishu.api.dto.WebAccessTokenCondition;
import org.jeecg.feishu.api.service.FeishuTokenService;
import org.jeecg.modules.system.provider.IAuthUserProvider;
import org.jeecg.modules.system.provider.ThirdType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FeishuAuthUserProvider implements IAuthUserProvider {
	
	@Autowired
	private FeishuTokenService feishuTokenService;

	@Override
	public String getUserIdByCode(String code) {
		
		WebAccessTokenCondition condition = new WebAccessTokenCondition();
		condition.setCode(code);
		condition.setGrantType("authorization_code");
		WebAccessToken access = feishuTokenService.getWebAccessToken(condition);
		return access.getUserId();
	}

	@Override
	public ThirdType getType() {
		return ThirdType.FEISHU;
	}

}
