package org.jeecg.feishu.api.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 *  员工搜索条件
 */
@Data
@ToString
@NoArgsConstructor
public class EmployeeCondition implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * 用于V3接口, 填写该字段表示获取部门下所有用户，选填。
     */
    @JSONField(name = "department_id")
    private String departmentId;

    /**
     * 用于V1接口,单次请求支持的最大用户数量为100
     */
    @JSONField(name = "employee_ids")
    private List<String> employeeIds;

    public EmployeeCondition(List<String> employeeIds) {
        this.employeeIds = employeeIds;
    }
}
