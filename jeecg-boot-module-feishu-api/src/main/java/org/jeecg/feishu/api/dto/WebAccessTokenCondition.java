package org.jeecg.feishu.api.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 根据code获取登录用户身份查询参数
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WebAccessTokenCondition implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
     * 授权类型，本流程中，此值为："authorization_code"
     */
    @JSONField(name = "grant_type")
    private String grantType;
    /**
     * 来自请求身份验证流程，用户扫码登录后会自动302到redirect_uri并带上此参数
     *
     * 示例值："xMSldislSkdK"
     */
    private String code;
}
