package org.jeecg.feishu.api.utils;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class NotifyDataDecrypter {

    public static byte[] decrypterKey(String keyStr) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            // won't happen
        }
        return digest.digest(keyStr.getBytes(StandardCharsets.UTF_8));
    }


    /**
     *
     * @param key 飞书应用配置的 Encrypt Key
     * @param content 请求json encrypt的对应的值
     * @return
     */
    public static String decrypt(String key, String content)  {

        byte[] decode = Base64.getDecoder().decode(content);

        Cipher cipher = null;
        try {
            cipher = Cipher.getInstance("AES/CBC/NOPADDING");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            e.printStackTrace();
        }

        byte[] iv = new byte[16];
        System.arraycopy(decode, 0, iv, 0, 16);

        byte[] data = new byte[decode.length - 16];
        System.arraycopy(decode, 16, data, 0, data.length);

        try {
            cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(decrypterKey(key), "AES"), new IvParameterSpec(iv));
        } catch (InvalidKeyException | InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }

        byte[] r = new byte[0];
        try {
            r = cipher.doFinal(data);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        }
        if (r.length > 0) {
            int p = r.length - 1;
            for (; p >= 0 && r[p] < 16; p--) {
            }

            if (p != r.length - 1) {
                byte[] rr = new byte[p + 1];
                System.arraycopy(r, 0, rr, 0, p + 1);
                r = rr;
            }
        }

        String str = new String(r, StandardCharsets.UTF_8);

        System.out.println(str);

        return new String(r, StandardCharsets.UTF_8);
    }


    public static void main(String[] args)  {
        // decrypt("kudryavka","FIAfJPGRmFZWkaxPQ1XrJZVbv2JwdjfLk4jx0k/U1deAqYK3AXOZ5zcHt/cC4ZNTqYwWUW/EoL+b2hW/C4zoAQQ5CeMtbxX2zHjm+E4nX/Aww+FHUL6iuIMaeL2KLxqdtbHRC50vgC2YI7xohnb3KuCNBMUzLiPeNIpVdnYaeteCmSaESb+AZpJB9PExzTpRDzCRv+T6o5vlzaE8UgIneC1sYu85BnPBEMTSuj1ZZzfdQi7ZW992Z4dmJxn9e8FL2VArNm99f5Io3c2O4AcNsQENNKtfAAxVjCqc3mg5jF0nKabA+u/5vrUD76flX1UOF5fzJ0sApG2OEn9wfyPDRBsApn9o+fceF9hNrYBGsdtZrZYyGG387CGOtKsuj8e2E8SNp+Pn4E9oYejOTR+ZNLNi+twxaXVlJhr6l+RXYwEiMGQE9zGFBD6h2dOhKh3W84p1GEYnSRIz1+9/Hp66arjC7RCrhuW5OjCj4QFEQJiwgL45XryxHtiZ7JdAlPmjVsL03CxxFZarzxzffryrWUG3VkRdHRHbTsC34+ScoL5MTDU1QAWdqUC1T7xT0lCvQELaIhBTXAYrznJl6PlA83oqlMxpHh0gZBB1jFbfoUr7OQbBs1xqzpYK6Yjux6diwpQB1zlZErYJUfCqK7G/zI9yK/60b4HW0k3M+AvzMcw=");
        decrypt("XwkFttsnRxMTXl0f93SwVcYvpVzf1jrm","feURdXsQDd+Wb/LeCc28Jetd/K0OsZVNvhirMQ2ICwLTzDFVeNb8cQAJ7/z8QDkHkvc9fB31nz9Hh9LJvvFR6J7osa3OlC+PAMuMoui4k2Xk9LKRy303JIccSP5fypZ5hyzvjeRRfsO8l7v3T42Uyl5SmHh9I9jeLBaHf3POG5vBcIN5Tqzaxg/op+lEdI+UUXmdntKrYoHH8CFtI2ibnV0PNSqsH+OCkBb75BaxEXYhKTcvf8wBZSYN5n85XS077httQdmnBdXhPQc7tOi3o3vYKgfTRBBQVV4shgeA9Y9VClhbbtI8iPlSMl71OHJqQ2FByDKHK46G6hISEU4lN686gTr1OfisX4OKlbNbfTe1cUoLothRRofoU2Smrdt8O32NHdAI/gGEZPgczU5bMTQVw9Bm3EBnBaH5mhGU4qFA+4F57BwW96QyAgZKe+8D0aq2kJ6Pcj5liCMyd5e22wjaePNK4nE2piTREsaCpHGzUUgj9/IeafHQ2G00gdlU");
    }
}