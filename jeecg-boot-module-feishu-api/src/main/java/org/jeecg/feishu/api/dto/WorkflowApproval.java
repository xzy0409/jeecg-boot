package org.jeecg.feishu.api.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * (WorkflowApproval)实体类
 *
 * @author makejava
 * @since 2021-10-12 00:43:56
 */
@Data
@ToString
public class WorkflowApproval implements Serializable {
    private static final long serialVersionUID = 485157014773165820L;

    private Long id;
    /**
     * 审批定义 Code
     */
    @JSONField(name = "approval_code")
    private String approvalCode;
    /**
     * 审批名称
     */
    @JSONField(name = "approval_name")
    private String approvalName;
    /**
     * 审批创建时间
     */
    @JSONField(name = "start_time")
    private Long startTime;
    /**
     * 审批完成时间，未完成为 0
     */
    @JSONField(name = "end_time")
    private Long endTime;
    /**
     * 发起审批用户
     */
    @JSONField(name = "user_id")
    private String userId;
    /**
     * 发起审批用户 open id
     */
    @JSONField(name = "open_id")
    private String openId;
    /**
     * 审批单编号
     */
    @JSONField(name = "serial_number")
    private String serialNumber;
    /**
     * 发起审批用户所在部门
     */
    @JSONField(name = "department_id")
    private String departmentId;
    /**
     * 审批实例状态
     * PENDING - 审批中
     * APPROVED - 通过
     * REJECTED - 拒绝
     * CANCELED - 撤回
     * DELETED - 删除
     */
    private String status;
    /**
     * 用户的唯一标识id
     */
    private String uuid;

    /**
     * 审批任务列表
     */
    @JSONField(name = "task_list")
    private List<Task> taskList;

    /**
     * 评论列表
     */
    @JSONField(name = "comment_list")
    private List<Comment> commentList;

    /**
     * 评论列表
     */
    @JSONField(name = "timeline")
    private List<TimeLine> timeLineList;

    @Data
    public static class Task {
        /**
         * task id
         */
        private String id;

        /**
         * 审批人
         * 自动通过、自动拒绝 task user_id 为空
         */
        @JSONField(name = "user_id")
        private String userId;

        /**
         * 审批人 open id
         */
        @JSONField(name = "open_id")
        private String openId;

        /**
         * 任务状态
         * PENDING - 审批中
         * APPROVED - 同意
         * REJECTED - 拒绝
         * TRANSFERRED - 已转交
         * DONE - 完成
         */
        private String status;

        /**
         * task 所属节点 id
         */
        @JSONField(name = "node_id")
        private String nodeId;

        /**
         * task 所属节点自定义 id, 如果没设置自定义 id, 则不返回该字段
         */
        @JSONField(name = "custom_node_id")
        private String customNodeId;

        /**
         * 审批方式
         * AND -会签
         * OR - 或签
         * AUTO_PASS -自动通过
         * AUTO_REJECT - 自动拒绝
         * SEQUENTIAL - 按顺序
         */
        @JSONField(name = "type")
        private String type;

        /**
         * task 开始时间
         */
        @JSONField(name = "start_time")
        private Long startTime;

        /**
         * task 结束时间
         */
        @JSONField(name = "end_time")
        private Long endTime;
    }

    @Data
    @ToString
    public static class Comment {
        /**
         * comment id
         */
        private String id;
        /**
         * 发表评论用户
         */
        @JSONField(name = "user_id")
        private String userId;
        /**
         * 发表评论用户 open id
         */
        @JSONField(name = "open_id")
        private String openId;
        /**
         * 评论内容
         */
        @JSONField(name = "comment")
        private String comment;
        /**
         * 评论时间
         */
        @JSONField(name = "create_time")
        private Long createTime;
    }

    @Data
    @ToString
    public static class TimeLine {
        /**
         * 	动态类型，不同类型 ext 内的 user_id_list 含义不一样
         * START - 审批开始
         * PASS - 通过
         * REJECT - 拒绝
         * AUTO_PASS - 自动通过
         * AUTO_REJECT - 自动拒绝
         * REMOVE_REPEAT - 去重
         * TRANSFER - 转交
         * ADD_APPROVER_BEFORE - 前加签
         * ADD_APPROVER - 并加签
         * ADD_APPROVER_AFTER - 后加签
         * DELETE_APPROVER - 减签
         * ROLLBACK_SELECTED - 指定回退
         * ROLLBACK - 全部回退
         * CANCEL - 撤回
         * DELETE - 删除
         * CC - 抄送
         */
        private String type;
        /**
         * 发生时间
         */
        @JSONField(name = "create_time")
        private Long createTime;
        /**
         * 动态产生用户
         */
        @JSONField(name = "user_id")
        private String userId;
        /**
         * 动态产生用户 open id
         */
        @JSONField(name = "open_id")
        private String openId;
        /**
         * 产生动态关联的task_id
         */
        @JSONField(name = "task_id")
        private String taskId;
        /**
         * 理由
         */
        @JSONField(name = "comment")
        private String comment;
    }
}