package org.jeecg.feishu.api.dto;

import java.io.Serializable;
import java.util.List;

import org.jeecg.feishu.api.dto.message.TextContent;

import com.alibaba.fastjson.annotation.JSONField;

import lombok.Data;

@Data
public class MessageCondition implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
     * msg_type
     */
    @JSONField(name = "msg_type")
    private String msgType;
    
    /**
     * content
     */
    @JSONField(name = "content")
    private TextContent content;
    
    /**
     * department_ids
     */
    @JSONField(name = "department_ids")
    private List<String> departmentIds;
    
    /**
     * open_ids
     */
    @JSONField(name = "open_ids")
    private List<String> openIds;
    /**
     * user_ids
     */
    @JSONField(name = "user_ids")
    private List<String> userIds;

}
