package org.jeecg.feishu.api.config;

import com.alibaba.fastjson.annotation.JSONField;
import org.jeecg.feishu.api.enums.AccessTokenType;
import org.jeecg.feishu.common.config.IStore;

import lombok.Data;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 飞书API应用配置
 * @author rcx
 */
@Component
@ConfigurationProperties(prefix = "feishu")
@Data
@ToString
public class FeishuConfig {

    /**
     * token缓存key
     */
    public static final String TENANT_TOKEN_KEY = "tenantTokenKeyConfig_";

    /**
     * token缓存key
     */
    public static final String APP_TOKEN_KEY = "appTokenKeyConfig_";

    /**
     * token缓存key
     */
    public static final String USER_TOKEN_KEY = "userTokenKeyConfig_";

    /**
     * 部门ID映射缓存key
     */
    public static final String DEPT_ID_MAPPING_KEY = "deptIdMappingKeyConfig_";
    /**
     * 部门ID映射缓存key
     */
    public static final String USER_ID_MAPPING_KEY = "userIdMappingKeyConfig_";
    /**
     * 部门ID映射缓存key
     */
    public static final String DEPT_INFO_KEY = "deptInfoKeyConfig_";

    @JSONField(deserialize = false)
    private AccessTokenType accessTokenType;

    @JSONField(name = "app_id")
    private String appId;

    @JSONField(name = "app_secret")
    private String appSecret;
    /**
     * open-api域名
     */
    @JSONField(name = "domain", serialize = false)
    private String domain;
    /**
     * 审批域名
     */
    @JSONField(name = "approval_domain", serialize = false)
    private String approvalDomain;
    /**
     * api httpath前綴
     */
    @JSONField(name = "api_root_path", serialize = false)
    private String apiRootPath;
    /**
     * 部门根节点ID
     */
    @JSONField(name = "root_dept_id", serialize = false)
    private String rootDeptId;

    @Autowired
    @Qualifier("defaultStore")
    @JSONField(serialize = false, deserialize = false)
    private IStore store;

    public String getCacheKey(String key) {
        return appId + "_" + key;
    }

}
