package org.jeecg.feishu.api.config.controller;

import java.io.UnsupportedEncodingException;

import org.jeecg.common.api.vo.Result;
import org.jeecg.feishu.api.service.FeishuTokenService;
import org.jeecg.feishu.common.exception.FeishuException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/feishu/auth")
@Api(tags="飞书-权限")
public class FishuAuthController {
	
	@Autowired
	private FeishuTokenService feishuTokenService;
	
	@ApiOperation(value = "获取飞书权限", notes = "获取飞书权限")
    @GetMapping(value = "/getAuthUrl")
    public Result<Object> getAuthUrl(
    		@ApiParam(name = "url", value = "菜单", required = true) @RequestParam(value = "url", required = true) String url) throws FeishuException, UnsupportedEncodingException {
		log.info("getAuthUrl url===>"+url);
		String result = feishuTokenService.getAuthUrl(url);
		log.info("getAuthUrl result===>"+result);
		return Result.OK(result);
    }
}
