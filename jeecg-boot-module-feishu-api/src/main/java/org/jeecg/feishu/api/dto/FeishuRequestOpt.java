package org.jeecg.feishu.api.dto;

import com.google.common.collect.Maps;
import lombok.Data;

import java.util.Map;

@Data
public class FeishuRequestOpt {

    private Map<String, Object> pathParams = Maps.newHashMap();
    private Map<String, Object> queryParams = Maps.newHashMap();
    private long timeoutOfMs = 5000L;
    private boolean isNotDataField = false;

}